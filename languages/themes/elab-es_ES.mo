��    ,      |      �      �     �     �  '   �     !  
   %     0     5     G     S     c     t     �     �     �     �     �  |   �  
   ?     J  &   O     v          �     �     �     �     �     �     �  !   �     �               $     3     A  A   [     �     �     �     �     �      �  �       �     �  )   �     �     �     �     �               7     O     `     p     �     �     �  �   �  	   K	     U	  $   \	     �	     �	     �	     �	  	   �	     �	     �	     �	     �	  %   �	     
     3
  	   D
     N
     ]
     l
  I   �
  
   �
     �
     �
     �
  
     )      All categories Apply coupon Be the first to review &ldquo;%s&rdquo; Buy Categories City Continue shopping Coupon code Customer Rating Customer Reviews Email address Estimate shipping Hide details Log in Login Lost your password? Lost your password? Please enter your username or email address. You will receive a link to create a new password via email. My account Name No products were added to the wishlist Password Postcode / ZIP Price Proceed to checkout Product Quantity Ratings Register Reviews Select a country / region&hellip; Ship to a different address? Show details Stars State / County Submit Review There are no reviews yet. This is only an estimate. Prices will be updated during checkout. Update Update cart What are you looking for? Your rating Your review Your review is awaiting approval Project-Id-Version: eLab
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-04-13 07:49+0000
PO-Revision-Date: 2020-07-09 21:47+0000
Last-Translator: Jorge Troya
Language-Team: Español
Language: es_ES
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.0; wp-5.4.2 Categorías Aplicar cupón Se el primero en valorar &ldquo;%s&rdquo; Comprar Categorías Ciudad Continuar comprando Código de cupón Valoraciones de los clientes Reseña de los clientes Dirección email Envío estimado Ocultar detalles Ingresar Ingresar Olvidaste tu contraseña? Olvidaste tu contraseña? Por favor ingrese su usuario o dirección de correo electrónico. Recibirás un enlace para crear una nueva contraseña en tu email Mi cuenta Nombre No hay ningún producto en Favoritos Contraseña Código postal Precio Continuar al checkout Productos Cantidad Clasificación Registro Reseñas Selecciona un país / región&hellip; El envío es a otra dirección? Mostrar detalles Estrellas Estado / País Enviar Reseña No hay reseñas aún Esto es solo un estimado. Precios serán actualizados durante el checkout Actualizar Actualizar carrito Que buscas hoy? Su valoración Su Reseña Su reseña esta en espera por aprobación 