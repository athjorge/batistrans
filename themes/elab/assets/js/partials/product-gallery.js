"use strict";

(function ($) {
  /**
   * We have Outer Flex slider, and in case of product style
   * we change scripts
   * Style 1 - Carousel on Flex slider thumbnails
   * Style 2 - Flex slider thumbnails acts like prev/next button (via css)
   */
  $(window).load(function () {
    var $body = $('body');

    if ($body.hasClass('product_single_style_1')) {
      initProductGallery();
    } else if ($body.hasClass('product_single_style_2')) {
      wrapFlexButtons();
      $('.flex-control-nav').on('click', function () {
        wrapFlexButtons();
      });
    } else if ($body.hasClass('product_single_style_3')) {
      $('ol.flex-control-nav').slick({
        infinite: false,
        vertical: true,
        verticalSwiping: true,
        slidesToShow: 3,
        slidesToScroll: 1
      });
    }
  });

  function initProductGallery() {
    var $productThumbs = $('.woocommerce div.product div.images .flex-control-thumbs');
    $productThumbs.addClass('owl-carousel');
    $productThumbs.imagesLoaded(function () {
      $productThumbs.owlCarousel({
        items: 3,
        slideBy: 3,
        loop: false,
        nav: true,
        dots: false,
        margin: 15
      });
    });
  }

  function wrapFlexButtons() {
    var $flexNav = $('.woocommerce-product-gallery .flex-control-nav');
    var active = 0;

    if ($flexNav.length) {
      var flexNavLength = $flexNav.find('li').length;
      $flexNav.find('img').each(function () {
        if ($(this).hasClass('flex-active')) {
          active = $(this).closest('li').index();
        }
      });
      $flexNav.find('li').removeClass('prev next');

      if (active > 0) {
        $flexNav.find('li').eq(active - 1).addClass('prev');
      }

      if (active < flexNavLength) {
        $flexNav.find('li').eq(active + 1).addClass('next');
      }
    }
  }
})(jQuery);