"use strict";

(function ($) {
  $(document).ready(function () {
    $('.x_portfolio').each(function () {
      var $this = $(this);
      var module_id = $(this).data('module');
      new Vue({
        el: $this[0],
        data: function data() {
          return {
            categories: window[module_id]['categories'],
            total: window[module_id]['total'],
            active_category: '',
            category: '',
            products: {},
            loading: false,
            packered: false,
            offset: 0
          };
        },
        mounted: function mounted() {
          if (this.categories.length) {
            this.getPortfolio(this.categories[0]);
          }
        },
        methods: {
          hasMorePosts: function hasMorePosts() {
            var _this = this;

            var c = _this.products[_this.active_category];
            return c.total > (c.offset + 1) * c.per_page;
          },
          loadMore: function loadMore() {
            var _this = this;

            if (_this.loading) return false;
            _this.loading = true;
            var c = _this.products[_this.active_category];
            var offset = c.offset + 1;
            var url = "".concat(x_ajax_url, "?action=x_get_portfolio&term_id=").concat(_this.category.term_id, "&offset=").concat(offset);

            _this.$http.get(url).then(function (r) {
              var products = r.body;

              _this.$set(_this.products[_this.category.term_id], 'products', c['products'].concat(products['products']));

              _this.$set(_this.products[_this.category.term_id], 'offset', products['offset']);

              _this.$set(_this.products[_this.category.term_id], 'per_page', products['per_page']);

              _this.packery();
            });
          },
          getPortfolio: function getPortfolio(category, newOffset) {
            var _this = this;

            _this.$set(_this, 'category', category);

            _this.$set(_this, 'active_category', category.term_id);

            _this.loading = true;

            if (typeof _this.products[category.term_id] !== 'undefined') {
              _this.packery();

              return;
            }

            _this.$set(_this.products, category.term_id, []);

            var url = "".concat(x_ajax_url, "?action=x_get_portfolio&term_id=").concat(category.term_id, "&offset=").concat(this.offset);

            _this.$http.get(url).then(function (r) {
              var products = r.body;

              _this.$set(_this.products, category.term_id, products);

              _this.packery();
            });
          },
          packery: function packery() {
            var _this = this;

            Vue.nextTick(function () {
              var $container = $('.' + module_id + ' .x_portfolio__grid');
              $container.imagesLoaded(function () {
                var $pckry = $container.packery({
                  // options
                  itemSelector: '.x_portfolio__grid__single',
                  gutter: 0
                });
                $pckry.packery('reloadItems');
                setTimeout(function () {
                  _this.products[_this.active_category].products.forEach(function (val, key) {
                    _this.$set(_this.products[_this.active_category].products[key], 'loaded', true);
                  });

                  var $pckry = $container.packery({
                    // options
                    itemSelector: '.x_portfolio__grid__single',
                    gutter: 0
                  });
                  $pckry.packery('reloadItems');
                }, 100);
                _this.loading = false;
              });
            });
          }
        }
      });
    });
  });
})(jQuery);