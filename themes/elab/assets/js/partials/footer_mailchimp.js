"use strict";

(function ($) {
  $(document).ready(function ($) {
    $(".stm_subscribe").on('submit', function (e) {
      e.preventDefault();
      var $this = $(this);
      var $preloader = $this.find(".stm_subscribe_preloader");
      $preloader.addClass("loading");
      $.ajax({
        type: 'POST',
        data: 'action=stm_subscribe&email=' + $($this).find(".stm_subscribe_email").val(),
        dataType: 'json',
        url: ajaxurl,
        success: function success(json) {
          if (json['success']) {
            $($this).replaceWith('<div class="success_message">' + json['success'] + '</div>');
          }

          if (json['error']) {
            alert(json['error']);
          }

          $preloader.removeClass("loading");
        }
      });
      return false;
    });
  });
})(jQuery);