"use strict";

(function ($) {
  $(document).ready(function () {
    elabStickySidebar();
    console.log('asdasd');
  });
  $(window).on('resize', function () {
    elabStickySidebar();

    if ($(window).width() <= 1200) {
      $('.elab_product_main .woocommerce-product-gallery').stickySidebar('destroy');
    }
  });

  function elabStickySidebar() {
    if ($(window).width() > 1200) {
      $('.elab_product_main .woocommerce-product-gallery').stickySidebar({
        bottomSpacing: 40
      });
      $('.wc-tabs > li > a').on('click', function () {
        setTimeout(function () {
          $('.elab_product_main .woocommerce-product-gallery').stickySidebar('updateSticky');
        }, 200);
      });
    }
  }
})(jQuery);