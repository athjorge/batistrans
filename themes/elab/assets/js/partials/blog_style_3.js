"use strict";

(function ($) {
  $(window).load(function () {
    masonryBlog();
  });

  var masonryBlog = function masonryBlog() {
    var $container = $('.elab-news_style_3 > .row');
    $container.imagesLoaded(function () {
      var $pckry = $container.packery({
        // options
        itemSelector: '.col-xl-3',
        gutter: 0
      });
    });
  };
})(jQuery);