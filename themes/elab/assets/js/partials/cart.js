"use strict";

(function ($) {
  $(document).ready(function ($) {
    var clicker = '.elab_cart_shipping__tabs_list span';
    $('body').on('click', clicker, function () {
      var $clicker = $(clicker);
      var $tab = $('.elab_cart_shipping__tab');
      $clicker.removeClass('active');
      $(this).addClass('active');
      var index = $(this).index();
      $tab.removeClass('active');
      $tab.eq(index).addClass('active');
    });
    var timer;
    $("body").bind("DOMSubtreeModified", '.elab_cart_shipping', function () {
      clearTimeout(timer);
      timer = setTimeout(function () {
        initNiceSelect();
      }, 1000);
    });
  });

  var initNiceSelect = function initNiceSelect() {
    var $selector = $('select');
    $selector.removeAttr('class');
    $selector.niceSelect();
    $selector.on('change', function () {
      setTimeout(function () {
        $selector.niceSelect('update');
      }, 100);
    });
  };
})(jQuery);