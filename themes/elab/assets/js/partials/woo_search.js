"use strict";

(function ($) {
  $(document).ready(function () {
    var current_category = '';
    $(document).on('click', function (event) {
      var $target = $(event.target);

      if (!$target.closest('.woo_search__categories').length) {
        $('.woo_search__categories').removeClass('active');
      }
    });
    $('.woo_search__category').on('click', function () {
      if (!$(this).hasClass('active_category')) {
        $('.woo_search__category').removeClass('active_category');
        $(this).addClass('active_category');
        $('.woo_search__categories').removeClass('active');
        current_category = $(this).attr('data-category');
        $(".woo_search__input").autocomplete("search");
      } else {
        $(this).closest('.woo_search__categories').addClass('active');
      }
    });

    if ($('.woo-search-wrapper').length) {
      $(".woo-search-wrapper .woo_search__input").autocomplete({
        source: function source(request, response) {
          $.ajax({
            url: "".concat(ajaxurl, "?action=elab_search_woo_products"),
            dataType: "json",
            data: {
              search_query: request.term,
              category: current_category,
              security: window.wp_data.elab_search_woo_products
            },
            success: function success(data) {
              response(data);
            }
          });
        },
        appendTo: ".woo-search-wrapper .woo_search",
        minLength: 2,
        select: function select(event, ui) {
          var win = window.open(ui.item.permalink, '_blank');
          win.focus();
        }
      }).data("ui-autocomplete")._renderItem = function (ul, item) {
        var item_html = "<a class='elab_choice_suggested'>";
        if (item.image) item_html += "<img src='" + item.image + "'/>";
        item_html += "<div class='elab_choice_suggested__content'>" + item.html;
        if (item.term) item_html += "<span>" + item.term + "</span>";
        item_html += "</div>";
        item_html += "</a>";
        return $("<li></li>").data("item.autocomplete", item).append(item_html).appendTo(ul);
      };

      jQuery.ui.autocomplete.prototype._resizeMenu = function () {
        var ul = this.menu.element;
        ul.outerWidth(this.element.outerWidth());
      };
    }

    checkWidth();
  });
  $(window).resize(function () {
    checkWidth();
  });
  var click_inited = false;

  function checkWidth() {
    if (click_inited) return true;
    click_inited = true;
    $('.woo_search button').on('click', function (e) {
      e.stopPropagation();
      var form = $(this).closest('form');
      var hasValue = form.find('.woo_search__input').val().length;

      if (!hasValue) {
        e.preventDefault();
        $('.woo_search').toggleClass('active');
        click_inited = false;
        return false;
      }
    });
  }
})(jQuery);