"use strict";

(function ($) {
  var items = [];
  var sum = 0;
  $(document).ready(function () {
    calcSum();
    $('.elab_product_accessories_single__add').on('click', function () {
      $(this).toggleClass('added');
      calcSum();
    });
    addToCart();
  });

  function calcSum() {
    items = [];
    sum = 0;
    $('.elab_product_accessories_single').each(function () {
      var $item = $(this).find('.elab_product_accessories_single__add');
      var price = $item.data('price');
      var product_id = $item.data('add');

      if ($item.hasClass('added')) {
        items.push(product_id);
        sum += price;
      }
    });
    printTotal();
  }

  function printTotal() {
    $('.elab_product_accessories_bottom_info .sum').text(wc_price(sum));
    $('.elab_product_accessories_bottom_info .num').text(items.length);
  }

  function addToCart() {
    $('.elab_product_accessories_bottom .btn').on('click', function () {
      var $this = $(this);
      if ($(this).hasClass('loading')) return false;
      $(this).addClass('loading');
      $.ajax({
        type: 'POST',
        data: 'action=elab_add_accessories_bundle&items=' + items.join('|') + '&security=' + window.wp_data.elab_add_accessories_bundle,
        dataType: 'json',
        url: ajaxurl,
        success: function success(json) {
          console.log(json);
          $this.removeClass('loading');

          if (json.added) {
            refresh_cart_fragment();
          }
        }
      });
    });
  }
  /* Named callback for refreshing cart fragment */


  function refresh_cart_fragment() {
    var $fragment_refresh = {
      url: wc_cart_fragments_params.wc_ajax_url.toString().replace('%%endpoint%%', 'get_refreshed_fragments'),
      type: 'POST',
      data: {
        time: new Date().getTime()
      },
      timeout: wc_cart_fragments_params.request_timeout,
      success: function success(data) {
        if (data && data.fragments) {
          $.each(data.fragments, function (key, value) {
            $(key).replaceWith(value);
          });
          $(document.body).trigger('wc_fragments_refreshed');
        }
      },
      error: function error() {
        $(document.body).trigger('wc_fragments_ajax_error');
      }
    };
    $.ajax($fragment_refresh);
  }

  function wc_price(num) {
    var $wrapper = $('.elab_product_accessories_bundle_wrapper');
    var sep = $wrapper.data('sep');
    return numberWithCommas(num, sep);
  }

  function numberWithCommas(num, sep) {
    console.log(num);
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, sep);
  }
})(jQuery);