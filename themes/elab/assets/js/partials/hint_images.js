"use strict";

/**
 * @var elab_vars
 */
(function ($) {
  $(document).ready(function () {
    $('.elab_hint_images').on('click', function () {
      var openText = $(this).attr('data-open-text');
      var closeText = $(this).attr('data-close-text');
      $('.el').text(openText);
    });
    $('.elab_hint_images').each(function () {
      var $container = $(this);
      if ($container.hasClass('single-hint')) return false;
      $container.imagesLoaded(function () {
        var $pckry = $container.packery({
          // options
          itemSelector: '.elab_hint_image',
          vertical: true,
          percentPosition: true
        });
      });
    });
  });
})(jQuery);