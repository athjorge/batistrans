"use strict";

(function ($) {
  $(document).ready(function () {
    $('.elab_woocommerce_per_page select').on('change', function () {
      var $this = $(this);
      var option_val = $this.val();
      window.location.replace($this.find("option[value=\"".concat(option_val, "\"]")).data('src'));
    });
  });
})(jQuery);