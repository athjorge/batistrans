"use strict";

(function ($) {
  var viewed = '';
  $(document).ready(function () {
    checkViewed();
  });
  $(window).load(function () {
    if (!viewed) $('body').addClass('promo-popup-locked');
  });
  $('.elab_promo_popup__overlay, .elab_promo_popup__close').on('click', function () {
    $('body').removeClass('promo-popup-locked');
    var popup = $(this).data('popup-id');
    localStorage.setItem(popup, 'viewed');
  });

  function checkViewed() {
    $('.elab_promo_popup').each(function () {
      viewed = localStorage.getItem($(this).data('popup-id'));
    });
  }
})(jQuery);