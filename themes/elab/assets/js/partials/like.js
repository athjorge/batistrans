"use strict";

/**
 * @var elab_vars
 */
(function ($) {
  $(document).ready(function () {
    likeit();
  });

  function likeit() {
    var $like = $('[data-like]');
    $like.each(function () {
      var post_id = $(this).attr('data-like');
      var name = 'plugin_' + post_id;
      var isClicked = localStorage.getItem(name);

      if (isClicked !== null) {
        $('[data-like]').removeClass('notLiked');
      }
    });
    $like.on('click', function (e) {
      e.preventDefault();
      var post_id = $(this).attr('data-like');
      var name = 'plugin_' + post_id;
      var isClicked = localStorage.getItem(name);

      if (isClicked === null) {
        $(this).removeClass('notLiked');
        localStorage.setItem(name, 'setted');
        $.ajax({
          url: elab_vars['ajax_url'],
          dataType: 'json',
          context: this,
          data: {
            action: 'elab_like_portfolio',
            post_id: post_id,
            security: window.wp_data.elab_like_portfolio
          },
          beforeSend: function beforeSend() {
            $(this).addClass('loading');
          },
          complete: function complete(data) {
            var data = data['responseJSON'];
            $(this).removeClass('loading');
            $(this).find('span').text(data);
          }
        });
      }
    });
  }
})(jQuery);