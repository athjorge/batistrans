"use strict";

(function ($) {
  $(document).ready(function () {
    $('.header_woo_categories__toggle').on('click', function () {
      $('.header_woo_categories').toggleClass('active');
    });
    $('.header_woo_categories__overlay').on('click', function () {
      $('.header_woo_categories').removeClass('active');
    });
    checkWidth();
  });

  function checkWidth() {
    var $woo_toggle = $('.header_woo_categories__toggle');
    /*Check For Home page && woo_categories*/

    if ($woo_toggle.length && $('body').hasClass('home')) {
      var width = $(window).width();

      if (width <= 1023) {
        $('.header_woo_categories').removeClass('active');
      }
    }
  }
})(jQuery);