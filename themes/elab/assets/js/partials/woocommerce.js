"use strict";

var elab_product_added = 0;
document.addEventListener("elab_product_added", function (e) {
  elab_product_added++;
  if (elab_product_added < 3) return true;
  var $ = jQuery;
  var $notice = $('body').find('.cart__notification');
  setTimeout(function () {
    $notice.addClass('added');
  }, 300);
  setTimeout(function () {
    $notice.removeClass('added');
  }, 3000);
});

(function ($) {
  $(window).load(function () {
    variation_change();
  });
  $(document).ready(function ($) {
    var $select = $('.woocommerce-billing-fields .nice-select, .update_totals_on_change .nice-select');
    var $selector = $('.woocommerce-billing-fields .nice-select li, .update_totals_on_change .nice-select li');
    $select.on('click', function () {
      $(this).addClass('open');
    });
    $selector.on('click', function (e) {
      e.stopPropagation();
      var $this = $(this);
      var $current_select = $this.closest('.nice-select');
      var $origin_select = $current_select.prev();
      $origin_select.val($this.data('value'));
      $current_select.find('.current').text($this.text());
      $current_select.removeClass('open');
    });
    var opened = false;
    $('.elab_order_reviewing__top a').on('click', function (e) {
      e.preventDefault();
      var openText = $(this).attr('data-show');
      var hideText = $(this).attr('data-hide');

      if (!opened) {
        opened = true;
        $(this).text(hideText);
      } else {
        opened = false;
        $(this).text(openText);
      }

      $(this).toggleClass('active');
      $('.woocommerce-checkout-review-order-table thead, .woocommerce-checkout-review-order-table tbody').slideToggle();
    });
    $('body').on('click', '.yith_woocompare_colorbox', function () {
      $('#cboxClose').trigger('click');
    });
    variation_change();
    $(".variations_form").on("woocommerce_variation_select_change", function () {
      variation_change();
    });
    $(document).on('show_variation', '.cart.variations_form', function (e) {
      var hasText = !!$.trim($('.woocommerce-variation.single_variation').text());
      $('.woocommerce-variation.single_variation').stop().removeAttr('style').toggle(hasText);
    }).on('hide_variation', function (e) {
      $('.woocommerce-variation.single_variation').stop().hide();
    });
    var orderby = 'form.woocommerce-ordering';
    $(orderby).on('change', 'select', function (e) {
      window.location.href = updateQueryStringParameter(location.href, 'orderby', $(this).val());
    });

    function updateQueryStringParameter(uri, key, value) {
      var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
      var separator = uri.indexOf('?') !== -1 ? "&" : "?";

      if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
      } else {
        return uri + separator + key + "=" + value;
      }
    }

    $('#yith-quick-view-modal').on('click', '.woocommerce-product-gallery__image', function (e) {
      e.stopPropagation();
      e.preventDefault();
      var wrapper = $(this).closest('.woocommerce-product-gallery__wrapper');
      var element = $(this).detach();
      wrapper.prepend(element);
    });
    shop_styles();
  });

  function shop_styles() {
    $('.elab_product_view_switcher .elab_grid_1').on('click', function (e) {
      e.preventDefault();
      change_shop_style('.elab_grid_1');
      $('ul.products > li').removeClass('elab_product_view__grid_2 elab_product_view__list').addClass('elab_product_view__grid');
    });
    $('.elab_product_view_switcher .elab_grid_2').on('click', function (e) {
      e.preventDefault();
      change_shop_style('.elab_grid_2');
      $('ul.products > li').removeClass('elab_product_view__list').addClass('elab_product_view__grid elab_product_view__grid_2');
    });
    $('.elab_product_view_switcher .elab_list_1').on('click', function (e) {
      e.preventDefault();
      change_shop_style('.elab_list_1');
      $('ul.products > li').removeClass('elab_product_view__grid elab_product_view__grid_2').addClass('elab_product_view__list');
    });
  }

  function change_shop_style(selector) {
    $('.elab_product_view_switcher a').removeClass('active');
    $(selector).addClass('active');
  }

  function variation_change() {
    var $variations_form = $('.variations_form');
    setTimeout(function () {
      var visible = $('.woocommerce-variation').is(':visible');

      if (visible) {
        $variations_form.addClass('variation_set');
      } else {
        $variations_form.removeClass('variation_set');
      }
    }, 0);
  }
})(jQuery);