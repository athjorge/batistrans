"use strict";

(function ($) {
  $(document).ready(function ($) {
    elabWooSubcategoryCarousel();
    elabAddShowMoreButton(10);
    $('.elab_price_filter .price_slider').on('slidechange', function (event, ui) {
      $('.elab_price_filter .price_slider').closest('form').submit();
    });
    $('.elab_filter_button, .sidebar-overlay').on('click', function (e) {
      e.preventDefault();
      $('.elab_woocommerce_product_archive.full_width').toggleClass('open-sidebar');
    });
  });

  function elabWooSubcategoryCarousel() {
    var $carousel = $('.elab_category_sub_carousel');
    $carousel.owlCarousel({
      items: 6,
      loop: false,
      nav: true,
      dots: false,
      autoplay: true,
      margin: 30,
      autoplayHoverPause: true,
      responsive: {
        0: {
          items: 1
        },
        350: {
          items: 2
        },
        769: {
          items: 3,
          margin: 40
        },
        1024: {
          items: 4
        },
        1400: {
          items: 5
        },
        1600: {
          items: 6
        }
      }
    });
  }

  function elabAddShowMoreButton(limit) {
    $('.woocommerce-widget-layered-nav-list').each(function () {
      var listLength = $(this).find('li').length;

      if (listLength > limit) {
        $(this).attr('data-closed-text', buttonTexts.closed);
        $(this).attr('data-opened-text', buttonTexts.opened);
        $(this).find('li').each(function (i) {
          if (i + 1 > limit) {
            $(this).addClass('overLimit hidden');
          }
        });
        $(this).append('<li class="show-more">' + buttonTexts.closed + '</li>');
      }
    });
    $('.woocommerce-widget-layered-nav-list').on('click', '.show-more', function () {
      var closedText = $(this).parent().attr('data-closed-text');
      var openedText = $(this).parent().attr('data-opened-text');
      $(this).parent().find('.overLimit').toggleClass('hidden');

      if ($(this).text() == closedText) {
        $(this).text(openedText);
      } else {
        $(this).text(closedText);
      }
    });
  }
})(jQuery);