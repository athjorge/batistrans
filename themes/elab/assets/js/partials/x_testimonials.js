"use strict";

(function ($) {
  $(document).ready(function () {
    $('.x_testimonials').each(function () {
      var $this = $(this);
      var module_id = $this.data('module');
      var $carousel = $(".".concat(module_id));
      var per_row = window[module_id]['per_row'];
      var margin = window[module_id]['style'] == 'style_2' ? 20 : 40;
      $carousel.imagesLoaded(function () {
        $carousel.owlCarousel({
          items: per_row,
          loop: false,
          nav: false,
          dots: true,
          margin: margin,
          autoplay: true,
          autoplayHoverPause: true,
          responsive: {
            0: {
              items: 1,
              autoHeight: true
            },
            769: {
              items: per_row,
              autoHeight: false
            }
          }
        });
      });
    });
  });
})(jQuery);