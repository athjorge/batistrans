"use strict";

(function ($) {
  $(document).ready(function () {
    $('.header_woo_categories__toggle').on('click', function () {
      $('.header_woo_categories').toggleClass('active');
    });
    $('.header_woo_categories__overlay').on('click', function () {
      $('.header_woo_categories').removeClass('active');
    });
  });
})(jQuery);