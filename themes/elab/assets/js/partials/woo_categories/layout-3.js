"use strict";

(function ($) {
  $(document).ready(function () {
    $('.header_woo_categories__toggle, .woo-category-burger, .header_woo_categories_overlay').on('click', function () {
      $('.header_woo_categories').toggleClass('active');
      $('.header_woo_categories_overlay').toggleClass('active');
    });
    $('.header_woo_categories .has_child').on('click', function () {
      $(this).parent().find('ul').slideToggle();
    });
  });
})(jQuery);