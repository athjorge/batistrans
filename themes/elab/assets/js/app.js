"use strict";

(function ($) {
  var preloaderEnabled = false;
  $(document).ready(function () {
    if (!$('body').hasClass('woocommerce-checkout')) {
      initNiceSelect();
    }

    widgetList();
    stm_share_url();
    fullwidth();
    toggler();
    menuChilds();
  });
  $(window).load(function () {
    yithChangePopupBehaviour();
    $("body").on('DOMSubtreeModified', "#colorbox", function () {
      var is_visible = $(this).is(":visible");
      if (is_visible) $('body').addClass('compareOnScreen');
      if (!is_visible) $('body').removeClass('compareOnScreen');
    });

    if ($('body').hasClass('elab_enable_preloader')) {
      preloaderEnabled = true;
      window.dispatchEvent(new Event('resize'));
      $('body').removeClass('elab_enable_preloader');
    }
  });
  $(window).resize(function () {
    fullwidth();
  });

  var initNiceSelect = function initNiceSelect() {
    var $selector = $('select');
    $selector.removeAttr('class');
    $selector.niceSelect();
    $selector.on('change', function () {
      setTimeout(function () {
        $selector.niceSelect('update');
      }, 100);
    });
  };

  var widgetList = function widgetList() {
    var selectors = '.widget.widget_nav_menu ul > .menu-item-has-children, .widget_product_categories .product-categories > .cat-parent';
    var $selectors = $(selectors);
    $selectors.each(function () {
      $(this).append('<span class="widget_toggle"><i class="lnricons-plus"></i></span>');
    });
    $selectors.on('click', '.widget_toggle', function () {
      $(this).closest('li').toggleClass('active').find('ul').toggle();
    });
  };

  var stm_share_url = function stm_share_url() {
    $('.stm_js__shareble a').on('click', function (e) {
      e.preventDefault();
      var url = $(this).data('share');
      var social = $(this).data('social') + '_share';
      window.open(url, social, 'width=580,height=296');
    });
  };

  var fullwidth = function fullwidth() {
    $('[data-fullwidth]').each(function () {
      var $this = $(this);
      var $container = $('.container');
      var windowW = $(window).width();
      var elW = $container.width();
      $container.each(function () {
        if ($(this).is(':visible')) {
          elW = $(this).width();
          return false;
        }
      });
      var mgLeft = -(windowW - elW) / 2;
      var data = {
        'width': "".concat(windowW, "px"),
        'margin-left': "".concat(mgLeft, "px")
      };
      $this.css(data);
    });
  };

  var toggler = function toggler() {
    var selector = $('[data-toggle-selector]');
    selector.on('click', function () {
      var open = $(this).attr('data-toggle-selector');
      var $open = $(open);

      if ($open.length) {
        $(this).toggleClass('active');
        $open.toggleClass('active');
      }
    });
  };

  var menuChilds = function menuChilds() {
    var parent = '.stmt-theme-header_menu';
    var $parent = $("".concat(parent, " > .menu-item-has-children"));
    $parent.each(function () {
      $(this).append('<span class="mobile_trigger"></span>');
    });
    $(parent).on('click', '.mobile_trigger', function (e) {
      e.preventDefault();
      e.stopPropagation();
      $(this).closest('li').find('.sub-menu').toggleClass('active');
    });
  };

  var yithChangePopupBehaviour = function yithChangePopupBehaviour() {
    $('body').unbind('yith_woocompare_open_popup');
    $('body').on('yith_woocompare_open_popup', function (e, data) {
      e.stopPropagation();
      var response = data.response;
      $.colorbox({
        href: response,
        iframe: true,
        width: '90%',
        height: '90%',
        className: 'yith_woocompare_colorbox',
        close: yith_woocompare.close_label,
        onClosed: function onClosed() {
          var widget_list = $('.yith-woocompare-widget ul.products-list'),
              data = {
            action: yith_woocompare.actionreload,
            context: 'frontend'
          };

          if (typeof $.fn.block != 'undefined') {
            widget_list.block({
              message: null,
              overlayCSS: {
                background: '#fff url(' + yith_woocompare.loader + ') no-repeat center',
                backgroundSize: '16px 16px',
                opacity: 0.6
              }
            });
          }

          $.ajax({
            type: 'post',
            url: yith_woocompare.ajaxurl.toString().replace('%%endpoint%%', yith_woocompare.actionreload),
            data: data,
            success: function success(response) {
              // add the product in the widget
              if (typeof $.fn.block != 'undefined') {
                widget_list.unblock().html(response);
              }

              widget_list.html(response);
            }
          });
        }
      });
      $(window).resize(function () {
        $.colorbox.resize({
          width: '90%',
          height: '90%'
        });
      });
    });
  };
})(jQuery);