<?php

defined('ABSPATH') || exit;

$id = get_the_ID();

$accessories = get_post_meta($id, 'product_accessories', true);

elab_enqueue_parted_style('accessories', 'woocommerce/');
elab_enqueue_parted_script('accessories', 'woocommerce/');

if (!empty($accessories)):
    $accessories = json_decode($accessories, true);

    array_unshift($accessories, array(
        'name' => get_the_title($id),
        'value' => $id,
    ));

    ?>

    <div class="elab_product_accessories_bundle_wrapper"
         data-currency="<?php echo esc_attr(get_option('woocommerce_currency')); ?>"
         data-currency-pos="<?php echo esc_attr(get_option('woocommerce_currency_pos')); ?>"
         data-sep="<?php echo esc_attr(get_option('woocommerce_price_thousand_sep')); ?>">

        <div class="elab_product_accessories_bundle">
            <?php foreach ($accessories as $accessory):
                $product_id = $accessory['value'];
                $product = wc_get_product($product_id);
                if(!$product) continue;
                $image = elab_get_cropped_image(get_post_thumbnail_id($product_id), 240, 230);
                $reg_price = $product->get_regular_price();
                $sale_price = $product->get_sale_price();
                $price = $product->get_price();

                ?>
                <div class="elab_product_accessories_single">
                    <?php if (!empty($image)) : ?>
                        <a href="<?php echo esc_url(get_the_permalink($product_id)); ?>" class="elab_product_accessories_single__image">
                            <?php echo elab_filtered_output($image); ?>
                        </a>
                    <?php endif; ?>

                    <div class="elab_product_accessories_single__content">

                        <a href="<?php echo esc_url(get_the_permalink($product_id)); ?>" class="elab_product_accessories_single__title heading_font">
                            <?php echo esc_html(get_the_title($product_id)); ?>
                        </a>

                        <div class="elab_product_accessories_single__prices heading_font">

                            <?php if (!empty($sale_price)): ?>
                                <span class="sale_price">
                                    <?php echo wc_price($reg_price); ?>
                                </span>
                            <?php endif; ?>

                            <?php if (!empty($price)): ?>
                                <span class="reg_price">
                                    <?php echo wc_price($price); ?>
                                </span>
                            <?php endif; ?>

                        </div>

                    </div>

                    <?php if ($id !== $product_id): ?>

                        <div class="elab_product_accessories_single__add added heading_font"
                             data-add="<?php echo intval($product_id) ?>"
                             data-price="<?php echo floatval($price); ?>">
                            <?php esc_html_e('Include', 'elab'); ?>
                        </div>

                    <?php else : ?>

                        <div class="elab_product_accessories_single__add elab_product_accessories_single__add_origin added heading_font"
                             data-add="<?php echo intval($product_id) ?>"
                             data-price="<?php echo floatval($price); ?>">
                            <?php esc_html_e('Include', 'elab'); ?>
                        </div>

                    <?php endif; ?>

                </div>

            <?php endforeach; ?>
        </div>

        <div class="elab_product_accessories_bottom heading_font">
            <div class="elab_product_accessories_bottom_info">
                <?php printf(esc_html__('%s for %s item(s)', 'elab'), wc_price($price) . "<span class='sum'></span>", "<span class='num'></span>"); ?>
            </div>
            <button class="btn btn-primary"><?php esc_html_e('+ Add to cart', 'elab'); ?></button>
        </div>

    </div>

<?php endif;