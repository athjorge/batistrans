<?php
/*
 * This file belongs to the YIT Framework.
 *
 * This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

while ( have_posts() ) : the_post();
?>

 <div class="product">

	<div id="product-<?php the_ID(); ?>" <?php post_class('product'); ?>>
        <div class="elab_image_wrap">
            <?php do_action( 'yith_wcqv_product_image' ); ?>
            <?php //do_action('woocommerce_product_thumbnails'); ?>
            <div class="action_buttons">
                <?php if( defined( 'YITH_WOOCOMPARE_VERSION' ) ): ?>
                    <a href="#"
                       class="compare x_compare"
                       data-product_id="<?php the_ID(); ?>"
                       data-tooltip="<?php esc_attr_e( 'Add to compare', 'elab' ); ?>"
                       rel="nofollow">
                        <i class="lnricons-shuffle"></i>
                    </a>
                <?php endif; ?>

                <?php if (defined('YITH_WCWL')):
                    $url = YITH_WCWL()->get_wishlist_url();
                    ?>
                    <div class="yith-wcwl-add-to-wishlist add-to-wishlist-<?php the_ID(); ?>">

                        <div class="yith-wcwl-add-button">

                            <a href="#"
                               rel="nofollow"
                               data-product-id="<?php the_ID(); ?>"
                               class="add_to_wishlist">
                                <i class="lnricons-heart"></i>
                            </a>

                        </div>

                        <div class="yith-wcwl-wishlistexistsbrowse hide" style="display: none;">
                            <a href="<?php echo esc_url($url); ?>" rel="nofollow">
                                <i class="lnricons-heart"></i>
                            </a>
                        </div>

                    </div>

                <?php endif; ?>
                <?php
                if (function_exists('stm_get_shares')):
                    stm_get_shares();
                endif;
                ?>
            </div>
        </div>

		<div class="summary entry-summary">
			<div class="summary-content">
				<?php do_action( 'yith_wcqv_product_summary' ); ?>
			</div>
		</div>

	</div>

</div>

<?php endwhile; // end of the loop.