<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

// Ensure visibility.
if (empty($product) || !$product->is_visible()) {
    return;
}

$gallery = $product->get_gallery_image_ids();

$gallery_image = (!empty($gallery[0])) ? elab_get_cropped_image_url($gallery[0], '270', '270') : '';

$product_view = elab_get_option( 'shop_product_view', 'grid' );

$classes = array(
    'grid' => 'x_product_buttons_wrapper elab_woo_image_wrapper elab_product_view__grid',
    'grid_2' => 'x_product_buttons_wrapper elab_woo_image_wrapper elab_product_view__grid elab_product_view__grid_2',
    'list' => 'x_product_buttons_wrapper elab_woo_image_wrapper elab_product_view__list',
);
if(!empty($_GET['product_view']) && !empty($classes[$_GET['product_view']])){
    $product_view = sanitize_text_field($_GET['product_view']);
}
?>
<li <?php wc_product_class($classes[$product_view]); ?>>
    <?php
    /**
     * Hook: woocommerce_before_shop_loop_item.
     *
     * @hooked woocommerce_template_loop_product_link_open - 10
     */
    //do_action('woocommerce_before_shop_loop_item'); ?>

    <div class="elab_woocommerce_content_image elab_woocoommerce_product_image">

        <a href="<?php echo esc_url(get_permalink()); ?>">

            <?php get_template_part('partials/woocommerce/content-product-labels'); ?>

            <?php wc_get_template('loop/sale-flash.php'); ?>

            <?php if (!empty($gallery_image)): ?>
                <div class="elab_woo_image_hover">
                    <?php do_action('woocommerce_before_shop_loop_item_title'); ?>
                    <img src="<?php echo elab_filtered_output(elab_get_cropped_image_url($gallery[0], '270', '270')); ?>"/>
                </div>
            <?php else: ?>
                <?php do_action('woocommerce_before_shop_loop_item_title'); ?>
            <?php endif; ?>
        </a>

    </div>

    <div class="elab_woocoommerce_product_content">
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="elab_product_title">
            <?php do_action('woocommerce_shop_loop_item_title'); ?>
        </a>
        <?php wc_get_template('loop/price.php'); ?>
        <?php if (!empty(get_the_excerpt())): ?>
            <div class="elab-product-excerpt">
                <?php echo get_the_excerpt(); ?>
            </div>
        <?php endif; ?>
        <div class="elab-product-rating">
            <div class="rating-wrap">
                <?php wc_get_template('loop/rating.php'); ?>
            </div>
            <?php
            if (get_option('woocommerce_enable_review_rating') === 'yes' && ($count = $product->get_review_count())): ?>
                <a href="<?php the_permalink(); ?>#tab-reviews" class="review_count">
                    <?php
                    printf(esc_html(_n('%1$s review', '%1$s reviews', $count, 'elab')), esc_html($count));
                    ?>
                </a>
            <?php endif; ?>
        </div>
        <div class="product_actions">
            <?php get_template_part('partials/woocommerce/product_buttons/style_1'); ?>
        </div>
    </div>

    <div class="x_product_buttons">
        <?php get_template_part('partials/woocommerce/product_buttons'); ?>
    </div>

    <?php get_template_part('partials/woocommerce/shop_styles/product-list'); ?>

</li>