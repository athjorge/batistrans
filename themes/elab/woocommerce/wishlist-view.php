<?php
/**
 * Wishlist page template
 *
 * @author Your Inspiration Themes
 * @package YITH WooCommerce Wishlist
 * @version 2.0.12
 */

if (!defined('YITH_WCWL')) {
    exit;
} // Exit if accessed directly
?>


<form id="yith-wcwl-form" action="<?php echo esc_attr($form_action); ?>" method="post"
      class="woocommerce woocommerce-cart">


    <table class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
        <thead>
        <tr>
            <th class="product-thumbnail">&nbsp;</th>
            <th class="product-name"><?php esc_html_e('Product', 'elab'); ?></th>
            <th class="product-price"><?php esc_html_e('Price', 'elab'); ?></th>
            <th class="product-buy"><?php esc_html_e('Buy', 'elab'); ?></th>
            <th class="product-remove">&nbsp;</th>
        </tr>
        </thead>

        <tbody>
        <?php
        if (count($wishlist_items) > 0) :
            $added_items = array();
            foreach ($wishlist_items as $item) :
                global $product;

                $item['prod_id'] = yit_wpml_object_id($item['prod_id'], 'product', true);

                if (in_array($item['prod_id'], $added_items)) {
                    continue;
                }

                $added_items[] = $item['prod_id'];
                $product = wc_get_product($item['prod_id']);
                $availability = $product->get_availability();
                $stock_status = isset($availability['class']) ? $availability['class'] : false;

                if ($product && $product->exists()) :
                    ?>
                    <tr id="yith-wcwl-row-<?php echo esc_attr($item['prod_id']) ?>"
                        data-row-id="<?php echo esc_attr($item['prod_id']); ?>">


                        <td class="product-thumbnail">
                            <a href="<?php echo esc_url(get_permalink(apply_filters('woocommerce_in_cart_product', $item['prod_id']))) ?>">
                                <?php echo elab_filtered_output($product->get_image()); ?>
                            </a>
                        </td>

                        <td class="product-name" data-title="<?php esc_attr_e('Name', 'elab'); ?>">
                            <a href="<?php echo esc_url(get_permalink(apply_filters('woocommerce_in_cart_product', $item['prod_id']))) ?>"><?php echo apply_filters('woocommerce_in_cartproduct_obj_title', $product->get_title(), $product) ?></a>
                            <?php do_action('yith_wcwl_table_after_product_name', $item); ?>
                        </td>


                        <td class="product-price" data-title="<?php esc_attr_e('Price', 'elab'); ?>">
                            <?php if ($show_price) :
                                $base_product = $product->is_type('variable') ? $product->get_variation_regular_price('max') : $product->get_price();
                                echo elab_filtered_output($base_product) ? $product->get_price_html() : apply_filters('yith_free_text', esc_html__('Free!', 'elab'), $product);
                            endif ?>
                        </td>


                        <td class="product-buy" data-title="<?php esc_attr_e('Buy', 'elab'); ?>">
                            <!-- Add to cart button -->
                            <?php if ($show_add_to_cart && isset($stock_status) && $stock_status != 'out-of-stock'): ?>
                                <?php woocommerce_template_loop_add_to_cart(); ?>
                            <?php else: ?>
                                <?php esc_html_e('Not available', 'elab'); ?>
                            <?php endif ?>
                        </td>

                        <td class="product-remove" data-title="<?php esc_attr_e('Remove', 'elab'); ?>">
                            <?php if ($is_user_owner): ?>
                                <div>
                                    <a href="<?php echo esc_url(add_query_arg('remove_from_wishlist', $item['prod_id'])) ?>"
                                       class="remove remove_from_wishlist"
                                       title="<?php echo apply_filters('yith_wcwl_remove_product_wishlist_message_title', esc_attr__('Remove this product', 'elab')); ?>">&times;</a>
                                </div>
                            <?php else: ?>
                                <?php esc_html_e('Not available', 'elab'); ?>
                            <?php endif; ?>
                        </td>


                    </tr>
                <?php
                endif;
            endforeach;
        else: ?>
            <tr>
                <td colspan="5"
                    class="wishlist-empty"><?php echo apply_filters('yith_wcwl_no_product_to_remove_message', esc_html__('No products were added to the wishlist', 'elab')) ?></td>
            </tr>
        <?php
        endif;

        if (!empty($page_links)) : ?>
            <tr class="pagination-row">
                <td colspan="<?php echo esc_attr($column_count) ?>"><?php echo elab_filtered_output($page_links); ?></td>
            </tr>
        <?php endif ?>
        </tbody>

    </table>


</form>
