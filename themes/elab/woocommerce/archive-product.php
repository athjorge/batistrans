<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

$have_sidebar = is_active_sidebar( 'shop' );

$shop_page_style = elab_get_option( 'shop_page_style', 'default' );

if(!empty($shop_page_style)){
    $shop_page_style_class = $shop_page_style;
}


if( isset( $_GET[ 'shop_page_style' ] ) && !empty( $_GET[ 'shop_page_style' ] ) ) {
    $shop_page_style = sanitize_text_field( $_GET[ 'shop_page_style' ] );
    $shop_page_style_class = sanitize_text_field( $_GET[ 'shop_page_style' ] );
}

$product_view = elab_get_option( 'shop_product_view', 'grid' );
if( !empty( $_GET[ 'product_view' ] ) ) {
    $product_view = sanitize_text_field( $_GET[ 'product_view' ] );
}

if(!empty($product_view)){
    $shop_page_style_class .= ' product-view__' . $product_view;
}

$queried_obj = get_queried_object();
$term_id = ( !empty( $queried_obj->term_id ) ) ? $queried_obj->term_id : '';
$shop_banner = elab_get_banner_id( $term_id );

//$shop_page_style_class = ($shop_banner)

if( $shop_banner ) {
    $shop_page_style_class .= ' with_banner';
} else {
    $shop_page_style_class .= ' without_banner';
}

$products_column_class = 'col-xl-9 col-lg-8 col-md-12';

if( $shop_page_style == 'full_width' ) {
    $products_column_class = 'col-xl-12 col-lg-12 col-md-12 full_width_column';
}

get_header(); ?>

    <div class="elab_woocommerce_product_archive <?php echo esc_attr( $shop_page_style_class ); ?>">
        <div class="container">

            <?php get_template_part( 'partials/titlebox/main' ); ?>

            <?php if( $have_sidebar ): ?>

                <div class="row">
                    <div class="col-xl-3 col-lg-4 col-md-12 elab-sidebar_column">
                        <?php
                        /**
                         * Hook: woocommerce_sidebar.
                         *
                         * @hooked woocommerce_get_sidebar - 10
                         */
                        do_action( 'woocommerce_sidebar' );
                        ?>
                    </div>

                    <div class="<?php echo esc_attr( $products_column_class ); ?>">

                        <?php get_template_part( 'partials/archive/product/content' ); ?>

                    </div>
                    <?php if( $shop_page_style == 'full_width' ): ?>
                    <div class="sidebar-overlay"></div>
                    <?php endif; ?>
                </div>

            <?php else: ?>

                <?php get_template_part( 'partials/archive/product/content' ); ?>

            <?php endif; ?>

        </div>
    </div>


<?php get_footer( 'shop' );