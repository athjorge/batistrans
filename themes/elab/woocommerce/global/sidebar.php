<?php
/**
 * Sidebar
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/sidebar.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     1.6.4
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
?>


    <div class="woocommerce-sidebar-toggler heading_font" data-toggle-selector=".woocommerce-sidebar">
        <i class="lnricons-equalizer"></i>
    </div>

    <div class="woocommerce-sidebar">
        <div class="woocommerce-sidebar-inner">
            <div class="woocommerce-sidebar-close" data-toggle-selector=".woocommerce-sidebar">
                <i class="lnricons-cross2"></i>
            </div>
            <?php dynamic_sidebar('shop'); ?>
        </div>
        <div class="woocommerce-sidebar-overlay" data-toggle-selector=".woocommerce-sidebar"></div>
    </div>

<?php /* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */