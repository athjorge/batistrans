<?php
/*Require TGM CLASS*/
require_once $stmt_inc_path . '/admin/tgm/class-tgm-plugin-activation.php';

/*Register plugins to activate*/
add_action('tgmpa_register', 'elab_require_plugins');

function elab_require_plugins($return = false)
{
    $plugins_path = "https://elab.stylemixthemes.com/plugins";

    $plugins = array(
        'x-builder' => array(
            'name' => 'X Builder',
            'slug' => 'x-builder',
            'source' => $plugins_path . '/x-builder.zip?v=' . time(),
            'required' => true,
            'external_url' => 'https://stylemixthemes.com/'
        ),
        'stm-configurations' => array(
            'name' => 'STM Configurations',
            'source' => $plugins_path . '/stm-configurations.zip?v=' . time(),
            //'source' => elab_get_package('stm-configurations', 'zip'),
            'slug' => 'stm-configurations',
            'required' => true,
            'version' => '1.0.1',
            'external_url' => 'https://stylemixthemes.com/'
        ),
        'woocommerce' => array(
            'name' => 'WooCommerce',
            'slug' => 'woocommerce',
            'required' => true,
        ),
        'yith-woocommerce-compare' => array(
            'name' => 'YITH WooCommerce Compare',
            'slug' => 'yith-woocommerce-compare',
            'required' => false,
        ),
        'yith-woocommerce-wishlist' => array(
            'name' => 'YITH WooCommerce Wishlist',
            'slug' => 'yith-woocommerce-wishlist',
            'required' => false,
        ),
        'yith-woocommerce-quick-view' => array(
            'name' => 'YITH WooCommerce Quick View',
            'slug' => 'yith-woocommerce-quick-view',
            'required' => false,
        ),
        'revslider' => array(
            'name' => 'Revolution Slider',
            'slug' => 'revslider',
            'source' => elab_get_package('revslider', 'zip'),
            'version' => '6.1.0',
            'required' => false,
            'external_url' => 'http://www.themepunch.com/revolution/',
        ),
        'contact-form-7' => array(
            'name' => 'Contact Form 7',
            'slug' => 'contact-form-7',
            'required' => false,
        ),
        'breadcrumb-navxt' => array(
            'name' => 'Breadcrumb NavXT',
            'slug' => 'breadcrumb-navxt',
            'required' => false,
        ),
        'pearl-header-builder' => array(
            'name' => 'Pearl Header Builder',
            'slug' => 'pearl-header-builder',
            'required' => true
        ),
        'dokan-lite' => array(
            'name' => 'Dokan Lite',
            'slug' => 'dokan-lite',
            'required' => false
        ),
        'wc-vendors' => array(
            'name' => 'WC Vendors Marketplace',
            'slug' => 'wc-vendors',
            'required' => false
        ),
    );

    if ($return) {
        return $plugins;
    } else {
        $config = array(
            'id' => 'elab_mine',
        );

        $layout_plugins = elab_layout_plugins(elab_get_theme_layout());
        $recommended_plugins = elab_recommended_plugins();
        $layout_plugins = array_merge($layout_plugins, $recommended_plugins);

        $tgm_layout_plugins = array();
        foreach ($layout_plugins as $layout_plugin) {
            $tgm_layout_plugins[$layout_plugin] = $plugins[$layout_plugin];
        }

        tgmpa($tgm_layout_plugins, $config);
    }
}