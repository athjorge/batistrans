<?php
add_action( 'admin_init', 'elab_init_default_settings', 1 );

function elab_init_default_settings() {
    $options = elab_stored_theme_options();

    $force = (!empty($_GET['force_reset_theme_options'])) ? true : false;

    if(empty($options) or $force) {

        $settings = ELAB_PATH . "/inc/admin/init/default_settings.json";
        ob_start();
        require_once($settings);
        $options = json_decode(ob_get_clean(), true);

        if(!empty($options) and is_array($options)) update_option('stmt_to_settings', $options);

        require_once( get_template_directory() . '/inc/admin/css/css-php.php' );
        elab_generate_styles();

    }
}

add_action('init', 'elab_remove_woo_redirect', 10);

function elab_remove_woo_redirect() {
    delete_transient( '_wc_activation_redirect' );
}