<?php

if (!defined('ABSPATH')) exit; //Exit if accessed directly

add_action('init', 'elab_generate_styles');

function elab_generate_styles()
{

    global $wp_filesystem;

    if (empty($wp_filesystem)) {
        require_once ABSPATH . '/wp-admin/includes/file.php';
        WP_Filesystem();
    }

    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/stm_configurations_styles';
    if (!$wp_filesystem->is_dir($upload_dir)) {
        wp_mkdir_p($upload_dir);
    }

    $layout = elab_get_theme_layout();

    ob_start();
    require_once get_template_directory() . '/inc/admin/css/theme_options/global.php';
    require_once get_template_directory() . '/inc/admin/css/theme_options/typography.php';
    require_once get_template_directory() . '/inc/admin/css/theme_options/header.php';
    require_once get_template_directory() . '/inc/admin/css/theme_options/footer.php';
    require_once get_template_directory() . '/inc/admin/css/theme_options/layout.php';
    require_once get_template_directory() . '/inc/admin/css/theme_options/post.php';
    require_once get_template_directory() . '/inc/admin/css/theme_options/widgets.php';
    require_once get_template_directory() . '/inc/admin/css/theme_options/buttons.php';
    require_once get_template_directory() . "/inc/admin/css/layouts/{$layout}/styles.php";

    if (class_exists('WooCommerce')) {
        require_once get_template_directory() . '/inc/admin/css/theme_options/woo.php';
    }

    $css = ob_get_clean();

    $wp_filesystem->put_contents("{$upload_dir}/styles.css", (elab_remove_spaces($css)), FS_CHMOD_FILE);
    elab_update_styles_version();

}

function elab_update_styles_version()
{
    $version = intval(get_option('elab_styles_v', 1));
    update_option('elab_styles_v', $version += 1);
}

function elab_remove_spaces($text)
{
    $text = preg_replace('/[\t\n\r\0\x0B]/', '', $text);
    $text = preg_replace('/([\s])\1+/', ' ', $text);
    $text = trim($text);
    return $text;
}

function elab_element_styles($style_name, $style)
{
    switch ($style_name) {
        case 'font-family':
            $style = "'{$style}'";
            break;
        case 'margin_bottom':
            $style_name = 'margin-bottom';
            break;
        case 'margin_top':
            $style_name = 'margin-top';
            break;
    }
    return "{$style_name} : {$style}";
}