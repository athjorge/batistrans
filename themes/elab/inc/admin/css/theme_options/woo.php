<?php
$to = get_option('stmt_to_settings', array());

//Colors
$primary_color = ( !empty( $to['primary_color'] ) ) ? $to['primary_color'] : '#ffffff';
$secondary_color = ( !empty( $to['secondary_color'] ) ) ? $to['secondary_color'] : '#ffffff';
$third_color = ( !empty( $to['third_color'] ) ) ? $to['third_color'] : '#ffffff';

if (!empty($to['default_header_font_family'])): ?>
    .widget.woocommerce.widget_products ul.product_list_widget li a .product-title,
    .widget.woocommerce.widget_products ul.product_list_widget li:before {
        font-family: "<?php echo sanitize_text_field($to['default_header_font_family']); ?>";
    }
<?php endif; ?>

.widget.woocommerce.widget_product_search button[type=submit] {
    background-color : <?php echo sanitize_text_field($secondary_color); ?>
}