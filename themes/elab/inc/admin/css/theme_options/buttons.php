<?php
$to = get_option('stmt_to_settings', array());

$primary_color = ( !empty( $to['primary_color'] ) ) ? $to['primary_color'] : '#ffffff';
$secondary_color = ( !empty( $to['secondary_color'] ) ) ? $to['secondary_color'] : '#ffffff';
$third_color = ( !empty( $to['third_color'] ) ) ? $to['third_color'] : '#ffffff';

?>

.btn-outline-primary {
    border-color: <?php echo esc_attr($primary_color); ?>;
    color: <?php echo esc_attr($primary_color); ?>;
}

.btn-outline-primary:hover {
    border-color: <?php echo esc_attr($primary_color); ?>;
    background-color: <?php echo esc_attr($primary_color); ?>;
    color: #fff;
}