<?php
$to = get_option( 'stmt_to_settings', array() );

//Colors
$primary_color = ( !empty( $to['primary_color'] ) ) ? $to['primary_color'] : '#ffffff';
$secondary_color = ( !empty( $to['secondary_color'] ) ) ? $to['secondary_color'] : '#ffffff';
$third_color = ( !empty( $to['third_color'] ) ) ? $to['third_color'] : '#ffffff';

//Fonts
$secondary_font = ( !empty( $to['default_header_font_family'] ) ) ? $to['default_header_font_family'] : 'sans-serif';

?>

.widget {
    border-bottom-color: <?php echo esc_attr( $primary_color ); ?>;
}

.search-form .search-submit {
    background-color: <?php echo esc_attr( $third_color ); ?>;
}

.widget.widget_recent_entries ul li a {
    color: <?php echo esc_attr( $primary_color ); ?>;
}

.widget.widget_recent_entries ul li a:hover {
    color: <?php echo esc_attr( $third_color ); ?>;
}

.widget.widget_recent_entries ul li a:hover:before {
    background-color: <?php echo esc_attr( $secondary_color ); ?> !important;
}

.widget.widget_recent_comments ul li a,
.widget.widget_archive ul li a {
    color: <?php echo esc_attr( $primary_color ); ?>;
}

.widget.widget_recent_comments ul li:before,
.widget.widget_recent_comments ul li a:hover,
.widget.widget_archive ul li a:hover {
    color: <?php echo esc_attr( $third_color ); ?>;
}
.widget.widget_archive ul li:before {
    color: <?php echo esc_attr( $secondary_color ); ?>;
}
.site-content .widget_calendar table thead {
    border-top-color: <?php echo esc_attr( $secondary_color ); ?>;
}
#wp-calendar tbody td#today {
    border-color: <?php echo esc_attr( $secondary_color ); ?>;
}
.widget.widget_calendar #wp-calendar tbody td a:hover,
.widget.widget_calendar #wp-calendar tbody th a:hover {
    background-color: <?php echo esc_attr( $third_color ); ?>;
}

.widget.widget_rss a {
    color: <?php echo esc_attr( $primary_color ); ?>;
}
.widget.widget_rss a:hover {
    color: <?php echo esc_attr( $third_color ); ?>;
}

