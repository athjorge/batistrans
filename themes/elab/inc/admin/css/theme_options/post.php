<?php
$to = get_option( 'stmt_to_settings', array() );

//Colors
$primary_color = ( !empty( $to['primary_color'] ) ) ? $to['primary_color'] : '#ffffff';
$secondary_color = ( !empty( $to['secondary_color'] ) ) ? $to['secondary_color'] : '#ffffff';
$third_color = ( !empty( $to['third_color'] ) ) ? $to['third_color'] : '#ffffff';

//Fonts
$secondary_font = ( !empty( $to['default_header_font_family'] ) ) ? $to['default_header_font_family'] : 'sans-serif';
?>

.post_archive_style_1 .stmt-theme_post_grid__inner .posted-on {
    background-color: <?php echo esc_attr( $secondary_color ); ?>;
}

.elab_list_news.sticky:before,
.post_archive_style_1 .stmt-theme_post_grid__inner .post-title h2:hover {
    color: <?php echo esc_attr( $third_color ); ?>;
}

.wp-audio-shortcode .mejs-inner .mejs-controls .mejs-playpause-button,
.wp-audio-shortcode .mejs-inner .mejs-controls .mejs-playpause-button:after {
    background-color: <?php echo esc_attr( $secondary_color ); ?>;
}
.wp-audio-shortcode .mejs-inner .mejs-controls .mejs-time span,
.wp-audio-shortcode .mejs-inner .mejs-controls .mejs-volume-button.mejs-mute button:before{
    color: <?php echo esc_attr( $primary_color ); ?>;
}
.wp-audio-shortcode .mejs-inner .mejs-controls .mejs-time-rail .mejs-time-total:after,
.wp-audio-shortcode .mejs-inner .mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-total:after {
    background-color: <?php echo esc_attr( $primary_color ); ?>;
}
.wp-audio-shortcode .mejs-inner .mejs-controls .mejs-time-rail .mejs-time-total .mejs-time-current,
.wp-audio-shortcode .mejs-inner .mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-total .mejs-horizontal-volume-current {
    background-color: <?php echo esc_attr( $third_color ); ?>;
}