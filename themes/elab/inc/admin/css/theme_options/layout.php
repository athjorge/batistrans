<?php
$to = get_option('stmt_to_settings', array());

if (!empty($to['content_max_width'])): ?>
    .container {
        max-width: <?php echo sanitize_text_field($to['content_max_width']); ?>;
    }
<?php endif;