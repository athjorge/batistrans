<?php
$to = get_option( 'stmt_to_settings', array() );

//Colors
//global
$primary_color = ( !empty( $to['primary_color'] ) ) ? $to['primary_color'] : 'inherit';
$secondary_color = ( !empty( $to['secondary_color'] ) ) ? $to['secondary_color'] : 'inherit';
$third_color = ( !empty( $to['third_color'] ) ) ? $to['third_color'] : 'inherit';
//footer
$footer_main_bg_color = ( !empty( $to['footer_main_bg'] ) ) ? $to['footer_main_bg'] : $primary_color;
$footer_main_text_color = ( !empty( $to['footer_main_text_color'] ) ) ? $to['footer_main_text_color'] : 'rgba(225,225,225, 0.5)';
$footer_main_links_color = ( !empty( $to['footer_main_links_color'] ) ) ? $to['footer_main_links_color'] : 'rgba(225,225,225, 0.5)';
$footer_main_links_action_color = ( !empty( $to['footer_main_links_action_color'] ) ) ? $to['footer_main_links_action_color'] : 'rgba(225,225,225, 1)';
//menu
$footer_menu_links_color = ( !empty( $to['footer_menu_links_color'] ) ) ? $to['footer_menu_links_color'] : $footer_main_links_color;
$footer_main_links_action_color = ( !empty( $to['footer_main_links_action_color'] ) ) ? $to['footer_main_links_action_color'] : $footer_main_links_action_color;
//copyright
$copyright_bg_color = ( !empty( $to['copyright_bg_color'] ) ) ? $to['copyright_bg_color'] : 'transparent';
$copyright_text_color = ( !empty( $to['copyright_text_color'] ) ) ? $to['copyright_text_color'] : $footer_main_text_color;
$copyright_links_color = ( !empty( $to['copyright_links_color'] ) ) ? $to['copyright_links_color'] : $footer_main_links_color;
$copyright_links_action_color = ( !empty( $to['copyright_links_action_color'] ) ) ? $to['copyright_links_action_color'] : $footer_main_links_action_color;
$copyright_border_line_color = ( !empty( $to['copyright_border_line_color'] ) ) ? $to['copyright_border_line_color'] : $footer_main_text_color;

//Background Image
$background_image = elab_get_option( 'footer_main_bg_image' );
$background_size = ( !empty( $to['footer_main_bg_image_size'] ) ) ? $to['footer_main_bg_image_size'] : 'contain';
if( !empty( $background_image ) ) {
    $background_image = elab_get_image_url( $background_image );
}
?>

footer.site-footer {
    <?php if( !empty( $background_image ) ) : ?>
    background: url("<?php echo esc_attr( $background_image ); ?>") no-repeat 50% 50%;
    background-size: <?php echo esc_attr( $background_size ); ?>;
    <?php endif; ?>
	background-color: <?php echo esc_attr( $footer_main_bg_color ); ?>;
	color: <?php echo esc_attr( $footer_main_text_color ); ?>;
}
footer.site-footer a {
    color: <?php echo esc_attr( $footer_main_links_color ); ?>;
}
footer.site-footer a:hover,
footer.site-footer a:focus,
footer.site-footer a:active {
    color: <?php echo esc_attr( $footer_main_links_action_color ); ?>;
}

footer.site-footer .footer_menu_box a {
    color: <?php echo esc_attr( $footer_menu_links_color ); ?>;
}
footer.site-footer .footer_menu_box a:hover,
footer.site-footer .footer_menu_box a:focus,
footer.site-footer .footer_menu_box a:active {
    color: <?php echo esc_attr( $copyright_links_action_color ); ?>;
}
footer.site-footer .footer_menu_box .footer-menu li:before {
    background-color: <?php echo esc_attr( $footer_menu_links_color ); ?>;
}

footer.site-footer .copyright_box {
    background-color: <?php echo esc_attr( $copyright_bg_color ); ?>;
    color: <?php echo esc_attr( $copyright_text_color ); ?>;
}
footer.site-footer .copyright_box .copyright_border_line {
    background-color: <?php echo esc_attr( $copyright_border_line_color ); ?>;
}
footer.site-footer .copyright_box a {
    color: <?php echo esc_attr( $copyright_links_color ); ?>;
}
footer.site-footer .copyright_box a:hover,
footer.site-footer .copyright_box a:focus,
footer.site-footer .copyright_box a:active {
    color: <?php echo esc_attr( $copyright_links_action_color ); ?>;
}

<?php if(!empty($footer_top_padding)): ?>
    .widgets_box {
        padding-top: <?php echo esc_attr($footer_top_padding); ?>px;
    }
<?php endif; ?>

<?php if(!empty($footer_bottom_padding)): ?>
    .widgets_box {
        padding-bottom: <?php echo esc_attr($footer_bottom_padding); ?>px;
    }
<?php endif; ?>