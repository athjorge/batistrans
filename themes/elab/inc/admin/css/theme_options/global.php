<?php
$to = get_option( 'stmt_to_settings', array() );

//Colors
$primary_color = ( !empty( $to['primary_color'] ) ) ? $to['primary_color'] : '#ffffff';
$secondary_color = ( !empty( $to['secondary_color'] ) ) ? $to['secondary_color'] : '#ffffff';
$third_color = ( !empty( $to['third_color'] ) ) ? $to['third_color'] : '#ffffff';

//Fonts
$secondary_font = ( !empty( $to['default_header_font_family'] ) ) ? $to['default_header_font_family'] : 'sans-serif';

//Background colors
$body_background_color = ( !empty( $to['body_background_color'] ) ) ? $to['body_background_color'] : '#ffffff';

?>

.stc, .stc_hv:hover {
    color: <?php echo esc_attr($secondary_color); ?>
}

.ttc, .ttc_hv:hover, .ttc_hv.active {
	color: <?php echo esc_attr($third_color); ?>
}

.tbrc, .tbrc_b:before, .tbrc_a:after {
	border-color: <?php echo esc_attr($third_color); ?>
}

body.error404 #content.site-content,
.sbc_a:after,
.sbc_b:before,
.sbc {
    background-color: <?php echo esc_attr($secondary_color); ?>
}

body {
    background-color: <?php echo esc_attr( $body_background_color ); ?>;
}

a {
    color: <?php echo esc_attr( $third_color ); ?>;
}

a:hover {
    color: <?php echo esc_attr( $third_color ); ?>;
}

.site-content blockquote {
    border-left-color: <?php echo esc_attr( $secondary_color ); ?>;
}

.site-content table thead {
    border-top-color: <?php echo esc_attr( $third_color ); ?>;
}

.site-content ul li:before {
    border-color: <?php echo esc_attr( $third_color ); ?>;
    background-color: <?php echo esc_attr( $third_color ); ?>;
}

.site-content dl dt:before {
    color: <?php echo esc_attr( $third_color ); ?>;
}

.pagination li .current {
    background-color: <?php echo esc_attr( $third_color ); ?>;
}

.pagination li a:hover {
    background-color: <?php echo esc_attr( $secondary_color ); ?>;
}

input[type=submit] {
    background-color: <?php echo esc_attr( $third_color ); ?>;
}

input[type=submit]:hover {
    background-color: <?php echo esc_attr( $secondary_color ); ?>;
}

html body .site-header ul li.stm_megamenu>ul.sub-menu>li ul.sub-menu>li:not(.megamenu-second-level)>a:hover {
    color: <?php echo esc_attr( $secondary_color); ?> !important;
}