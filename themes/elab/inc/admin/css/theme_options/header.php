<?php
$to = get_option( 'stmt_to_settings', array() );

//Colors
$primary_color = ( !empty( $to['primary_color'] ) ) ? $to['primary_color'] : '#ffffff';
$secondary_color = ( !empty( $to['secondary_color'] ) ) ? $to['secondary_color'] : '#ffffff';
$third_color = ( !empty( $to['third_color'] ) ) ? $to['third_color'] : '#ffffff';

//Fonts
$secondary_font = ( !empty( $to['default_header_font_family'] ) ) ? $to['default_header_font_family'] : 'sans-serif';

//Background colors
$header_main_bg_color = ( !empty( $to['header_main_bg_color'] ) ) ? $to['header_main_bg_color'] : '#ffffff';

?>

header.site-header {
    background-color: <?php echo esc_attr( $header_main_bg_color ); ?>;
}
ul.stmt-theme-header_menu>li:before {
    background-color: <?php echo esc_attr( $third_color ); ?>;
}
ul.stmt-theme-header_menu>li:hover > a {
    color: #ffffff;
}