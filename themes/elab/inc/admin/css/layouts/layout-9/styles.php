<?php
$to = get_option('stmt_to_settings', array());

//Colors
$primary_color = (!empty($to['primary_color'])) ? $to['primary_color'] : '#ffffff';
$secondary_color = (!empty($to['secondary_color'])) ? $to['secondary_color'] : '#ffffff';
$third_color = (!empty($to['third_color'])) ? $to['third_color'] : '#ffffff';

if( !empty($to['default_header_font_family']) ){
    $heading_font_family = $to['default_header_font_family'];
}
else {
    $heading_font_family = 'Poppins';
}
?>
