body {
    outline: none;
}

.site-footer .widget_nav_menu ul li {
    line-height: 13px;
}

.site-footer .widget_nav_menu ul li a {
    font-size: 13px;
}

.site-footer .widget_contacts ul.widget_contacts li .text {
    font-size: 13px;
}

.widget.widget_product_categories ul li a {
    margin-right : 35px;
}

@media (max-width: 1200px) and (min-width: 993px) {
    .widget_contacts ul.widget_contacts li .icon {
        display:none;
    }
}