<?php
$to = get_option('stmt_to_settings', array());

//Colors
$primary_color = (!empty($to['primary_color'])) ? $to['primary_color'] : '#ffffff';
$secondary_color = (!empty($to['secondary_color'])) ? $to['secondary_color'] : '#ffffff';
$third_color = (!empty($to['third_color'])) ? $to['third_color'] : '#ffffff';

//Fonts
$secondary_font = ( !empty( $to['default_header_font_family'] ) ) ? $to['default_header_font_family'] : 'sans-serif';

?>

body ul.stmt-theme-header_menu>li>a {
    font-family : <?php echo sanitize_text_field($secondary_font); ?>;
}

.site-footer .widget_nav_menu ul li {
    line-height: 13px;
}

.site-footer .widget_nav_menu ul li a {
    font-size: 13px;
}

.site-footer .widget_contacts ul.widget_contacts li .text {
    font-size: 13px;
}

.widget.widget_product_categories ul li a {
    margin-right : 35px;
}