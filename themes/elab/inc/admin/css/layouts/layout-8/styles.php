<?php
$to = get_option('stmt_to_settings', array());

if( !empty($to['default_header_font_family']) ){
    $heading_font_family = $to['default_header_font_family'];
}
else {
    $heading_font_family = 'Poppins';
}
?>

body {
    outline: none;
}

.site-footer .widget_nav_menu ul li {
    line-height: 13px;
}

.site-footer .widget_nav_menu ul li a {
    font-size: 13px;
}

.site-footer .widget_contacts ul.widget_contacts li .text {
    font-size: 13px;
}

.widget.widget_product_categories ul li a {
    margin-right : 35px;
}

@media (max-width: 1200px) and (min-width: 993px) {
    .widget_contacts ul.widget_contacts li .icon {
        display:none;
    }
}

body.elab_header_style_7 .main-header .stmt-theme-header_menu a {
    font-family: <?php echo sanitize_text_field($heading_font_family); ?>;
}