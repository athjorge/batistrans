<?php
$to = get_option('stmt_to_settings', array());

//Colors
$primary_color = (!empty($to['primary_color'])) ? $to['primary_color'] : '#ffffff';
$secondary_color = (!empty($to['secondary_color'])) ? $to['secondary_color'] : '#ffffff';
$third_color = (!empty($to['third_color'])) ? $to['third_color'] : '#ffffff';

?>

.header-simple .bottom-bar {
    background-color: <?php echo esc_attr($secondary_color); ?>;
}

body.elab-layout-2 .default.x_best_sellers .x_best_sellers__category {
    background-color: <?php echo esc_attr($third_color); ?>;
}