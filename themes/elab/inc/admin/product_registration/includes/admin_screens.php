<?php
//Register scripts and styles for admin pages
function elab_startup_styles()
{
    wp_enqueue_style('stm-startup_css', get_template_directory_uri() . '/inc/admin/product_registration/assets/css/style.css', null, 1.6, 'all');
}

add_action('admin_enqueue_scripts', 'elab_startup_styles');

//Register Startup page in admin menu
function elab_register_startup_screen()
{
    $theme = elab_get_theme_info();
    $theme_name = $theme['name'];
    $theme_name_sanitized = 'elab';

    /*Item Registration*/
    add_menu_page(
        $theme_name,
        esc_html__('Elab', 'elab'),
        'manage_options',
        $theme_name_sanitized,
        'elab_theme_admin_page_functions',
        get_template_directory_uri() . '/inc/admin/product_registration/assets/img/icon.png',
        '2.1111111111'
    );

    /*Demo Import*/
    add_submenu_page(
        $theme_name_sanitized,
        esc_html__('Demo import', 'elab'),
        esc_html__('Demo import', 'elab'),
        'manage_options',
        $theme_name_sanitized . '-demos',
        'elab_theme_admin_install_demo_page'
    );

    /*System status*/
    add_submenu_page(
        $theme_name_sanitized,
        esc_html__('System status', 'elab'),
        esc_html__('System status', 'elab'),
        'manage_options',
        $theme_name_sanitized . '-system-status',
        'elab_theme_admin_system_status_page'
    );

    /*Support page*/
    add_submenu_page(
        $theme_name_sanitized,
        esc_html__('Support', 'elab'),
        esc_html__('Support', 'elab'),
        'manage_options',
        $theme_name_sanitized . '-support',
        'elab_theme_admin_support_page'
    );

    /*Theme options export*/
    if (!empty($_GET['stm_get_to'])) {
        add_submenu_page(
            $theme_name_sanitized,
            esc_html__('Theme options export', 'elab'),
            esc_html__('Theme options export', 'elab'),
            'manage_options',
            'gigant-theme-options-export',
            'elab_theme_options_export'
        );
    }
}

add_action('admin_menu', 'elab_register_startup_screen', 20);

function elab_startup_templates($path)
{
    load_template(get_template_directory() . '/inc/admin/product_registration/screens/' . $path . '.php', true);
}

//Startup screen menu page welcome
function elab_theme_admin_page_functions()
{
    elab_startup_templates('startup');
}

/*Support Screen*/
function elab_theme_admin_support_page()
{
    elab_startup_templates('support');
}

/*Install Plugins*/
function elab_theme_admin_plugins_page()
{
    elab_startup_templates('plugins');
}

/*Install Demo*/
function elab_theme_admin_install_demo_page()
{
    elab_startup_templates('install_demo');
}

/*System status*/
function elab_theme_admin_system_status_page()
{
    elab_startup_templates('system_status');
}

//Admin tabs
function elab_get_admin_tabs($screen = 'welcome')
{
    $theme = elab_get_theme_info();
    $creds = elab_get_creds();
    $theme_name = $theme['name'];
    $theme_name_sanitized = 'stm-admin';
    if (empty($screen)) {
        $screen = $theme_name_sanitized;
    }
    ?>
    <div class="clearfix">
        <div class="elab_info">
            <div class="elab_version"><?php echo substr($theme['v'], 0, 3); ?></div>
        </div>
        <div class="stm-about-text-wrap">
            <h1><?php printf(esc_html__('Welcome to %s', 'elab'), $theme_name); ?></h1>
        </div>
    </div>
    <?php $notice = get_site_transient('stm_auth_notice');
    if (!empty($creds['t']) && !empty($notice)): ?>
        <div class="stm-admin-message"><strong>Theme Registration Error:</strong> <?php echo esc_attr($notice); ?></div>
    <?php endif; ?>
    <h2 class="nav-tab-wrapper">
        <a href="<?php echo esc_url_raw(admin_url('admin.php?page=elab')); ?>"
           class="<?php echo ('welcome' === $screen) ? 'nav-tab-active' : ''; ?> nav-tab"><?php esc_attr_e('Product Registration', 'elab'); ?></a>

        <a href="<?php echo esc_url_raw(admin_url('admin.php?page=elab-demos')); ?>"
           class="<?php echo ('demos' === $screen) ? 'nav-tab-active' : ''; ?> nav-tab"><?php esc_attr_e('Install Demos', 'elab'); ?></a>

        <a href="<?php echo esc_url_raw(admin_url('admin.php?page=tgmpa-install-plugins')); ?>"
           class="<?php echo ('plugins' === $screen) ? 'nav-tab-active' : ''; ?> nav-tab"><?php esc_attr_e('Plugins', 'elab'); ?></a>

        <?php if (class_exists('STMT_TO_Settings')): ?>
            <a href="<?php echo esc_url_raw(admin_url('admin.php?page=stmt-to-settings')); ?>"
               class="nav-tab"><?php esc_attr_e('Theme Options', 'elab'); ?></a>
        <?php endif; ?>

        <a href="<?php echo esc_url_raw(admin_url('admin.php?page=elab-support')); ?>"
           class="<?php echo ('support' === $screen) ? 'nav-tab-active' : ''; ?> nav-tab"><?php esc_attr_e('Support', 'elab'); ?></a>

        <a href="<?php echo esc_url_raw(admin_url('admin.php?page=elab-system-status')); ?>"
           class="<?php echo ('system-status' === $screen) ? 'nav-tab-active' : ''; ?> nav-tab"><?php esc_attr_e('System Status', 'elab'); ?></a>

    </h2>
    <?php
}