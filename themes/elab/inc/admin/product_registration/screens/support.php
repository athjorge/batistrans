<?php

// Do not allow directly accessing this file.
if (!defined('ABSPATH')) {
    exit('Direct script access denied.');
}

$theme = elab_get_theme_info();
$theme_name = $theme['name'];
?>
<div class="wrap about-wrap stm-admin-wrap  stm-admin-support-screen">
    <?php elab_get_admin_tabs('support'); ?>
    <div class="stm-admin-important-notice">

        <p class="about-description"><?php printf(wp_kses_post(esc_html__('%s comes with 6 months of free support for every license you purchase. Support can be extended through subscriptions via ThemeForest.', 'elab')), $theme_name); ?></p>
        <p><a href="<?php echo esc_url(elab_theme_support_url() . 'support/'); ?>"
              class="button button-large button-primary stm-admin-button stm-admin-large-button" target="_blank"
              rel="noopener noreferrer"><?php esc_attr_e('Create A Support Account', 'elab'); ?></a></p>
    </div>

    <div class="stm-admin-row">
        <div class="stm-admin-two-third">

            <div class="stm-admin-row">

                <div class="stm-admin-one-half">
                    <div class="stm-admin-one-half-inner">
                        <h3>
							<span>
								<img src="<?php echo esc_url(elab_get_admin_images_url('ticket.svg')); ?>"/>
							</span>
                            <?php esc_html_e('Ticket System', 'elab'); ?>
                        </h3>
                        <p>
                            <?php esc_html_e('We offer excellent support through our advanced ticket system. Make sure to register your purchase first to access our support services and other resources.', 'elab'); ?>
                        </p>
                        <a href="<?php echo esc_url(elab_theme_support_url() . 'support/'); ?>" target="_blank">
                            <?php esc_html_e('Submit a ticket', 'elab'); ?>
                        </a>
                    </div>
                </div>

                <div class="stm-admin-one-half">
                    <div class="stm-admin-one-half-inner">
                        <h3>
							<span>
								<img src="<?php echo esc_url(elab_get_admin_images_url('docs.svg')); ?>"/>
							</span>
                            <?php esc_html_e('Documentation', 'elab'); ?>
                        </h3>
                        <p>
                            <?php printf(wp_kses_post(esc_html__('Our online documentaiton is a useful resource for learning the every aspect and features of %s.', 'elab')), $theme_name); ?>
                        </p>
                        <a href="<?php echo esc_url(elab_theme_support_url() . 'manuals/hotel/'); ?>" target="_blank">
                            <?php esc_html_e('Learn more', 'elab'); ?>
                        </a>
                    </div>
                </div>
            </div>

            <div class="stm-admin-row">

                <div class="stm-admin-one-half">
                    <div class="stm-admin-one-half-inner">
                        <h3>
							<span>
								<img src="<?php echo esc_url(elab_get_admin_images_url('tutorials.svg')); ?>"/>
							</span>
                            <?php esc_html_e('Video Tutorials', 'elab'); ?>
                        </h3>
                        <p>
                            <?php printf(wp_kses_post(esc_html__('We recommend you to watch video tutorials before you start the theme customization. Our video tutorials can teach you the different aspects of using %s.', 'elab')), $theme_name); ?>
                        </p>
                        <a href="https://www.youtube.com/watch?v=qjPLasciSrc" target="_blank">
                            <?php esc_html_e('Watch Videos', 'elab'); ?>
                        </a>
                    </div>
                </div>

                <div class="stm-admin-one-half">
                    <div class="stm-admin-one-half-inner">
                        <h3>
							<span>
								<img src="<?php echo esc_url(elab_get_admin_images_url('forum.svg')); ?>"/>
							</span>
                            <?php esc_html_e('Community Forum', 'elab'); ?>
                        </h3>
                        <p>
                            <?php printf(wp_kses_post(esc_html__('Our forum is the best place for user to user interactions. Ask another %s user or share your experience with the community to help others.', 'elab')), $theme_name); ?>
                        </p>
                        <a href="<?php echo esc_url(elab_theme_support_url() . 'forums/'); ?>" target="_blank">
                            <?php esc_html_e('Visit Our Forum', 'elab'); ?>
                        </a>
                    </div>
                </div>

            </div>

        </div>

        <div class="stm-admin-one-third">
            <a href="https://stylemix.net/?utm_source=dashboard&utm_medium=banner&utm_campaign=elabwp" target="_blank">
                <img src="<?php echo esc_url(elab_get_admin_images_url('banner-1.png')); ?>"/>
            </a>
        </div>
    </div>

</div>