<?php
/**
 * Increase products quantity and products list after ajax add to cart
 *
 * @return mixed
 */
function elab_cart_fragments($fragments) {

    $fragments = (!empty($fragments)) ? $fragments : array();

    /*Notification*/
    ob_start();
    get_template_part('partials/header/parts/cart/notification');
    $notification = ob_get_contents();
    ob_end_clean();

	/*Mini cart*/
	ob_start();
	get_template_part('partials/header/parts/cart/mini-cart');
	$mini_cart = ob_get_contents();
	ob_end_clean();

	/*Quantity*/
	ob_start();
	get_template_part('partials/header/parts/cart/quantity');
	$quantity = ob_get_contents();
	ob_end_clean();

    /*Total Price*/
    ob_start();
    get_template_part('partials/header/parts/cart/price');
    $price = ob_get_contents();
    ob_end_clean();

    $fragments['.cart__notification'] = $notification;
    $fragments['.mini-cart'] = $mini_cart;
    $fragments['.cart__quantity-badge'] = $quantity;
    $fragments['.cart__price-badge'] = $price;

	return $fragments;
}

add_filter( 'woocommerce_add_to_cart_fragments', 'elab_cart_fragments' );

/*Clear cart*/
add_action( 'init', 'elab_woocommerce_clear_cart_url' );
function elab_woocommerce_clear_cart_url() {
	global $woocommerce;

	if ( isset( $_GET['empty-cart'] ) ) {
		$woocommerce->cart->empty_cart();
	}
}