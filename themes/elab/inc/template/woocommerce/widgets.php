<?php
add_filter('list_product_cats', 'elab_list_product_cats', 10, 2);

function elab_list_product_cats($cat_name, $cat) {

    $cat_id = $cat->term_id;
    $icon = get_term_meta($cat_id, 'x_product_icon', true);
    $icon = (!empty($icon)) ? $icon : 'lnricons-arrow-right';

    return "<i class='{$icon} stc'></i> {$cat_name}";
}