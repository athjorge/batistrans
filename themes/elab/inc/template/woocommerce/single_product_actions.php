<?php
add_filter('woocommerce_get_image_size_single', 'elab_set_product_img_size');
add_filter('woocommerce_get_image_size_shop_single', 'elab_set_product_img_size');
add_filter('woocommerce_get_image_size_woocommerce_single', 'elab_set_product_img_size');
function elab_set_product_img_size()
{
    $size = array(
        'width' => 460,
        'height' => 560,
        'crop' => 1,
    );
    return $size;
}

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'add_compare_link', 35);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 7);

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
add_action('woocommerce_before_add_to_cart_button', 'woocommerce_template_single_price');
add_filter( 'woocommerce_show_variation_price', '__return_true' );

// function elab_woocommerce_variations_price() {
//     wc_get_template( 'single-product/price.php' );
// }

add_action('woocommerce_share', 'elab_woocommerce_compare_wishlist');
function elab_woocommerce_compare_wishlist() {
    $single_product_style = elab_single_product_style();
    $disallowed_styles = array('style_3');

    if(!in_array($single_product_style, $disallowed_styles)) {
        get_template_part('partials/woocommerce/compare_wishlist');
    }
}

remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
add_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 25);

add_action('woocommerce_after_add_to_cart_button', 'elab_add_share_after_cart_button');
function elab_add_share_after_cart_button() {
    $single_product_style = elab_single_product_style();
    $available_styles = array('style_2');

    if(in_array($single_product_style, $available_styles) and function_exists('stm_get_shares')) {
        stm_get_shares();
    }
}

add_action('elab_after_product_images', 'elab_after_product_images');
function elab_after_product_images() {
    $single_product_style = elab_single_product_style();
    $available_styles = array('style_2');

    if(in_array($single_product_style, $available_styles)) {
        get_template_part('partials/woocommerce/timer');
    }
}
/*For Product Style 3*/
add_action('wp', 'elab_woocommerce_add_actions');
function elab_woocommerce_add_actions() {
    /*woocommerce_single_variation*/
    $single_product_style = elab_single_product_style();

    if($single_product_style === 'style_3') {

        remove_action('woocommerce_single_variation', 'woocommerce_single_variation', 10);

        add_action('woocommerce_before_variations_form', 'elab_add_price');
        function elab_add_price()
        {
            woocommerce_single_variation();
            wc_get_template('single-product/price.php');
        }
    }
    elseif($single_product_style === 'style_4') {
        add_filter('woocommerce_product_tabs', 'elab_remove_tabs');
        function elab_remove_tabs($tabs){
            unset($tabs['additional_information']);
            unset($tabs['more_seller_product']);
            return $tabs;
        }
        remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
        remove_action('woocommerce_share', 'stm_get_shares');
    }
}