<?php
require_once $stmt_inc_path . '/template/woocommerce/cart.php';
require_once $stmt_inc_path . '/template/woocommerce/ajax.php';
require_once $stmt_inc_path . '/template/woocommerce/widgets.php';
require_once $stmt_inc_path . '/template/woocommerce/single_product_actions.php';

add_action( 'after_setup_theme', 'elab_add_woocommerce_support' );

function elab_add_woocommerce_support()
{
    add_theme_support( 'woocommerce', array(
        'thumbnail_image_width' => 270,
        'thumbnail_image_crop' => false,
        'single_image_width' => 527,
        'single_image_crop' => false,
        'product_grid' => array(
            'default_rows' => 4,
            'min_rows' => 4,
            'max_rows' => 4,
            'default_columns' => 4,
        ),
    ) );
}

add_filter( 'woocommerce_get_image_size_single', function( $size ) {
    return array(
        'width' => 527,
        'height' => 527,
        'crop' => 0,
    );
} );

add_filter( 'woocommerce_get_image_size_thumbnail', function( $size ) {
    return array(
        'width' => 270,
        'height' => 270,
        'crop' => 1,
    );
} );

function elab_woo_scripts()
{
    elab_enqueue_parted_style( 'archive', 'woocommerce/' );
    elab_enqueue_parted_style( 'widgets', 'woocommerce/' );
    if (class_exists('WeDevs_Dokan')) elab_enqueue_parted_style( 'dokan', 'dokan/' );
    if (class_exists('WC_Vendors')) elab_enqueue_parted_style( 'wcvendors', 'wcvendors/' );


    $style = elab_get_option( 'product_card_style', 'style_1' );
    elab_enqueue_parted_style( "product_buttons/$style", 'woocommerce/' );

    elab_enqueue_parted_style( "image_hover/style_1", 'woocommerce/' );
}

add_action( 'wp_enqueue_scripts', 'elab_woo_scripts' );

add_filter( 'stmt_to_settings', 'elab_stmt_to_settings' );
function elab_stmt_to_settings( $settings )
{

    $banners = elab_get_posts( 'stmt-banners' );

    $settings[ 'args' ][ 'stmt_to_settings' ][ 'woocommerce' ] = array(
        'name' => esc_html__( 'Shop', 'elab' ),
        'fields' => array(
            'shop_banner' => array(
                'type' => 'select',
                'label' => esc_html__( 'Shop Banner', 'elab' ),
                'options' => $banners
            ),
            'product_card_style' => array(
                'type' => 'select',
                'label' => esc_html__( 'Product Hover Style', 'elab' ),
                'options' => array(
                    'style_1' => esc_html__( 'Full Hover with Buy button', 'elab' ),
                    'style_2' => esc_html__( 'Small box with icon buttons', 'elab' ),
                )
            ),
            'shop_product_view' => array(
                'type' => 'select',
                'label' => esc_html__( 'Products View in Shop Page', 'elab' ),
                'options' => array(
                    'grid' => esc_html__( 'Grid style 1', 'elab' ),
                    'grid_2' => esc_html__( 'Grid style 2', 'elab' ),
                    'list' => esc_html__( 'List', 'elab' ),
                ),
                'value' => 'grid'
            ),
            'shop_page_style' => array(
                'type' => 'select',
                'label' => esc_html__( 'Shop Page Style', 'elab' ),
                'options' => array(
                    'default' => esc_html__( 'Default', 'elab' ),
                    'full_width' => esc_html__( 'Full Width', 'elab' ),
                ),
                'value' => 'default'
            ),
            'single_product_style' => array(
                'type' => 'select',
                'label' => esc_html__( 'Single Product Style', 'elab' ),
                'options' => array(
                    'style_1' => esc_html__( 'Image Left', 'elab' ),
                    'style_2' => esc_html__( 'Image right', 'elab' ),
                    'style_3' => esc_html__( 'Image Center', 'elab' ),
                    'style_4' => esc_html__('Product info right', 'elab'),
                ),
                'value' => 'style_1'
            ),
            'shop_per_page' => array(
                'type' => 'number',
                'label' => esc_html__( 'Products Per Page', 'elab' ),
            ),
            'price_filter_step' => array(
                'type' => 'number',
                'label' => esc_html__( 'Price Filter Step', 'elab' ),
            )
        )
    );

    return $settings;
}

/*Archive Filters*/
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );

add_filter( 'woocommerce_layered_nav_term_html', 'elab_woocommerce_layered_nav_term_html', 10, 3 );
function elab_woocommerce_layered_nav_term_html( $term_html, $term, $link )
{

    $query_args = array();
    if( !empty( $_GET[ 'per_page' ] ) ) {
        $query_args[ 'per_page' ] = intval( $_GET[ 'per_page' ] );
    }

    $new_link = ( add_query_arg( $query_args, html_entity_decode( $link ) ) );

    return ( str_replace( $link, $new_link, $term_html ) );
}

/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter( 'loop_shop_per_page', 'elab_loop_shop_per_page', 20 );
function elab_loop_shop_per_page( $cols )
{
    // $cols contains the current number of products per page based on the value stored on Options -> Reading
    // Return the number of products you wanna show per page.

    $default_per_page = elab_get_option( 'shop_per_page', get_option( 'posts_per_page' ) );

    $cols = ( !empty( $_GET[ 'per_page' ] ) ) ? intval( $_GET[ 'per_page' ] ) : $default_per_page;
    return $cols;
}

add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );

add_filter( 'woocommerce_upsell_display_args', 'elab_related_products_args', 20 );
add_filter( 'woocommerce_output_related_products_args', 'elab_related_products_args', 20 );
function elab_related_products_args( $args )
{
    $args[ 'posts_per_page' ] = 5;
    $args[ 'columns' ] = 5;
    return $args;
}

/*Cart Page*/
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );

add_action( 'product_buttons_style', 'elab_product_buttons_style' );

function elab_product_buttons_style()
{
    $style = elab_get_option( 'product_card_style', 'style_1' );
    return "product_buttons/{$style}";
}

add_action( 'x_enable_buttons_style', 'elab_x_enable_buttons_style' );

function elab_x_enable_buttons_style()
{
    return false;
}

function elab_hide_product_categories_widget( $list_args )
{

    $list_args[ 'hide_empty' ] = 1;

    return $list_args;

}

add_filter( 'woocommerce_product_categories_widget_args', 'elab_hide_product_categories_widget' );
if( !empty( $_POST[ 'action' ] ) && $_POST[ 'action' ] == 'yith_load_product_quick_view' ) {
    add_action( 'woocommerce_product_thumbnails', 'woocommerce_show_product_thumbnails', 30 );
}

add_action( 'elab_after_body_started', 'elab_add_quick_view_message_popup' );

function elab_add_quick_view_message_popup()
{
    if( defined( 'YITH_WCWL' ) ): ?>
        <div id="yith-wcwl-popup-message" style="display: none;">
            <div id="yith-wcwl-message"></div>
        </div>
    <?php endif;
}

add_filter('woocommerce_product_tabs', 'elab_woocommerce_product_tabs');
function elab_woocommerce_product_tabs($tabs) {

    $id = get_the_ID();
    $accessories = get_post_meta($id, 'product_accessories', true);

    if(!empty($accessories)) {
        $tabs['accessories'] = array(
            'title' => esc_html__('Accessories', 'elab'),
            'priority' => 15,
            'callback' => 'elab_add_accessories_tab',
        );
    }

    return $tabs;
}

function elab_add_accessories_tab() {
    get_template_part('woocommerce/single-product/accessories');
}

add_action('wp_ajax_elab_add_accessories_bundle', 'elab_add_accessories_bundle');
add_action('wp_ajax_nopriv_elab_add_accessories_bundle', 'elab_add_accessories_bundle');

function elab_add_accessories_bundle() {
    check_ajax_referer('elab_add_accessories_bundle', 'security');
    $r = array(
        'added' => false
    );

    if(!empty($_POST['items'])) {
        $items = explode('|', sanitize_text_field($_POST['items']));

        foreach($items as $item) {
            WC()->cart->add_to_cart( $item );
            $r['added'] = true;
        }


    }

    wp_send_json($r);
}
//add_action('init', 'update_products');
//function update_products(){
//    if(!empty($_GET['update_products'])){
//        $args = array(
//            'post_type' => 'product',
//            'posts_per_page' => -1,
//        );
//        $all_products = array();
//        $q = new WP_Query( $args );
//
//
//        if( $q->have_posts() ) {
//            while ( $q->have_posts() ) {
//                $q->the_post();
//                $all_products[] = array();
//                $product_cat = wp_get_post_terms(get_the_ID(), 'product_cat', array('fields' => 'ids'));
//
//                $all_products[] = array(
//                    'id' => get_the_ID(),
//                    'cat' => $product_cat
//                );
//
//            }
//            wp_reset_postdata();
//
//        }
//
//        foreach($all_products as $product){
//            $product_accessories = array();
//            $args = array(
//                'post_type' => 'product',
//                'posts_per_page' => 3,
//                'orderby' => 'rand',
//                'post__not_in' => array($product['id']),
//                'tax_query' => array(
//                    array(
//                        'taxonomy' => 'product_cat',
//                        'field' => 'term_id',
//                        'terms' => $product['cat'],
//                        'operator' => 'NOT IN'
//                    )
//                )
//            );
//            $p = new WP_Query( $args );
//            if( $p->have_posts() ) {
//                while ( $p->have_posts() ) {
//                    $p->the_post();
//                    $product_accessories[] = array(
//                        'name' => get_the_title(),
//                        'value' => get_the_ID(),
//                    );
//                }
//                $product_accessories = json_encode($product_accessories);
//                update_post_meta($product['id'], 'product_accessories', $product_accessories);
//                wp_reset_postdata();
//            }
//        }
//    }
//}
