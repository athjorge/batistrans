<?php

add_action( 'wp_ajax_elab_search_woo_products', 'elab_search_woo_products' );
add_action( 'wp_ajax_nopriv_elab_search_woo_products', 'elab_search_woo_products' );

function elab_search_woo_products()
{
    check_ajax_referer( 'elab_search_woo_products', 'security' );
    $r = array();

    $s = ( !empty( $_GET[ 'search_query' ] ) ) ? sanitize_text_field( $_GET[ 'search_query' ] ) : '';
    $term_id = ( !empty( $_GET[ 'category' ] ) ) ? intval( $_GET[ 'category' ] ) : '';

    $args = array(
        'post_type' => 'product',
        'post_status' => 'publish',
        's' => $s,
    );

    if( !empty( $term_id ) ) {
        $args[ 'tax_query' ] = array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'term_id',
                'terms' => array( $term_id )
            )
        );
    }

    $q = new WP_Query( $args );

    if( $q->have_posts() ) {
        while ( $q->have_posts() ) {
            $q->the_post();

            $id = get_the_ID();
            $title = get_the_title();
            $image_id = get_post_thumbnail_id();
            $image = ($image_id) ? elab_get_cropped_image_url($image_id, 45, 45) : '';
            $terms = wp_get_post_terms($id, 'product_cat');

            $term = (!empty($terms[0])) ? $terms[0]->name : '';

            $r_single = array(
                'id' => $id,
                'label' => $title,
                'html' => elab_highlight_words($title, $s),
                'permalink' => get_the_permalink(),
                'image' => $image,
                'term' => $term,
            );

            $r[] = $r_single;
        }

        wp_reset_postdata();
    }

    wp_send_json( $r );
}
