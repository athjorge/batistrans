<?php
add_action('after_setup_theme', 'elab_setup');
function elab_setup()
{
    load_theme_textdomain('elab', get_template_directory() . '/languages');

    add_theme_support('automatic-feed-links');

    add_theme_support('title-tag');

    add_theme_support('post-thumbnails');

    register_nav_menus(array(
        'top-bar' => esc_html__('Top bar menu', 'elab'),
        'menu-header' => esc_html__('Header menu', 'elab'),
        'menu-footer' => esc_html__('Footer menu', 'elab'),
    ));

    add_theme_support('html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ));

    add_image_size('elab_965x520', 1015);
}

function elab_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'elab'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'elab'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer 1', 'elab'),
        'id' => 'footer-1',
        'description' => esc_html__('Add widgets here.', 'elab'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer 2', 'elab'),
        'id' => 'footer-2',
        'description' => esc_html__('Add widgets here.', 'elab'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer 3', 'elab'),
        'id' => 'footer-3',
        'description' => esc_html__('Add widgets here.', 'elab'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer 4', 'elab'),
        'id' => 'footer-4',
        'description' => esc_html__('Add widgets here.', 'elab'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer 5', 'elab'),
        'id' => 'footer-5',
        'description' => esc_html__('Add widgets here.', 'elab'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer 6', 'elab'),
        'id' => 'footer-6',
        'description' => esc_html__('Add widgets here.', 'elab'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => esc_html__('WooCommerce', 'elab'),
        'id' => 'shop',
        'description' => esc_html__('Add widgets here.', 'elab'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

}
add_action('widgets_init', 'elab_widgets_init');

function elab_add_nonces(){
    $variables = array (
        'elab_install_plugin' => wp_create_nonce('elab_install_plugin'),
        'elab_ajax_add_review' => wp_create_nonce('elab_ajax_add_review'),
        'elab_like_portfolio' => wp_create_nonce('elab_like_portfolio'),
        'elab_search_woo_products' => wp_create_nonce('elab_search_woo_products'),
        'elab_quick_view' => wp_create_nonce('elab_quick_view'),
        'elab_add_accessories_bundle' => wp_create_nonce('elab_add_accessories_bundle'),
    );
    echo( '<script type="text/javascript">window.wp_data = '. json_encode( $variables ). ';</script>' );
}
add_action('wp_head', 'elab_add_nonces');
add_action('admin_head', 'elab_add_nonces');

