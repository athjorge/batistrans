<?php
/*Single Comment view*/
if (!function_exists('elab_comment')) {
    function elab_comment($comment, $args, $depth)
    {
        $GLOBALS['comment'] = $comment;
        extract($args, EXTR_SKIP);

        $tag = ('div' == $args['style']) ? 'div' : 'li';
        $add_below = ('div' == $args['style']) ? 'comment' : 'div-comment'; ?>

        <<?php echo esc_attr($tag) ?> <?php comment_class(empty($args['has_children']) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
            <?php if ('div' != $args['style']) { ?>
            <div id="div-comment-<?php comment_ID() ?>" class="comment-body clearfix">
        <?php } ?>

        <?php if ($args['avatar_size'] != 0) {
            $avatar = get_avatar($comment, 75);
            ?>
            <div class="vcard">
                <?php if (strpos($avatar, 'f72c502e0d657f363b5f2dc79dd8ceea') === false): ?>
                    <?php echo wp_kses_post($avatar); ?>
                <?php else: ?>
                    <div class="stm_no_avatar">
                        <i class="lnricons-user"></i>
                    </div>
                <?php endif; ?>
            </div>
        <?php } ?>

        <div class="comment-info clearfix">

            <div class="comment-meta">
                <?php printf(esc_html__('by %s on %s', 'elab'), get_comment_author_link(), get_comment_date()); ?>
                <?php comment_reply_link(array_merge($args, array(
                        'reply_text' => esc_html__('Reply', 'elab') . '<i class="lnricons-reply"></i>',
                        'add_below' => $add_below, 'depth' => $depth,
                        'max_depth' => $args['max_depth']))
                ); ?>
                <?php edit_comment_link(sprintf(esc_html__('Edit %s', 'elab'), '<i class="lnricons-pencil"></i>'), '', ''); ?>
            </div>

            <div class="comment-text">
                <?php comment_text(); ?>
            </div>

            <?php if ($comment->comment_approved == '0') { ?>
                <em class="comment-awaiting-moderation"><?php esc_html_e('Your comment is awaiting moderation.', 'elab'); ?></em>
            <?php } ?>

        </div>

        <?php if ('div' != $args['style']) { ?>
        </div>
    <?php } ?>
        <?php
    }
}

add_filter('comment_form_default_fields', 'elab_comment_form_fields');

function elab_comment_form_fields($fields)
{
    $commenter = wp_get_current_commenter();
    $req = get_option('require_name_email');
    $aria_req = ($req ? " aria-required='true'" : '');
    $html5 = current_theme_supports('html5', 'comment-form') ? 1 : 0;
    $fields = array(
        'author' => '<div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group comment-form-author">
                <input placeholder="' . esc_attr__('Name', 'elab') . ($req ? ' *' : '') . '" class="form-control" name="author" type="text" value="' . esc_attr($commenter['comment_author']) . '" size="30"' . $aria_req . ' />
            </div>
        </div>',
        'email' => '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <div class="form-group comment-form-email">
                <input placeholder="' . esc_attr__('E-mail', 'elab') . ($req ? ' *' : '') . '" class="form-control" name="email" ' . ($html5 ? 'type="email"' : 'type="text"') . ' value="' . esc_attr($commenter['comment_author_email']) . '" size="30"' . $aria_req . ' />
            </div>
        </div></div>',
    );

    return $fields;
}

add_filter('comment_form_defaults', 'elab_comment_form');

function elab_comment_form($args)
{
    $args['comment_field'] = '<div class="form-group comment-form-comment">
        <textarea placeholder="' . esc_attr__('Comment', 'elab') . ' *" class="form-control" name="comment" rows="9" aria-required="true"></textarea>
    </div>';

    $args['submit_button'] = '<button name="%1$s" type="submit" id="%2$s" class="btn btn-primary %3$s" value="%4$s">%4$s</button>';


    return $args;
}