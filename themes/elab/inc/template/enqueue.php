<?php
function elab_scripts()
{
    $script_vars = elab_scripts_vars();
    $layout = elab_get_theme_layout();

    /*Styles*/
    wp_enqueue_style('elab-style', get_stylesheet_uri());
    elab_enqueue_parted_style('preloader', 'footer_parts/');
    wp_enqueue_style('nice-select', "{$script_vars['css']}nice-select.css", array(), $script_vars['v']);
    wp_register_style('owl-carousel', "{$script_vars['css']}owl.carousel.min.css", array(), $script_vars['v']);
    wp_register_style('slick', "{$script_vars['css']}slick.css", array(), $script_vars['v']);
    wp_enqueue_style('elab-app-style', "{$script_vars['css']}style.css", array(), $script_vars['v']);

    wp_enqueue_style('elab-layout-style', "{$script_vars['css']}partials/layout/{$layout}.css", array(), $script_vars['v']);

    //Fonts
    wp_enqueue_style('elab-google-fonts', elab_google_fonts());
    wp_enqueue_style('elab-linear-icons', "{$script_vars['linear_icons']}linear.css", array(), $script_vars['v']);
    wp_enqueue_style('elab-icons', "{$script_vars['elab_icons']}elab-icons.css", array(), $script_vars['v']);
//    wp_enqueue_style('fontawesome', "{$script_vars['fa-brands']}all.min.css", array(), $script_vars['v']);
    wp_enqueue_style('fontawesome-brands', "{$script_vars['fa-brands']}brands.min.css", array(), $script_vars['v']);
    wp_enqueue_style('elab-icons', "{$script_vars['elab_icons']}elab-icons.css", array(), $script_vars['v']);

    /*Scripts*/
    $ajax_url = admin_url('admin-ajax.php');
    wp_add_inline_script(
        'jquery',
        "if(typeof ajaxurl === 'undefined') var ajaxurl = '{$ajax_url}'",
        'before');

    $google_api_key = elab_get_option('google_map_api_key');
    wp_enqueue_script('nice-select', "{$script_vars['js']}jquery.nice-select.min.js", array('jquery'), $script_vars['v'], true);
    wp_enqueue_script('packery', "{$script_vars['js']}packery.min.js", array('jquery'), $script_vars['v'], true);
    wp_register_script('slick', "{$script_vars['js']}slick.min.js", array('jquery'), $script_vars['v'], true);
    wp_register_script('sticky-sidebar', "{$script_vars['js']}sticky-sidebar.min.js", array('jquery'), $script_vars['v'], true);
    wp_register_script('single-product_style_4', "{$script_vars['js']}partials/single-product_style_4.js", array('jquery', 'sticky-sidebar'), $script_vars['v'], true);
    wp_register_script('owl-carousel', "{$script_vars['js']}owl.carousel.min.js", array('jquery'), $script_vars['v'], true);
    wp_enqueue_script('elab-app', "{$script_vars['js']}app.js", array('jquery'), $script_vars['v'], true);
    wp_register_script('gmap', '//maps.googleapis.com/maps/api/js?key=' . $google_api_key . '&callback=initGoogleScripts', array('elab-app'), $script_vars['v'], true);
    wp_add_inline_script(
        'gmap',
        'function initGoogleScripts() {let stmGmap = new CustomEvent("stm_gmap_api_loaded");document.body.dispatchEvent(stmGmap);}',
        'before');

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    if (class_exists('WooCommerce')) elab_enqueue_parted_script('woocommerce', array('elab-app'));

    if (class_exists('WooCommerce') && (is_shop() || is_product_category() || is_tax('stmt_brand_taxonomy') )) {
        wp_enqueue_script('owl-carousel');
        wp_enqueue_style('owl-carousel');
        wp_enqueue_script('elab-woo-archive', "{$script_vars['js']}partials/woo_shop.js", array('jquery'), $script_vars['v'], true);
        wp_localize_script( 'elab-woo-archive', 'buttonTexts', array( 'closed' => esc_html__('Show More', 'elab'), 'opened' => esc_html__('Show Less', 'elab')  ) );
    };

    $current_object = get_queried_object();
    $inline_styles = '';

    /*Get Global Background color*/
    $page_color = elab_get_option('page_color', '');
    if (!empty($page_color)) {
        $inline_styles .= "body #page {
                background-color: {$page_color};
            }";
    }

    if (!is_wp_error($current_object) and !empty($current_object->ID)) {
        $post_id = $current_object->ID;
    }

    if(elab_is_shop()) {
        $post_id = get_option( 'woocommerce_shop_page_id' );
    }

    if(!empty($post_id)) {

        $transparent_title_box = get_post_meta($post_id, 'page_title_transparent', true);
        if ($transparent_title_box) {
            elab_enqueue_parted_style('transparent_title_box', '');
        }

        $bg_color = get_post_meta($post_id, 'page_color', true);
        if (!empty($bg_color)) {
            $inline_styles .= "body #page {
                background-color: {$bg_color};
            }";
        }

        $footer_top_padding = get_post_meta($post_id, 'footer_top_padding', true);
        $footer_bottom_padding = get_post_meta($post_id, 'footer_bottom_padding', true);


        if(!empty($footer_top_padding)) {
            $inline_styles .= "body .widgets_box {
                padding-top : {$footer_top_padding}px !important;
            }";
        }

        if(!empty($footer_bottom_padding)) {
            $inline_styles .= "body .widgets_box {
                padding-bottom : {$footer_bottom_padding}px !important;
            }";
        }

    }

    if (elab_is_shop() || elab_is_product_category()) {
        $shop_page_id = get_option('woocommerce_shop_page_id');
        if (!empty($shop_page_id)) {
            $bg_color = get_post_meta($shop_page_id, 'page_color', true);
            if (!empty($bg_color)) {
                $inline_styles .= "body #page {
                background-color: {$bg_color};
            }";
            }
        }
    }

    if (!empty($inline_styles)) wp_add_inline_style('elab-style', $inline_styles);

    $content_width = elab_get_option('content_max_width', 1470);
    if (empty($content_width)) $content_width = 1470;
    wp_localize_script('elab-app', 'elab_vars', array(
        'ajax_url' => esc_url(admin_url('admin-ajax.php')),
        'content_width' => $content_width,
    ));

}

add_action('wp_enqueue_scripts', 'elab_scripts');

function elab_footer_scripts()
{
    /*Theme Options Custom Styles*/
    $upload = wp_upload_dir();
    $baseurl = $upload['baseurl'] . '/stm_configurations_styles';
    wp_enqueue_style('elab-app-style-custom', "{$baseurl}/styles.css", array(), get_option('elab_styles_v', 1));
}

add_action('get_footer', 'elab_footer_scripts');

function elab_scripts_vars()
{
    $vars = array();
    $url = get_template_directory_uri() . '/assets/';

    global $theme_info;
    $theme_info = wp_get_theme();

    $vars['linear_icons'] = "{$url}icons/linearicons/";
    $vars['fa-brands'] = "{$url}icons/fa-brands/";
    $vars['elab_icons'] = "{$url}icons/elab/";
    $vars['css'] = apply_filters('elab_assets_css_path', "{$url}css/");
    $vars['admin_css'] = apply_filters('elab_assets_admin_css_path', "{$url}admin_css/");
    $vars['js'] = "{$url}js/";
    $vars['v'] = (WP_DEBUG) ? time() : $theme_info->get('Version');

    return $vars;
}

add_action('wp_default_scripts', 'elab_move_jquery_into_footer');
function elab_move_jquery_into_footer($wp_scripts)
{

    if (is_admin()) {
        return;
    }

    $wp_scripts->add_data('jquery', 'group', 1);
    $wp_scripts->add_data('jquery-core', 'group', 1);
    $wp_scripts->add_data('jquery-migrate', 'group', 1);
}

function elab_google_fonts()
{
    $fonts_url = '';
    $enable_google_fonts = _x('on', 'Main font: on or off', 'elab');


    if ($enable_google_fonts === 'on') {
        $settings = elab_stored_theme_options();
        /*Default Headings font-family*/
        $ffs = array();
        $weights = apply_filters('elab_font_weight', '100,200,300,400,400i,500,600,700,900');
        if (!empty($settings['default_header_font_family'])) $ffs[] = "{$settings['default_header_font_family']}:{$weights}";

        $typo = array(
            'body', 'h1, .h1', 'h2, .h2', 'h3, .h3', 'h4, .h4', 'h5, .h5', 'h6, .h6'
        );

        foreach ($typo as $setting) {
            if (empty($settings[$setting])) continue;
            $data = json_decode($settings[$setting], true);
            if (!empty($data['font-family'])) $ffs[] = "{$data['font-family']}:{$weights}";
        }

        $subsets = apply_filters('elab_font_subset', 'latin,latin-ext');

        $query_args = array(
            'family' => urlencode(implode('|', array_unique($ffs))),
            'subset' => urlencode($subsets),
        );

        $fonts_url = (!empty($ffs)) ? add_query_arg($query_args, 'https://fonts.googleapis.com/css') : '';
    }

    return esc_url($fonts_url);

}

add_action('init', 'elab_google_fonts');

add_action('admin_enqueue_scripts', 'elab_admin_scripts');
function elab_admin_scripts()
{
    $script_vars = elab_scripts_vars();
    wp_enqueue_style('elab-linear-icons', "{$script_vars['linear_icons']}linear.css", array(), $script_vars['v']);
    wp_enqueue_style('elab-icons', "{$script_vars['elab_icons']}elab-icons.css", array(), $script_vars['v']);
    wp_enqueue_style('elab-admin-css', "{$script_vars['admin_css']}admin.css", array(), $script_vars['v']);
}