<?php
function elab_get_theme_layout() {
    return get_option('stm_layout_mode', 'layout-1');
}

function elab_layout_colors() {
    $colors = array(
        'layout-1' => array(
            '#292e38',
            '#ffc400',
            '#ffc401',
        ),
        'layout-2' => array(
            '#2E1212',
            '#607D8B',
            '#F7DFDF',
        ),
        'layout-3' => array(
            '#2a2c32',
            '#304950',
            '#304950',
        ),
        'layout-4' => array(
            '#222832',
            '#FFC400',
            '#FFC400',
        ),
        'layout-5' => array(
            '#022248',
            '#1241cd',
            '#fde3ee',
        ),
        'layout-6' => array(
            '#2a2c32',
            '#6e62e2',
            '#24c37d',
        ),
        'layout-7' => array(
            '#292e38',
            '#2659CF',
            '#2659CE',
        ),
        'layout-8' => array(
            '#292e38',
            '#EAB11A',
            '#EAB11A',
        ),
        'layout-9' => array(
            '#303841',
            '#31ac26',
            '#31ac26',
        ),
    );

    return $colors[elab_get_theme_layout()];
}

function elab_site_colors() {

    $to = get_option( 'stmt_to_settings', array() );

    $layout_colors = elab_layout_colors();

    $colors = array();

    $colors[] = ( !empty( $to['primary_color'] ) ) ? $to['primary_color'] : $layout_colors[0];
    $colors[] = ( !empty( $to['secondary_color'] ) ) ? $to['secondary_color'] : $layout_colors[1];
    $colors[] = ( !empty( $to['third_color'] ) ) ? $to['third_color'] : $layout_colors[2];

    return $colors;
}

function elab_layout_plugins($layout = 'layout-1', $get_layouts = false) {

	$required = array(
		'x-builder',
		'stm-configurations',
        'woocommerce',
        'contact-form-7',
        'breadcrumb-navxt',
        'yith-woocommerce-compare',
        'yith-woocommerce-wishlist',
        'yith-woocommerce-quick-view'
	);

	$plugins = array(
		'layout-1' => array(
            'revslider',
		),
        'layout-2' => array(
            'revslider',
        ),
        'layout-3' => array(
            'revslider',
        ),
        'layout-4' => array(
            'revslider',
        ),
        'layout-5' => array(
            'revslider',
        ),
        'layout-6' => array(
            'revslider',
        ),
        'layout-7' => array(
            'revslider',
        ),
        'layout-8' => array(
            'revslider',
        ),
        'layout-9' => array(
            'revslider',
        ),
	);

	if ($get_layouts) return $plugins;

	$r = (!empty($plugins[$layout])) ? array_merge($required, $plugins[$layout]) : $required;
	return $r;
}

function elab_recommended_plugins() {
	return array(
	    'dokan-lite',
        'wc-vendors'
    );
}

add_filter('x_bestsellers_large_image', 'elab_x_bestsellers_large_image', 10, 3);

function elab_x_bestsellers_large_image($image, $i, $style) {
    $layout = elab_get_theme_layout();

    if($layout === 'layout-1' && $i === 5) {
        $image = array(240, 275);
    }

    if($layout === 'layout-8' && $i === 5) {
        $image = array(240, 275);
    }

    if($layout === 'layout-6' && $i === 5) {
        $image = array(240, 275);
    }

    if($layout === 'layout-2' && $i === 5 && $style !== 'with_banner') {
        $image = array(240, 275);
    }

    if($layout === 'layout-3' && $i === 5) {
        $image = array(240, 275);
    }

    if(($layout === 'layout-5' || $layout === 'layout-7') && $i === 5) {
        $image = array(240, 275);
    }

    return $image;
}
