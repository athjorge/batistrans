<?php
function elab_pa( $arr, $die = false )
{
    echo '<pre>';
    print_r( $arr );
    echo '</pre>';

    if( $die ) die;
}

function elab_svg_mime( $mimes )
{
    $mimes[ 'ico' ] = 'image/icon';
    $mimes[ 'svg' ] = 'image/svg+xml';
    $mimes[ 'xml' ] = 'application/xml';
    /*TODO delete*/
    $mimes[ 'zip' ] = 'application/zip';

    return $mimes;
}

add_filter( 'upload_mimes', 'elab_svg_mime', 100 );

function elab_body_classes( $classes )
{
    if( !is_singular() ) {
        $classes[] = '';
    }
    $layout = elab_get_theme_layout();

    $classes[] = 'elab_header_style_' . elab_get_option('header_style', 1);
    $classes[] = 'post_archive_' . elab_get_option( 'post_archive_style', 'style_1' );
    $classes[] = 'footer_' . elab_get_option( 'footer_main_style', 'style_1' );
    $classes[] = ( defined( 'STM_HB_VER' ) ) ? 'header-builder' : 'header-simple';
    $classes[] = "elab-{$layout}";
    $classes[] = "product_single_" . elab_single_product_style();

    $enable_preloader = elab_get_option( 'enable_preloader', true );
    if( $enable_preloader ) $classes[] = 'elab_enable_preloader';

    return $classes;
}

add_filter( 'body_class', 'elab_body_classes' );

function elab_pingback_header()
{
    if( is_singular() && pings_open() ) {
        printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
    }
}

add_action( 'wp_head', 'elab_pingback_header' );

function elab_minimize_word( $word, $length = '40', $affix = '...' )
{

    if( !empty( intval( $length ) ) ) {
        $w_length = mb_strlen( $word );
        if( $w_length > $length ) {
            $word = mb_strimwidth( $word, 0, $length, $affix );
        }
    }

    return sanitize_text_field( $word );
}

function elab_stored_theme_options()
{
    $options = get_option( 'stmt_to_settings', array() );
    return apply_filters( 'elab_stored_theme_options', $options );
}

function elab_get_option( $option_name, $default = false )
{
    $options = elab_stored_theme_options();
    $option = null;

    if( isset( $options[ $option_name ] ) ) {
        $option = $options[ $option_name ];
    } elseif( isset( $default ) ) {
        $option = $default;
    }

    return $option;
}

function elab_reset_custom_styles()
{
    if( !empty( $_GET[ 'rcs' ] ) ) {
        require_once( get_template_directory() . '/inc/admin/css/css-php.php' );
        elab_generate_styles();
    }
}

add_action( 'init', 'elab_reset_custom_styles', 100 );

function elab_get_image_url( $id, $size = 'full' )
{
    $url = '';
    if( !empty( $id ) ) {
        $image = wp_get_attachment_image_src( $id, $size, false );
        if( !empty( $image[ 0 ] ) ) {
            $url = $image[ 0 ];
        }
    }

    return $url;
}

function my_search_form()
{
    $form = '<form role="search" method="get" class="search-form" action="' . esc_url( home_url( '/' ) ) . '" >
        <input type="text" placeholder="' . esc_attr__( 'Search...', 'elab' ) . '" value="' . get_search_query() . '" name="s" class="search-field" />
        <button type="submit" class="search-submit"><span class="lnricons-magnifier"></span></button>
    </form>';

    return $form;
}

add_filter( 'get_search_form', 'my_search_form', 100 );

function elab_update_layout()
{
    if( isset( $_GET[ 'stm_update_layout' ] ) ) {
        $layout = $_GET[ 'stm_update_layout' ];
        update_option( 'stm_layout_mode', $layout );
    }
}

add_action( 'init', 'elab_update_layout' );

function elab_assets_version()
{
    if( WP_DEBUG ) return time();

    $theme = wp_get_theme();
    return $theme->get( 'Version' );
}

function elab_filtered_output( $data )
{
    return apply_filters( 'elab_filtered_output', $data );
}

function elab_enqueue_parted_style( $name, $parts = 'header_parts/', $inline = '' )
{

    $handle = "elab-{$name}-{$parts}";

    $script_vars = elab_scripts_vars();

    wp_enqueue_style(
        $handle,
        "{$script_vars['css']}partials/{$parts}{$name}.css",
        array(),
        elab_assets_version()
    );
    if( !empty( $inline ) ) wp_add_inline_style( $handle, $inline );
}

function elab_enqueue_parted_script( $name, $deps = array(), $inline = '', $localize_name = '', $localize = array() )
{
    $handle = "elab-{$name}";
    wp_enqueue_script(
        $handle,
        get_template_directory_uri() . "/assets/js/partials/{$name}.js",
        $deps,
        elab_assets_version()
    );

    if( !empty( $inline ) ) {
        wp_add_inline_script( $handle, $inline );
    }

    if( !empty( $localize ) and !empty( $localize_name ) ) {
        wp_localize_script( $handle, $localize_name, $localize );
    }

}

function elab_get_cropped_image_url( $image_id, $width, $height, $crop = true )
{
    if( function_exists('stm_x_builder_get_cropped_image_url') ) {
        return stm_x_builder_get_cropped_image_url( $image_id, $width, $height, $crop );
    }
    return elab_get_image_url( $image_id, "{$width}x{$height}" );
}

function elab_get_cropped_image( $image_id, $width, $height, $crop = true )
{
    $image_url = elab_get_cropped_image_url( $image_id, $width, $height, $crop );
    if( empty( $image_url ) ) return "";
    return "<img src='{$image_url}' width='{$width}' height='{$height}' alt='" . esc_attr__( 'Image', 'elab' ) . "' />";
}

function elab_pagination( $pagination = array(), $defaults = array() )
{

    $pagination[ 'prev_text' ] = '<i class="lnricons-chevron-left"></i>';
    $pagination[ 'next_text' ] = '<i class="lnricons-chevron-right"></i>';
    $pagination[ 'type' ] = 'array';

    $pagination = wp_parse_args( $pagination, $defaults );

    $pagination = paginate_links( $pagination );
    if( !empty( $pagination ) ):
        $has_prev = '';
        $has_next = '';
        foreach( $pagination as $page ) {
            if( strpos( $page, 'prev page-numbers' ) !== false ) $has_prev = 'stm_has_prev';
            if( strpos( $page, 'next page-numbers' ) !== false ) $has_next = 'stm_has_next';
        }


        ob_start();

        ?>
        <ul class="pagination <?php echo esc_attr( $has_prev . ' ' . $has_next ) ?>">
            <?php foreach( $pagination as $key => $page ):
                $class = 'page-item';
                if( strpos( $page, 'prev' ) !== false ) $class = 'stm_prev';
                if( strpos( $page, 'next' ) !== false ) $class = 'stm_next';
                ?>
                <li class="<?php echo esc_attr( $class ); ?>">
                    <?php echo wp_kses_post( $page ); ?>
                </li>
            <?php endforeach; ?>
        </ul>

        <?php $pagination = ob_get_clean();
    endif;

    return $pagination;
}

add_action( 'wp_ajax_elab_like_portfolio', 'elab_like_portfolio' );
add_action( 'wp_ajax_nopriv_elab_like_portfolio', 'elab_like_portfolio' );
function elab_like_portfolio()
{
    check_ajax_referer( 'elab_like_portfolio', 'security' );
    $post_id = intval( $_GET[ 'post_id' ] );
    $likes = get_post_meta( $post_id, 'likes_count', true );
    $likes = ( empty( $likes ) ) ? 0 : $likes;
    $likes = intval( $likes ) + 1;
    update_post_meta( $post_id, 'likes_count', $likes );
    wp_send_json( $likes );
}

function elab_get_posts( $post_type = 'post' )
{
    $r = array(
        '' => esc_html__( 'Select', 'elab' )
    );
    if( !is_admin() ) return $r;

    $args = array(
        'post_type' => $post_type,
        'posts_per_page' => -1,
    );

    $q = new WP_Query( $args );


    if( $q->have_posts() ) {
        while ( $q->have_posts() ) {
            $q->the_post();

            $r[ get_the_ID() ] = get_the_title();

        }

        wp_reset_postdata();
    }

    return $r;

}

function elab_get_page_url( $query_args = array() )
{
    global $wp;
    if( !empty( $_GET ) and empty( $query_args ) ) $query_args = $_GET;
    return home_url( add_query_arg( array( $query_args ), $wp->request ) );
}

function elab_add_query_arg( $args )
{
    return add_query_arg( $args, elab_get_page_url() );
}

function elab_get_banner_id( $term_id = '' )
{
    $shop_banner = elab_get_option( 'shop_banner' );

    if( empty( $term_id ) ) {
        $queried_object = get_queried_object();

        if( !empty( $queried_object->term_id ) ) $term_banner = get_term_meta( $queried_object->term_id, 'shop_banner', true );

        if( !empty( $term_banner ) ) $shop_banner = $term_banner;
    } else {
        $term_banner = get_term_meta( $term_id, 'shop_banner', true );
        if( !empty( $term_banner ) ) $shop_banner = $term_banner;
    }

    return $shop_banner;
}

function elab_single_product_style()
{
    $style = elab_get_option( 'single_product_style', 'style_1' );
    if( elab_is_product() ) {
        $product_style = get_post_meta( get_the_ID(), 'product_style', true );
        if( !empty( $product_style ) ) $style = $product_style;
    }

    return $style;
}

function elab_is_product()
{
    return ( function_exists( 'is_product' ) ) ? is_product() : false;
}

function elab_get_current_page()
{
    global $pagenow;
    return $pagenow;
}

function elab_get_adjacent_post( $direction = 'prev', $post_types = 'post' )
{
    if( !function_exists( 'stmt_configurations_get_adjacent_post' ) ) return false;

    return stmt_configurations_get_adjacent_post( $direction, $post_types );
}

function elab_is_shop()
{
    return ( function_exists( 'is_shop' ) ) ? is_shop() : false;
}

function elab_is_product_category()
{
    return ( function_exists( 'is_product_category' ) ) ? is_product_category() : false;
}

function elab_highlight_words($text, $word){
$text = preg_replace('#'. preg_quote($word) .'#i', '<span class="hlw">\\0</span>', $text);
    return $text;
}