<?php elab_enqueue_parted_style('style-2', 'widgets/news/'); ?>
<a class="widget_media__single" href="<?php the_permalink(); ?>">

    <?php if (has_post_thumbnail()): ?>
        <div class="widget_media__image">
            <img alt="<?php esc_attr(get_the_title()) ?>"
                 src="<?php echo esc_url(elab_get_cropped_image_url(get_post_thumbnail_id(), '100', '100')) ?>"/>
        </div>
    <?php endif; ?>

    <div class="widget_media__content">
        <div class="widget_media__date heading_font">
            <span><?php echo get_the_date('d'); ?></span>
            <div class="label"><?php echo get_the_date('M'); ?></div>
        </div>
        <h3><?php the_title(); ?></h3>
    </div>
</a>