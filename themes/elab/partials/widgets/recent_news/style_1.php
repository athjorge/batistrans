<?php elab_enqueue_parted_style('style-1', 'widgets/news/'); ?>
<a class="widget_media__single" href="<?php the_permalink(); ?>">
    <div class="widget_media__date heading_font">
        <span><?php echo get_the_date('d'); ?></span>
        <div class="label"><?php echo get_the_date('M'); ?></div>
    </div>
    <div class="widget_media__image">
        <?php if (has_post_thumbnail()): ?>
            <img alt="<?php echo esc_attr(get_the_title()) ?>"
                 src="<?php echo esc_url(elab_get_cropped_image_url(get_post_thumbnail_id(), '285', '285')) ?>"/>
        <?php else: ?>
            <div class="widget_media__holder"></div>
        <?php endif; ?>
        <div class="widget_media__content">
            <div class="author">
                <?php printf(esc_html__('by %s', 'elab'), get_the_author()); ?>
            </div>
            <h3><?php the_title(); ?></h3>
            <span><?php esc_html_e('Read More', 'elab'); ?></span>
        </div>
    </div>
</a>