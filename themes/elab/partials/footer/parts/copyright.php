<?php
$copyright = elab_get_option('copyright');
$copyright_image = elab_get_option('copyright_image', '');
if (!empty($copyright_image)) $copyright_image = elab_get_image_url($copyright_image, 'full');
if ($copyright || $copyright_image):
    elab_enqueue_parted_style('copyright', 'footer_parts/');
    ?>

    <div class="copyright_box">

        <div class="container">

            <div class="copyright text-center">

                <div class="copyright-text"><?php echo wp_kses_post($copyright); ?></div>

                <?php if (!empty($copyright_image)): ?>
                    <div class="copyright-image">
                        <img src="<?php echo esc_url($copyright_image); ?>"
                             alt="<?php esc_attr_e('Copyright image', 'elab'); ?>"/>
                    </div>
                <?php endif; ?>

            </div>

        </div>

    </div>

<?php endif;