<?php elab_enqueue_parted_style('footer_widgets', ''); ?>

<?php
if (
    is_active_sidebar('footer-1') or
    is_active_sidebar('footer-2') or
    is_active_sidebar('footer-3') or
    is_active_sidebar('footer-4') or
    is_active_sidebar('footer-5') or
    is_active_sidebar('footer-6')): ?>

    <div class="widgets_box">
        <div class="container">

            <div class="row">
                <?php for ($i = 1; $i <= 6; $i++): ?>
                    <?php if (is_active_sidebar("footer-{$i}")):
                        $md_width = elab_get_option("widget_area_{$i}_width", '3');
                        $sm_width = elab_get_option("widget_area_{$i}_width_sm", $md_width);
                        ?>
                        <div class="col-lg-<?php echo esc_attr($md_width) ?> col-md-<?php echo esc_attr($sm_width); ?>">
                            <?php dynamic_sidebar("footer-{$i}"); ?>
                        </div>
                    <?php endif; ?>
                <?php endfor; ?>
            </div>

        </div>
    </div>
<?php endif; ?>