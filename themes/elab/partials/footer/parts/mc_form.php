<?php
$mc_list_id = elab_get_option('mc_list_id', '');
$mc_api_key = elab_get_option('mc_api_key', '');

elab_enqueue_parted_style('footer_mailchimp', '');
elab_enqueue_parted_script('footer_mailchimp', '');

if (!empty($mc_api_key) and !empty($mc_list_id)): ?>
    <div class="iconbox_form">
        <form action="/" class="stm_subscribe">
            <div class="stm_mailchimp_unit">
                <div class="form-group">
                    <input type="email"
                           name="email"
                           class="form-control stm_subscribe_email"
                           placeholder="<?php esc_attr_e('Enter your E-mail to subscribe...', 'elab'); ?>"
                           required/>
                    <button class="btn btn-outline-primary">
                        <span><?php esc_html_e('Subscribe', 'elab'); ?></span>
                    </button>
                </div>
                <div class="stm_subscribe_preloader"><?php esc_html_e('Please wait...', 'elab'); ?></div>
            </div>
        </form>
    </div>
<?php endif; ?>