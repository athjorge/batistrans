<?php
elab_enqueue_parted_style('footer_mailchimp', '');
elab_enqueue_parted_script('footer_mailchimp', '');
$enable_footer_maiclhimp = elab_get_option('enable_footer_maiclhimp', false);
$is_coming_soon = is_page_template('coming-soon.php');
if ($enable_footer_maiclhimp or $is_coming_soon):

    $mc_list_id = elab_get_option('mc_list_id', '');
    $mc_api_key = elab_get_option('mc_api_key', '');

    $iconboxes = array(
        array(
            'prefix' => '',
            'image' => get_template_directory_uri() . '/assets/images/iconboxes/skype.svg',
        ),
        array(
            'prefix' => '_2',
            'image' => get_template_directory_uri() . '/assets/images/iconboxes/phone-call.svg',
        ),
        array(
            'prefix' => '_3',
            'image' => get_template_directory_uri() . '/assets/images/iconboxes/mail.svg',
        ),
    );

    ?>

    <div class="elab_mailchimp">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-xl-7 col-lg-6 col-md-6 col-sm-12">

                    <div class="iconboxes">
                        <?php foreach ($iconboxes as $iconbox):

                            $prefooter_icon = elab_get_option("prefooter_icon{$iconbox['prefix']}", '');
                            $prefooter_title = elab_get_option("prefooter_title{$iconbox['prefix']}", '');
                            $prefooter_subtitle = elab_get_option("prefooter_subtitle{$iconbox['prefix']}", '');


                            if (empty($prefooter_title)) continue;
                            ?>
                            <div class="iconbox">

                                <?php if (!empty($prefooter_icon)): ?>
                                    <div class="iconbox__icon">
                                        <i class="<?php echo esc_attr($prefooter_icon); ?>"></i>
                                    </div>
                                <?php else: ?>
                                    <div class="iconbox__icon">
                                        <img src="<?php echo esc_url($iconbox['image']) ?>" alt="<?php esc_attr_e('Iconbox image', 'elab'); ?>" />
                                    </div>
                                <?php endif; ?>


                                <div class="iconbox__content">

                                    <?php if (!empty($prefooter_title)): ?>
                                        <div class="iconbox__title heading_font">
                                            <?php printf(_x('%s', 'Prefooter Title', 'elab'), $prefooter_title); ?>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (!empty($prefooter_subtitle)): ?>
                                        <div class="iconbox__subtitle">
                                            <?php printf(_x('%s', 'Prefooter Subtitle', 'elab'), $prefooter_subtitle); ?>
                                        </div>
                                    <?php endif; ?>

                                </div>

                            </div>
                        <?php endforeach; ?>
                    </div>

                </div>

                <div class="col-xl-5 col-lg-6 col-md-6 col-sm-12">
                    <div class="elab-mc-form-wrapper">
                        <?php get_template_part('partials/footer/parts/mc_form'); ?>
                    </div>
                </div>
            </div>


        </div>
    </div>

<?php endif;