<?php

$prefooter_position = elab_get_option('prefooter_position', 'pre');

if ($prefooter_position === 'pre') {
    get_template_part('partials/footer/parts/mailchimp');
}

get_template_part('partials/footer/parts/widgets');

if ($prefooter_position === 'inside') {
    get_template_part('partials/footer/parts/mailchimp');
}

get_template_part("partials/footer/parts/copyright");

if ($prefooter_position === 'after') {
    get_template_part('partials/footer/parts/mailchimp');
}