<?php
$style = elab_get_option('product_card_style', 'style_1');
elab_enqueue_parted_style("product_buttons/{$style}", 'woocommerce/');

get_template_part("partials/woocommerce/product_buttons/{$style}");