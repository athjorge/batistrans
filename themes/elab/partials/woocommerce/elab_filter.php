<a href="#" class="elab_filter_button btn btn-outline-primary">
    <i class="lnricons-arrow-down"></i>
    <i class="lnricons-arrow-up"></i>
    <?php esc_html_e('Filter', 'elab'); ?>
</a>