<?php
elab_enqueue_parted_style('product_iconbox', 'woocommerce/');
$post_id = get_the_ID();
$iconbox_title = get_post_meta($post_id, 'iconbox_title', true);

$icon_box = array();
for ($i = 1; $i <= 5; $i++) {
    $icon = get_post_meta($post_id, "iconbox_icon_{$i}", true);
    $title = get_post_meta($post_id, "iconbox_title_{$i}", true);

    if (!empty($icon) and !empty($title)) {
        $icon_box[] = array(
            'icon' => $icon,
            'title' => $title,
        );
    }
}

if (!empty($iconbox_title) and !empty($icon_box)):
    ?>
    <div class="clearfix"></div>
    <div class="elab_product_iconbox" data-fullwidth="1">
        <div class="container">
            <h3 class="text-center elab_product_iconbox__main_title"><?php echo sanitize_text_field($iconbox_title); ?></h3>

            <div class="elab_product_iconbox__wrapper">
                <?php foreach ($icon_box as $box): ?>
                    <div class="elab_product_iconbox__single">
                        <div class="elab_product_iconbox__single__icon">
                            <i class="<?php echo esc_attr($box['icon']); ?>"></i>
                        </div>
                        <div class="elab_product_iconbox__single__title">
                            <h4><?php echo sanitize_text_field($box['title']); ?></h4>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>

        </div>
    </div>
    <div class="clearfix"></div>

<?php endif;