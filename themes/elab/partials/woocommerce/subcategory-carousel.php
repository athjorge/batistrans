<?php
$shop_page_style = elab_get_option( 'shop_page_style', 'default' );
if( isset( $_GET[ 'shop_page_style' ] ) && !empty( $_GET[ 'shop_page_style' ] ) ) {
    $shop_page_style = sanitize_text_field( $_GET[ 'shop_page_style' ] );
}
if($shop_page_style != 'full_width') return;
$queried_obj = get_queried_object();
$term_id = ( !empty( $queried_obj->term_id ) ) ? $queried_obj->term_id : '';
if( !empty( $term_id ) ):
    $child_terms = get_term_children( $term_id, 'product_cat' );
    ?>

    <div class="elab_category_sub_carousel owl-carousel">
        <?php foreach( $child_terms as $child_id ): ?>
            <?php
            $child_term = get_term( $child_id, 'product_cat', OBJECT );
            $child_term_thumbnail_id = get_term_meta( $child_id, 'thumbnail_id', true );
            if( $child_term->count == 0 ) continue;
            ?>
            <a href="<?php echo esc_url( get_term_link( $child_id ) ); ?>" class="elab_category_sub_carousel_item">
                <?php if( !empty( $child_term_thumbnail_id ) ): ?>
                    <div class="elab_category_sub_carousel_item__image">
                        <?php echo elab_get_cropped_image( $child_term_thumbnail_id, '200', '120', true ); ?>
                    </div>
                <?php endif; ?>
                <?php if( !empty( $child_term->name ) ): ?>
                    <h4><?php echo esc_html( $child_term->name ); ?></h4>
                <?php endif; ?>
                <?php if( !empty( $child_term->description ) ): ?>
                    <div class="elab_category_sub_carousel_item__description">
                        <?php echo wp_kses_post( $child_term->description ); ?>
                    </div>
                <?php endif; ?>
                <?php if( defined( 'X_BUILDER_DIR' ) ): ?>
                    <?php
                    $lowest_price = stm_x_get_min_price_per_product_cat( $child_id );
                    if( !empty( $lowest_price ) ):
                        ?>
                        <div class="elab_category_sub_carousel_item__lowest_price">
                            <?php
                            esc_html_e( 'Starting at', 'elab' );
                            echo '<span> <span class="symbol">' . esc_html( get_woocommerce_currency_symbol() ) . '</span>' . esc_html( $lowest_price ) . '</span>';
                            ?>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </a>
        <?php endforeach; ?>
    </div>

<?php endif; ?>