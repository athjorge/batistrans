<?php
/**
 * @var $id
 *
 */

?>

<div class="x_builder_product_buttons product">

    <div data-tooltip="<?php esc_attr_e('Add to cart', 'elab') ?>">
        <?php woocommerce_template_loop_add_to_cart(); ?>
    </div>

    <?php if (defined('YITH_WCWL')):
        $url = YITH_WCWL()->get_wishlist_url();
        ?>
        <div class="yith-wcwl-add-to-wishlist add-to-wishlist-<?php echo esc_attr($id); ?>">

            <div class="yith-wcwl-add-button" data-tooltip="<?php esc_attr_e('Add to wishlist', 'elab'); ?>">

                <a href="#"
                   rel="nofollow"
                   data-product-id="<?php echo esc_attr($id); ?>"
                   class="add_to_wishlist">
                    <i class="lnricons-heart"></i>
                    <span><?php esc_html_e('Add to Wishlist', 'elab'); ?></span>
                </a>

            </div>

            <div class="yith-wcwl-wishlistexistsbrowse hide" style="display: none;"
                 data-tooltip="<?php esc_attr_e('View wishlist', 'elab'); ?>">
                <a href="<?php echo esc_url($url); ?>" rel="nofollow">
                    <i class="lnricons-heart"></i>
                    <span><?php esc_html_e('Browse Wishlist', 'elab'); ?></span>
                </a>
            </div>

            <div class="yith-wcwl-wishlistaddedbrowse hide" style="display: none;"
                 data-tooltip="<?php esc_attr_e('View wishlist', 'elab'); ?>">
                <a href="<?php echo esc_url($url); ?>" rel="nofollow">
                    <i class="lnricons-heart"></i>
                    <span><?php esc_html_e('Browse Wishlist', 'elab'); ?></span>
                </a>
            </div>

        </div>

    <?php endif; ?>

    <?php if (defined('YITH_WOOCOMPARE')): ?>
        <a href="#"
           class="compare button x_compare"
           data-product_id="<?php echo intval($id); ?>"
           data-tooltip="<?php esc_attr_e('Add to compare', 'elab'); ?>"
           rel="nofollow">
            <i class="lnricons-shuffle"></i>
            <span><?php esc_html_e('Compare', 'elab'); ?></span>
        </a>
    <?php endif; ?>

    <?php if (class_exists('YITH_WCQV')): ?>
        <a href="#"
           class="quick_view yith-wcqv-button"
           data-product_id="<?php echo intval($id); ?>"
           data-tooltip="<?php esc_attr_e('Quick View', 'elab'); ?>">
            <i class="lnricons-eye"></i>
        </a>
    <?php endif; ?>
</div>
