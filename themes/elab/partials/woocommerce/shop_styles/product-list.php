<?php
defined('ABSPATH') || exit;

global $product;

// Ensure visibility.
if (empty($product) || !$product->is_visible()) {
    return;
}

$gallery = $product->get_gallery_image_ids();
$terms = get_the_terms(get_the_ID(), 'product_cat');
$sku = !empty($product->get_sku()) ? $product->get_sku() : '';
$average = $product->get_average_rating();
$average = round($average, 1);
?>

<div class="elab_product_description">
    <a href="<?php echo esc_url(get_permalink()); ?>">
        <?php
        do_action('woocommerce_shop_loop_item_title');
        ?>
    </a>
    <div class="elab_product_categories">
        <?php if (!empty($terms)): ?>
            <span class="category_label"><?php esc_html_e('Category:', 'elab'); ?></span>
            <?php foreach ($terms as $term): ?>
                <a href="<?php echo esc_url(get_term_link($term->term_id, 'product_cat')); ?>">
                    <?php echo esc_html($term->name); ?>
                </a>
            <?php endforeach; ?>
            <span class="separator">|</span>
        <?php endif; ?>

        <?php if (!empty($sku)): ?>
            <span class="sku">
                        <span class="sky__label"><?php esc_html_e('SKU:', 'elab'); ?></span>
                        <span class="sky__value"><?php echo esc_html(' ' . $sku); ?></span>
                    </span>
        <?php endif; ?>
    </div>
    <div class="elab_product_excerpt">
        <?php echo get_the_excerpt(); ?>
    </div>
</div>
<div class="elab_product_info">
    <div class="elab_product_info__price">
        <?php
        $regular_price = $product->get_regular_price();
        $sale_price = $product->get_sale_price();
        $price_class = 'regular_price';
        if (empty($sale_price)) {
            $price_class = 'sale_price';
        }
        $symbol = get_woocommerce_currency_symbol();
        $price_prefix = '';
        if (!empty($sale_price)) {
            $price_prefix = esc_html__('Was', 'elab') . ' ';
            $sale_price = floatval($sale_price);
            echo '<span class="sale_price"><span>' . esc_html($symbol) . '</span>' . esc_html(number_format($sale_price, 2, '.', ' ')) . '</span>';
        }
        if (!empty($regular_price)) {
            $regular_price = floatval($regular_price);
            echo '<span class="' . esc_attr($price_class) . '"><span>' . esc_html($price_prefix) . esc_html($symbol) . '</span>' . esc_html(number_format($regular_price, 2, '.', ' ')) . '</span>';
        }
        if (empty($regular_price) && empty($sale_price)) {
            echo '<span class="default-price">' . wp_kses_post($product->get_price_html()) . '</span>';
        }
        ?>
    </div>
    <?php if (!empty($regular_price) && !empty($sale_price)): ?>
        <div class="save_label">
                    <span>
                        <?php
                        $sale_value = floatval($regular_price) - floatval($sale_price);
                        esc_html_e('Save', 'elab');
                        echo ' ' . esc_html($symbol . $sale_value);
                        ?>
                    </span>
        </div>
    <?php endif; ?>
    <div class="elab_product_info__buttons">
        <?php
        //elab_enqueue_parted_style("product_buttons/style_1", 'woocommerce/');
        get_template_part("partials/woocommerce/product_buttons/style_1");
        ?>
    </div>
    <div class="elab_product_info__rating elab_woo_reviews">
        <div class="average_rating_unit">
            <div class="average-rating-stars">
                <?php do_action('woocommerce_after_shop_loop_item_title'); ?>
                <?php if (!empty($average)): ?>
                    <div class="average_rating_value"><?php echo esc_html($average); ?></div>
                <?php endif; ?>
                <?php
                if (get_option('woocommerce_enable_review_rating') === 'yes' && ($count = $product->get_review_count())): ?>
                    <a href="<?php the_permalink(); ?>#tab-reviews" class="review_count"> |
                        <?php
                        printf(esc_html(_n('%1$s review', '%1$s reviews', $count, 'elab')), esc_html($count));
                        ?>
                    </a>
                <?php endif; ?>
            </div>
            <div class="elab_stock">
                <?php
                $stock = $product->get_stock_quantity();
                if ($stock != null) {
                    $in_stock = esc_html__('In Stock', 'elab') . '<span class="stock"> ' . esc_html($stock) . ' </span><span class="label">' . esc_html__('Available', 'elab') . '</span>';
                    if ($stock < 1) {
                        $in_stock = esc_html__('Out of Stock', 'elab');
                    }
                    echo wp_kses_post($in_stock);
                }
                ?>
            </div>
        </div>

    </div>
</div>
