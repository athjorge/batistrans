<?php
if (defined('YITH_WOOCOMPARE_VERSION')):
    global $yith_woocompare; ?>

    <div class="elab_woocommerce_compare">
        <span class="product">
            <a href="<?php echo esc_url($yith_woocompare->obj->view_table_url()); ?>" class="yith-woocompare-open">
                <i class="lnricons-shuffle"></i>
                <?php esc_html_e('Compare', 'elab'); ?>
            </a>
        </span>
    </div>

<?php endif;