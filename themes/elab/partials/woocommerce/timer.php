<?php if (defined('STM_X_BUILDER_V')) :

    $id = get_the_ID();
    $_product = wc_get_product($id);

    $sale_price = $_product->get_sale_price();
    $sale_price = (!empty($sale_price)) ? strip_tags(wc_price($_product->get_sale_price())) : '';
    $sale_to = $_product->get_date_on_sale_to();
    if (!empty($sale_to)) {
        $current_time = time();
        $sale_to = strtotime($sale_to);
        $sale_to = ($current_time < $sale_to) ? date('M j, Y G:i:s', $sale_to) : null;

        stm_x_builder_register_script('timer', array('vue.js'));
        stm_x_builder_register_style('timer');
        elab_enqueue_parted_script('single_timer');
        ?>

        <div class="x_single_timer">

            <Timer :starttime="'<?php echo esc_attr($sale_to) ?>'"
                   :endtime="'<?php echo esc_attr($sale_to) ?>'"
                   trans='{"day":"<?php esc_attr_e('Day', 'elab') ?>","hours":"<?php esc_attr_e('Hours', 'elab') ?>","minutes":"<?php esc_attr_e('Minutes', 'elab') ?>","seconds":"<?php esc_attr_e('Seconds', 'elab') ?>"}'>
            </Timer>

        </div>

    <?php };
endif;