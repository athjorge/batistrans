<div class="clearfix"></div>
<div class="elab_compare_wishlist">

    <?php if (defined('YITH_WOOCOMPARE_VERSION')):
        global $yith_woocompare;
        ?>
        <div class="elab_compare elab_after_add_buttons">
            <span class="product">
                <a href="<?php echo esc_url($yith_woocompare->obj->add_product_url(get_the_ID())); ?>"
                   class="compare"
                   data-product_id="<?php echo esc_attr(get_the_ID()); ?>">
                    <i class="lnricons-shuffle"></i>
                    <?php esc_html_e('Compare', 'elab'); ?>
                </a>
            </span>
        </div>
    <?php endif; ?>

    <?php if (defined('YITH_WCWL')): ?>
        <div class="elab_wishlist elab_after_add_buttons">
            <?php echo do_shortcode('[yith_wcwl_add_to_wishlist]'); ?>
        </div>
    <?php endif; ?>

    <?php
    if(function_exists('stm_get_shares') && elab_is_product()){
        $single_product_style = elab_single_product_style();
        if($single_product_style === 'style_4'){
            stm_get_shares();
        }
    }
    ?>
</div>