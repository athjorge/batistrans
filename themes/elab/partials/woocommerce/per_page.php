<?php
elab_enqueue_parted_script('woocommerce_per_page');
$per_pages = array(
    5, 15, 20, get_option('posts_per_page')
);
$current_per_page = (!empty($_GET['per_page'])) ? intval($_GET['per_page']) : get_option( 'posts_per_page' );

if(!in_array($current_per_page, $per_pages)) $per_pages[] = $current_per_page;
sort($per_pages);
?>

<div class="elab_woocommerce_per_page">
    <form method="get">
        <select name="per_page" data-src="<?php echo esc_url(elab_get_page_url()); ?>">

            <?php foreach($per_pages as $per_page): ?>
                <option value="<?php echo esc_attr($per_page); ?>"
                        <?php echo selected($per_page, $current_per_page); ?>
                        data-src="<?php echo esc_url(elab_add_query_arg(array('per_page' => $per_page))); ?>">
                    <?php printf(esc_html__('Show %s', 'elab'), $per_page); ?>
                </option>
            <?php endforeach; ?>

        </select>
    </form>
</div>