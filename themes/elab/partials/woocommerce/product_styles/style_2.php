<?php
defined('ABSPATH') || exit;

do_action('woocommerce_before_single_product');

if (post_password_required()) {
    echo get_the_password_form(); // WPCS: XSS ok.
    return;
}

?>
    <div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>

        <div class="elab_product_main">
            <?php do_action('woocommerce_before_single_product_summary'); ?>

            <div class="summary entry-summary">
                <?php do_action('woocommerce_single_product_summary'); ?>
            </div>

            <div class="clearfix"></div>

        </div>

        <?php get_template_part('partials/woocommerce/iconbox'); ?>
        <?php get_template_part('partials/woocommerce/hint_images'); ?>

        <?php
        /**
         * Hook: woocommerce_after_single_product_summary.
         *
         * @hooked woocommerce_output_product_data_tabs - 10
         * @hooked woocommerce_upsell_display - 15
         * @hooked woocommerce_output_related_products - 20
         */
        do_action('woocommerce_after_single_product_summary');
        ?>
    </div>

<?php do_action('woocommerce_after_single_product'); ?>