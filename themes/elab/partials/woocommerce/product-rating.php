<?php
$rating_enabled = get_option('woocommerce_enable_reviews');
$comments_open = comments_open();

if ($rating_enabled == 'yes' and $comments_open):
    $product = wc_get_product(get_the_id());
    $rating_count = $product->get_rating_count();
    $average = $product->get_average_rating();
    $average = round($average, 1); ?>

    <!-- Reviews -->
    <div class="elab_woo_reviews clearfix">
        <!-- Reviews Average ratings -->
        <div class="average_rating">
            <h4 class="rating_sub_title"><?php esc_html_e('Customer Rating', 'elab'); ?></h4>
            <div class="average_rating_unit heading_font">
                <div class="average-rating-stars">
                    <?php do_action('woocommerce_after_shop_loop_item_title'); ?>
                    <div class="average_rating_value"><?php echo esc_attr($average); ?></div>
                </div>
                <div class="clearfix"></div>
                <div class="average_rating_num">
                    <?php echo esc_attr($rating_count . ' ' . esc_html__('Ratings', 'elab')); ?>
                </div>
            </div>
        </div>
        <!-- Reviews Average ratings END -->

        <!-- Review detailed Rating -->
        <?php
        $comments = get_approved_comments(get_the_ID());

        $rate1 = $rate2 = $rate3 = $rate4 = $rate5 = 0;
        // The Comment Loop
        if ($comments) {
            foreach ($comments as $comment) {
                $rate = get_comment_meta($comment->comment_ID, 'rating', true);
                switch ($rate) {
                    case 1:
                        $rate1++;
                        break;
                    case 2:
                        $rate2++;
                        break;
                    case 3:
                        $rate3++;
                        break;
                    case 4:
                        $rate4++;
                        break;
                    case 5:
                        $rate5++;
                        break;
                } // switch
            }
        }
        $rates = array('5' => $rate5, '4' => $rate4, '3' => $rate3, '2' => $rate2, '1' => $rate1);
        ?>
        <div class="detailed_rating">
                <?php foreach ($rates as $key => $rate): ?>
                    <?php
                    if ($rate != 0 or $rating_count != 0) {
                        $fill_value = round($rate * 100 / $rating_count, 2);
                    } else {
                        $fill_value = 0;
                    }
                    ?>
                    <div class="stars_<?php echo esc_attr($key); ?>">
                        <div class="key"><?php echo esc_attr(__('Stars', 'elab') . ' ' . $key); ?></div>
                        <div class="value">(<?php echo esc_attr($rate); ?>)</div>
                        <div class="bar">
                            <div class="bar_filler" style="width:<?php echo esc_attr($fill_value); ?>%"></div>
                        </div>
                    </div>
                <?php endforeach; ?>
        </div>
        <!-- Review detailed Rating END -->
    </div> <!-- clearfix -->
    <div class="multiseparator"></div>

    <?php wp_reset_query(); ?>

<?php endif; ?>