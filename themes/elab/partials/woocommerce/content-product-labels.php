<?php
$product_id = get_the_ID();
$_product = wc_get_product($id);

$sale_price = $_product->get_sale_price();
$regular_price = $_product->get_regular_price();

$quantity = $_product->get_stock_quantity();

if(!empty($sale_price)) {
    $sale_discount = strip_tags(wc_price($regular_price - $sale_price));
}

if(!empty($sale_discount)):
    ?>
    <div class="elab_woocommerce_label elab_woocommerce_label__sale">
        <span><?php printf(esc_html__('Save %s', 'elab'), $sale_discount); ?></span>
    </div>
<?php endif;

if(isset($quantity) and $quantity < 10): ?>
    <div class="elab_woocommerce_label elab_woocommerce_label__">
        <span><?php printf(esc_html__('%s items left', 'elab'), $quantity); ?></span>
    </div>
<?php endif;