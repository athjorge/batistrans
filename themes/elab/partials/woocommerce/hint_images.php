<?php
$product_id = get_the_ID();
$image_1 = get_post_meta($product_id, 'hint_image_1', true);
$image_2 = get_post_meta($product_id, 'hint_image_2', true);
$image_3 = get_post_meta($product_id, 'hint_image_3', true);

$image_1 = (!empty($image_1)) ? json_decode($image_1, true) : '';
$image_2 = (!empty($image_2)) ? json_decode($image_2, true) : '';
$image_3 = (!empty($image_3)) ? json_decode($image_3, true) : '';

$sizes = array(
    array('740', '400'),
    array('985', '760'),
    array('890', '630'),
);

if (!empty($image_1['image_id']) and !empty($image_2['image_id']) and !empty($image_3['image_id'])):
    elab_enqueue_parted_style('hint_images', 'woocommerce/');
    elab_enqueue_parted_script('hint_images', array('imagesloaded', 'packery'));
    ?>

    <div class="elab_hint_images" data-fullwidth="1">

        <?php for ($i = 1; $i < 4; $i++): ?>
            <div class="elab_hint_image elab_hint_image_<?php echo esc_attr($i); ?>">
                <div class="elab_hint_image__inner">
                    <?php echo elab_get_cropped_image(${"image_{$i}"}['image_id'], $sizes[$i - 1][0], $sizes[$i - 1][1]); ?>
                    <?php if (!empty(${"image_{$i}"}['hints'])): ?>
                        <?php foreach (${"image_{$i}"}['hints'] as $hint):
                            $is_right = (intval($hint['x'] < 70)) ? 'right' : 'left';
                            ?>
                            <div class="elab_hint_image__hint elab_hint_image__hint_<?php echo esc_attr($is_right); ?>"
                                 style="top: <?php echo esc_attr($hint['y']); ?>; left: <?php echo esc_attr($hint['x']); ?>;">
                                <div class="elab_hint_image__hint_plus">+</div>
                                <div class="elab_hint_image__hint_text heading_font"><?php echo sanitize_text_field($hint['hint']); ?></div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endfor; ?>

    </div>
<?php else: ?>
    <div class="hint_images_holder"></div>
<?php endif;