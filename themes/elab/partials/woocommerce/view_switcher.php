<?php
elab_enqueue_parted_style('woocommerce_view_switcher', 'woocommerce/');
$product_view = elab_get_option( 'shop_product_view', 'grid' );
if( !empty( $_GET[ 'product_view' ] ) ) {
    $product_view = sanitize_text_field( $_GET[ 'product_view' ] );
}
?>
<div class="elab_product_view_switcher">
    <a href="<?php echo add_query_arg(array('product_view' => 'grid')); ?>" class="elab_grid_1 <?php if($product_view == 'grid') echo 'active'; ?>">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
            <defs>
                <style>
                    .cls-1 {
                        fill-rule: evenodd;
                    }
                </style>
            </defs>
            <path id="Rounded_Rectangle_1_copy_5" data-name="Rounded Rectangle 1 copy 5" class="cls-1" d="M15.625,16h-3.25A1.816,1.816,0,0,1,12,14.5v-1a1.816,1.816,0,0,1,.375-1.5h3.25A1.816,1.816,0,0,1,16,13.5v1A1.816,1.816,0,0,1,15.625,16Zm0-6h-3.25A1.816,1.816,0,0,1,12,8.5v-1A1.816,1.816,0,0,1,12.375,6h3.25A1.816,1.816,0,0,1,16,7.5v1A1.816,1.816,0,0,1,15.625,10Zm0-6h-3.25A1.816,1.816,0,0,1,12,2.5v-1A1.816,1.816,0,0,1,12.375,0h3.25A1.816,1.816,0,0,1,16,1.5v1A1.816,1.816,0,0,1,15.625,4Zm-6,12H6.375A1.816,1.816,0,0,1,6,14.5v-1A1.816,1.816,0,0,1,6.375,12h3.25A1.816,1.816,0,0,1,10,13.5v1A1.816,1.816,0,0,1,9.625,16Zm0-6H6.375A1.816,1.816,0,0,1,6,8.5v-1A1.816,1.816,0,0,1,6.375,6h3.25A1.816,1.816,0,0,1,10,7.5v1A1.816,1.816,0,0,1,9.625,10Zm0-6H6.375A1.816,1.816,0,0,1,6,2.5v-1A1.816,1.816,0,0,1,6.375,0h3.25A1.816,1.816,0,0,1,10,1.5v1A1.816,1.816,0,0,1,9.625,4Zm-6,12H0.375A1.816,1.816,0,0,1,0,14.5v-1A1.816,1.816,0,0,1,.375,12h3.25A1.816,1.816,0,0,1,4,13.5v1A1.816,1.816,0,0,1,3.625,16Zm0-6H0.375A1.816,1.816,0,0,1,0,8.5v-1A1.816,1.816,0,0,1,.375,6h3.25A1.816,1.816,0,0,1,4,7.5v1A1.816,1.816,0,0,1,3.625,10Zm0-6H0.375A1.816,1.816,0,0,1,0,2.5v-1A1.816,1.816,0,0,1,.375,0h3.25A1.816,1.816,0,0,1,4,1.5v1A1.816,1.816,0,0,1,3.625,4Z"/>
        </svg>

    </a>
    <a href="<?php echo add_query_arg(array('product_view' => 'grid_2')); ?>" class="elab_grid_2 <?php if($product_view == 'grid_2') echo 'active'; ?>">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
            <defs>
                <style>
                    .cls-1 {
                        fill-rule: evenodd;
                    }
                </style>
            </defs>
            <path id="Rounded_Rectangle_1_copy_3" data-name="Rounded Rectangle 1 copy 3" class="cls-1" d="M1.5,0h1A1.5,1.5,0,0,1,4,1.5V5.625a1.5,1.5,0,0,1-1.5,1.5h-1A1.5,1.5,0,0,1,0,5.625V1.5A1.5,1.5,0,0,1,1.5,0Zm6,0h1A1.5,1.5,0,0,1,10,1.5V5.625a1.5,1.5,0,0,1-1.5,1.5h-1A1.5,1.5,0,0,1,6,5.625V1.5A1.5,1.5,0,0,1,7.5,0Zm6,0h1A1.5,1.5,0,0,1,16,1.5V5.625a1.5,1.5,0,0,1-1.5,1.5h-1a1.5,1.5,0,0,1-1.5-1.5V1.5A1.5,1.5,0,0,1,13.5,0ZM1.5,8.875h1a1.5,1.5,0,0,1,1.5,1.5V14.5A1.5,1.5,0,0,1,2.5,16h-1A1.5,1.5,0,0,1,0,14.5V10.375A1.5,1.5,0,0,1,1.5,8.875Zm6,0h1a1.5,1.5,0,0,1,1.5,1.5V14.5A1.5,1.5,0,0,1,8.5,16h-1A1.5,1.5,0,0,1,6,14.5V10.375A1.5,1.5,0,0,1,7.5,8.875Zm6,0h1a1.5,1.5,0,0,1,1.5,1.5V14.5A1.5,1.5,0,0,1,14.5,16h-1A1.5,1.5,0,0,1,12,14.5V10.375A1.5,1.5,0,0,1,13.5,8.875Z"/>
        </svg>
    </a>
    <a href="<?php echo add_query_arg(array('product_view' => 'list')); ?>" class="elab_list_1 <?php if($product_view == 'list') echo 'active'; ?>">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
            <defs>
                <style>
                    .cls-1 {
                        fill-rule: evenodd;
                    }
                </style>
            </defs>
            <path id="Rounded_Rectangle_1_copy_3" data-name="Rounded Rectangle 1 copy 3" class="cls-1" d="M5.375,16H1.5A1.5,1.5,0,0,1,0,14.5v-1A1.5,1.5,0,0,1,1.5,12h13A1.5,1.5,0,0,1,16,13.5v1A1.5,1.5,0,0,1,14.5,16H5.375ZM0,8.5v-1A1.5,1.5,0,0,1,1.5,6h13A1.5,1.5,0,0,1,16,7.5v1A1.5,1.5,0,0,1,14.5,10H1.5A1.5,1.5,0,0,1,0,8.5Zm0-6v-1A1.5,1.5,0,0,1,1.5,0h13A1.5,1.5,0,0,1,16,1.5v1A1.5,1.5,0,0,1,14.5,4H1.5A1.5,1.5,0,0,1,0,2.5Z"/>
        </svg>
    </a>
</div>
