<?php
$post_tags = get_the_tags();
?>

<?php if ($post_tags) : ?>
    <div class="post-tags-list" data-title="<?php esc_attr_e('Tags:', 'elab'); ?>">

        <?php foreach ($post_tags as $tag): ?>
            <a href="<?php echo esc_url(get_tag_link($tag)) ?>"
               title="<?php echo esc_attr($tag->name); ?>"><?php echo esc_attr($tag->name); ?></a>
            <span>,</span>
        <?php endforeach; ?>

    </div>
<?php endif; ?>
