<?php if (has_post_thumbnail()): ?>
    <div class="single-post-image">
        <?php the_post_thumbnail('elab_965x520'); ?>
    </div>
<?php endif;