<div class="single_post_layout clearfix">

    <?php get_template_part('partials/single/post/parts/title'); ?>

    <div class="single_post_layout_after_title">
        <?php get_template_part('partials/single/post/parts/categories'); ?>

        <?php get_template_part('partials/archive/post/parts/author'); ?>
    </div>


    <?php get_template_part('partials/single/post/parts/image'); ?>

    <?php get_template_part('partials/single/post/parts/content'); ?>

    <div class="post-bottom">

        <div class="left">
            <?php get_template_part('partials/single/post/parts/tags'); ?>
        </div>

        <div class="right">
            <?php get_template_part('partials/single/post/parts/share'); ?>
        </div>

    </div>
</div>

<?php get_template_part('partials/single/post/parts/nextpage'); ?>

<?php get_template_part('partials/single/post/parts/comments'); ?>

<?php get_template_part('partials/single/post/parts/pingbacks'); ?>