<?php
$args = array(
    'before' => '<ul class="pagination"><li>',
    'after' => '</li></ul>',
    'link_before' => '',
    'link_after' => '',
    'next_or_number' => 'number',
    'separator' => '</li><li>',
    'nextpagelink' => esc_html__('Next', 'elab'),
    'previouspagelink' => esc_html__('Previous', 'elab'),
    'pagelink' => '%',
    'echo' => 1
);

wp_link_pages($args);