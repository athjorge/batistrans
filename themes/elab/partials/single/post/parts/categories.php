<?php
$categories = wp_get_post_categories(get_the_ID());
if ($categories) : ?>
    <div class="post-tags-list post-categories" data-title="<?php esc_attr_e('Categories:', 'elab'); ?>">
        <?php foreach ($categories as $category_id):
            $category = get_category($category_id);
            ?>
            <a href="<?php echo esc_url(get_category_link($category)) ?>"
               title="<?php echo esc_attr($category->name); ?>">
                <?php echo esc_attr($category->name); ?>
            </a>
        <?php endforeach; ?>

    </div>
<?php endif; ?>
