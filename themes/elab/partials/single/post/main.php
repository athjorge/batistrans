<?php if (have_posts()):
    $active_sidebar = is_active_sidebar('sidebar-1');
    ?>

    <div class="row">
        <div class="container">

            <?php get_template_part('partials/titlebox/main'); ?>

            <?php while (have_posts()): the_post(); ?>

                <div class="row">

                    <?php if ($active_sidebar): ?>

                        <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                            <?php get_template_part('partials/single/post/parts/layout'); ?>
                        </div>

                        <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                            <div class="elab_post_sidebar">
                                <?php dynamic_sidebar("sidebar-1"); ?>
                            </div>
                        </div>

                    <?php else: ?>
                        <div class="col-sm-12">
                            <?php get_template_part('partials/single/post/parts/layout'); ?>
                        </div>
                    <?php endif; ?>

                </div>

            <?php endwhile; ?>

        </div>
    </div>

<?php else: ?>

    <?php get_template_part('partials/content', 'none'); ?>

<?php endif;