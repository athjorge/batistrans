<?php
$cats = wp_get_post_terms($id, 'stmt_portfolio_category');
if (!empty($cats) and !is_wp_error($cats)) {
    $cats = wp_list_pluck($cats, 'name');
}
if (!empty($cats)): ?>
    <div class="elab_cats">
        <?php echo sanitize_text_field(implode('/', $cats)); ?>
    </div>
<?php endif;