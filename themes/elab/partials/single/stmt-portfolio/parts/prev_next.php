<?php
elab_enqueue_parted_style('prev_next', 'portfolio/');

$prev_post = elab_get_adjacent_post('prev', array('stmt-portfolio'));
$next_post = elab_get_adjacent_post('next', array('stmt-portfolio'));

if (!empty($prev_post) || !empty($next_post)):?>
    <div class="elab_prev_next">

        <?php if (!empty($prev_post)): ?>
            <a href="<?php the_permalink($prev_post->ID) ?>" class="elab_prev_next__item elab_prev_next__prev">
                <div class="elab_prev_next__image">
                    <i class="lnricons-chevron-left"></i>
                    <?php echo elab_get_cropped_image(get_post_thumbnail_id($prev_post->ID), 55, 55); ?>
                </div>
            </a>
        <?php endif; ?>

        <?php if (!empty($next_post)): ?>
            <a href="<?php the_permalink($next_post->ID) ?>" class="elab_prev_next__item elab_prev_next__next">
                <div class="elab_prev_next__image">
                    <i class="lnricons-chevron-right"></i>
                    <?php echo elab_get_cropped_image(get_post_thumbnail_id($next_post->ID), 55, 55); ?>
                </div>
            </a>
        <?php endif; ?>

    </div>
<?php endif;