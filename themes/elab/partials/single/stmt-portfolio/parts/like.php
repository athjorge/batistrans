<?php
elab_enqueue_parted_style('like', 'portfolio/');
elab_enqueue_parted_script('like', array('jquery'));
$post_id = get_the_ID();
$likes = get_post_meta($post_id, 'likes_count', true);
$likes = (empty($likes)) ? 0 : $likes;
?>

<div class="stm_data_like notLiked" data-like="<?php echo intval($post_id); ?>">
    <i class="lnricons-hearts"></i>
    <span><?php echo sanitize_text_field($likes); ?></span>
</div>