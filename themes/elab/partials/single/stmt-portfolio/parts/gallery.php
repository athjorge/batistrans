<?php
$post_id = get_the_ID();
$gallery = get_post_meta($post_id, 'gallery', true);

if (!empty($gallery)) $gallery = json_decode($gallery, true);

if (!empty($gallery)): ?>
    <div class="portfolio_gallery">
        <?php foreach ($gallery as $item): ?>
            <div class="portfolio_gallery__single">
                <?php echo elab_get_cropped_image($item['id'], 845, 700, false); ?>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif;