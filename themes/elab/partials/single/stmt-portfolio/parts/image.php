<?php if (has_post_thumbnail()): ?>
    <div class="single-post-image">
        <?php echo elab_get_cropped_image(get_post_thumbnail_id(), 845, 700); ?>
    </div>
<?php endif;