<div class="stmt-portfolio">

    <?php get_template_part('partials/single/stmt-portfolio/parts/prev_next'); ?>

    <div class="row">
        <div class="col-sm-7">
            <?php get_template_part('partials/single/stmt-portfolio/parts/image'); ?>
            <?php get_template_part('partials/single/stmt-portfolio/parts/gallery'); ?>
        </div>
        <div class="col-sm-5">
            <div class="elab_portfolio__content">
                <?php get_template_part('partials/single/stmt-portfolio/parts/like'); ?>
                <?php get_template_part('partials/single/post/parts/title'); ?>
                <?php get_template_part('partials/single/post/parts/content'); ?>

                <?php get_template_part('partials/single/stmt-portfolio/parts/cats'); ?>
                <?php get_template_part('partials/single/stmt-portfolio/parts/posted_on'); ?>
                <?php get_template_part('partials/single/post/parts/share'); ?>
            </div>
        </div>

    </div>

</div>