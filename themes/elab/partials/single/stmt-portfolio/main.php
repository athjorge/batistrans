<?php
if (have_posts()):
    elab_enqueue_parted_style('style_1', 'portfolio/');
    ?>

    <div class="row">
        <div class="container">

            <?php get_template_part('partials/titlebox/main'); ?>

            <?php while (have_posts()): the_post(); ?>

                <div class="row">

                    <div class="col-sm-12">
                        <?php get_template_part('partials/single/stmt-portfolio/parts/layout'); ?>
                    </div>

                </div>

            <?php endwhile; ?>

        </div>
    </div>

<?php else: ?>

    <?php get_template_part('partials/content', 'none'); ?>

<?php endif;