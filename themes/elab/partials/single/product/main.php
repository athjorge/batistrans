<?php if (have_posts()):
    $active_sidebar = is_active_sidebar('sidebar-1');
    $single_product_style = elab_single_product_style();
    elab_enqueue_parted_style("single_product_style/{$single_product_style}", 'woocommerce/');
    ?>

    <div class="row">
        <div class="container">

            <?php get_template_part('partials/titlebox/main'); ?>

            <?php while (have_posts()): the_post(); ?>

                <?php get_template_part("partials/woocommerce/product_styles/{$single_product_style}"); ?>

            <?php endwhile; ?>

        </div>
    </div>

<?php else: ?>

    <?php get_template_part('partials/content', 'none'); ?>

<?php endif;