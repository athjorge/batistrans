<?php elab_enqueue_parted_script('blog_style_3', array('jquery', 'packery', 'imagesloaded')); ?>
<?php $blog_banner = elab_get_option( 'blog_banner', '' ); ?>
<div class="elab_blog_style_3 <?php if(!empty($blog_banner)) echo 'with_banner'; ?>">
    <div class="elab_titlebox">
        <?php get_template_part('partials/archive/post/parts/breadcrumbs'); ?>
    </div>
</div>

<div class="blog-full-width" data-fullwidth="1">

    <?php
    if( !empty( $blog_banner ) ):
        ?>
        <div class="blog-banner">
            <?php get_template_part( 'partials/banner-blog/banner' ); ?>
        </div>
    <?php endif; ?>
    <?php if( have_posts() ): ?>
<div class="elab-news_style_3">
    <div class="row">

        <?php get_template_part( 'partials/archive/post/parts/content-3' ); ?>

    </div>

    <div class="pagination_style_3">
        <?php get_template_part( 'partials/archive/post/parts/pagination' ); ?>
    </div>

    <?php else: ?>

        <?php get_template_part( 'partials/archive/post/content', 'none' ); ?>

    <?php endif; ?>
</div>
</div>
