<?php get_template_part('partials/titlebox/main'); ?>

<?php if (have_posts()): ?>

    <div class="row">
        <div class="col-md-12">
            <?php get_template_part('partials/archive/post/parts/content-2'); ?>
        </div>
    </div>


<?php else: ?>

    <?php get_template_part('partials/archive/post/content', 'none'); ?>

<?php endif;