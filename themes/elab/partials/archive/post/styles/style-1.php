<?php
$active_sidebar = is_active_sidebar('sidebar-1');
?>

<?php get_template_part('partials/titlebox/main'); ?>

<?php if (have_posts()): ?>

    <div class="row">

        <?php if ($active_sidebar): ?>

            <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                <?php get_template_part('partials/archive/post/parts/content-1'); ?>
            </div>

            <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                <div class="elab_post_sidebar">
                    <?php dynamic_sidebar("sidebar-1"); ?>
                </div>
            </div>

        <?php else: ?>
            <?php get_template_part('partials/archive/post/parts/content-1'); ?>
        <?php endif; ?>

    </div>


<?php else: ?>

    <?php get_template_part('partials/archive/post/content', 'none'); ?>

<?php endif;