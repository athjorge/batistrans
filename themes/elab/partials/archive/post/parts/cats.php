<?php
$categories = wp_get_post_categories( get_the_ID(), array(
    'fields' => 'all'
) );
if( !empty( $categories ) ): ?>
<div class="cats">
<?php
    $links = array();
    foreach( $categories as $category ) {
        $links[] = '<a href="' . esc_url(get_term_link( $category->term_id, $category->taxonomy )) . '" class="category heading_font">' . esc_html( $category->name ) . '</a>';
    }
    echo wp_kses_post(implode(', ', $links));
    ?>
</div>
<?php endif;