<?php while (have_posts()): the_post(); ?>

    <div <?php post_class('elab_list_news elab_blog_style_2'); ?>>
        <div class="post_date">
            <span class="day">
            <?php echo esc_html(get_the_date('d', get_the_ID())); ?>
            </span>
            <span class="month">
            <?php echo esc_html(get_the_date('M', get_the_ID())); ?>
            </span>
        </div>

        <div class="elab_list_news__inner">

            <?php if (has_post_thumbnail()): ?>
                <div class="elab_list_news__image">
                    <?php get_template_part('partials/archive/post/parts/featured_image_style_2'); ?>
                </div>
            <?php endif; ?>

            <div class="elab_list_news__content">
                <div class="post-author">
                    <?php printf(esc_html__('By %s', 'elab'), "<strong>" . get_the_author() . "</strong>"); ?>
                </div>
                <?php get_template_part('partials/archive/post/parts/title'); ?>
                <?php get_template_part('partials/archive/post/parts/excerpt'); ?>
                <?php get_template_part('partials/archive/post/parts/more'); ?>
            </div>

        </div>

    </div>

<?php endwhile; ?>

<?php get_template_part('partials/archive/post/parts/pagination');