<?php if( has_post_thumbnail() ): ?>

    <div class="post-thumbnail">
        <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
            <?php
            $image_id = get_post_thumbnail_id();
            echo elab_get_cropped_image($image_id, '580', '520', true);
            ?>
        </a>
    </div>

<?php endif; ?>