<div class="posted-on">

    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <?php printf(esc_html__('on %s', 'elab'), get_the_date()); ?>
    </a>

</div>