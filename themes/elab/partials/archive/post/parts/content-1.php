<?php while (have_posts()): the_post(); ?>

    <div <?php post_class('elab_list_news'); ?>>

        <div class="elab_list_news__inner">

            <?php if (has_post_thumbnail()): ?>
                <div class="elab_list_news__image">
                    <?php get_template_part('partials/archive/post/parts/featured_image'); ?>
                </div>
            <?php endif; ?>

            <div class="elab_list_news__content">
                <?php get_template_part('partials/archive/post/parts/author'); ?>
                <?php get_template_part('partials/archive/post/parts/title'); ?>
                <?php get_template_part('partials/archive/post/parts/excerpt'); ?>
                <?php get_template_part('partials/archive/post/parts/more'); ?>
            </div>

        </div>

    </div>

<?php endwhile; ?>

<?php get_template_part('partials/archive/post/parts/pagination');