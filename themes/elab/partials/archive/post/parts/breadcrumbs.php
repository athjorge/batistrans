<?php

elab_enqueue_parted_style('breadcrumbs', '');

if (function_exists('bcn_display')): ?>
    <div class="stm_breadcrumbs">
        <?php bcn_display(); ?>
    </div>
<?php else: ?>
    <div class="stm_breadcrumbs__holder"></div>
<?php endif;