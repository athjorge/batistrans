<div class="author">
    <?php printf(esc_html__('By %s', 'elab'), "<strong>" . get_the_author() . "</strong>"); ?>
    <?php get_template_part('partials/archive/post/parts/posted_on'); ?>
</div>