<?php
$blog_style = elab_get_option('blog_style', '1');
if(isset($_GET['blog_style']) && !empty($_GET['blog_style'])) $blog_style = sanitize_text_field($_GET['blog_style']);
if(empty($blog_style)) $blog_style = 1;
get_template_part('partials/archive/post/styles/style', $blog_style);