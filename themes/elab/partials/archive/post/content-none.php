<h4 class="nothing-found"><?php esc_html_e('Nothing Found, try again.', 'elab'); ?></h4>
<div class="row">
    <div class="col-sm-12 col-md-12 col-xl-6 col-lg-6">
        <?php echo get_search_form(); ?>
    </div>
</div>