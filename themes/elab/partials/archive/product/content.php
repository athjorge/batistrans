<?php get_template_part('partials/banner/banner'); ?>
<?php
$shop_page_style = elab_get_option('shop_page_style', 'default');

if (isset($_GET['shop_page_style']) && !empty($_GET['shop_page_style'])) {
    $shop_page_style = sanitize_text_field($_GET['shop_page_style']);
}
?>
    <header class="woocommerce-products-header">

        <?php
        /**
         * Hook: woocommerce_archive_description.
         *
         * @hooked woocommerce_taxonomy_archive_description - 10
         * @hooked woocommerce_product_archive_description - 10
         */
        do_action('woocommerce_archive_description');
        ?>
    </header>

<?php if (woocommerce_product_loop()) { ?>
    <?php get_template_part('partials/woocommerce/subcategory', 'carousel'); ?>
    <div class="elab_woocommerce_bar">


        <?php /**
         * Hook: woocommerce_before_shop_loop.
         *
         * @hooked woocommerce_output_all_notices - 10
         * @hooked woocommerce_result_count - 20
         * @hooked woocommerce_catalog_ordering - 30
         */
        do_action('woocommerce_before_shop_loop'); ?>


        <?php get_template_part('partials/woocommerce/compare'); ?>

        <?php
        if ($shop_page_style == 'full_width') {
            get_template_part('partials/woocommerce/price_filter');
        }
        ?>

        <?php get_template_part('partials/woocommerce/view_switcher'); ?>

        <?php get_template_part('partials/woocommerce/per_page'); ?>

        <?php
        if ($shop_page_style == 'full_width') {
            get_template_part('partials/woocommerce/elab_filter');
        }
        ?>
    </div>

    <?php woocommerce_product_loop_start();

    if (wc_get_loop_prop('total')) {
        while (have_posts()) {
            the_post();

            /**
             * Hook: woocommerce_shop_loop.
             *
             * @hooked WC_Structured_Data::generate_product_data() - 10
             */
            do_action('woocommerce_shop_loop');
            wc_get_template_part('content', 'product');

        }
    }

    woocommerce_product_loop_end();

    /**
     * Hook: woocommerce_after_shop_loop.
     *
     * @hooked woocommerce_pagination - 10
     */
    do_action('woocommerce_after_shop_loop');

} else {
    /**
     * Hook: woocommerce_no_products_found.
     *
     * @hooked wc_no_products_found - 10
     */
    do_action('woocommerce_no_products_found');
}
?>