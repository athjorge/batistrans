<?php
$show_sidebar = elab_get_option('show_sidebar', false);
$active_sidebar = is_active_sidebar('sidebar-1');
?>

<?php get_template_part('partials/titlebox/main'); ?>

<?php if ( have_posts() ): ?>

    <?php while ( have_posts() ): the_post(); ?>
        <?php
        $hide_post_sidebar = get_post_meta(get_the_ID(), 'hide_sidebar', true);
        ?>
        <?php if ($show_sidebar && $active_sidebar && !$hide_post_sidebar): ?>

            <div class="row">
                <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                    <?php get_template_part('partials/archive/page/parts/main' ); ?>
                </div>

                <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                    <div class="elab_post_sidebar">
                        <?php dynamic_sidebar("sidebar-1"); ?>
                    </div>
                </div>
            </div>

        <?php else: ?>

            <?php get_template_part('partials/archive/page/parts/main' ); ?>

        <?php endif; ?>

    <?php endwhile; ?>

<?php else: ?>

    <?php get_template_part('partials/content', 'none' ); ?>

<?php endif;