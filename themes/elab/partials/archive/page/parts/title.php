<?php
$show_page_title = elab_get_option('show_title', true);
if ($show_page_title): ?>
    <div class="page-title">
        <h1><?php the_title(); ?></h1>
    </div>
<?php endif;