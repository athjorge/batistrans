<?php
$shop_banner = elab_get_banner_id();
$image = get_post_meta($shop_banner, 'image', true);
if (!empty($image)): ?>
    <div class="elab__banner_image">
        <?php echo elab_get_cropped_image($image, 600, 400, false); ?>
    </div>
<?php endif;