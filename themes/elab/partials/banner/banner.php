<?php

$queried_obj = get_queried_object();
$term_id = (!empty($queried_obj->term_id)) ? $queried_obj->term_id : '';
$shop_banner = elab_get_banner_id($term_id);


if ($shop_banner):
    $title = get_post_meta($shop_banner, 'title', true);
    $image = get_post_meta($shop_banner, 'image', true);
    $banner_style = get_post_meta($shop_banner, 'banner_style', true);

    if (!empty($title)):
        $parts = "partials/banner/";
        get_template_part("{$parts}styles");

        $banner_classes = array(
            'elab__banner',
            'elab__banner__' . $shop_banner
        );
        if(!empty($banner_style)) $banner_classes[] = $banner_style;
        if (empty($image)) $banner_classes[] = 'elab__banner__image_not_exist';
        ?>

        <div class="<?php echo esc_attr(implode(' ', $banner_classes)); ?>">

            <div class="elab__banner_text">

                <?php get_template_part("{$parts}title"); ?>
                <?php get_template_part("{$parts}content"); ?>
                <?php get_template_part("{$parts}btns"); ?>

            </div>

        </div>

    <?php endif; ?>
<?php endif;