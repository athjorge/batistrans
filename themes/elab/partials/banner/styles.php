<?php
$shop_banner = elab_get_banner_id();
$bg_color = get_post_meta($shop_banner, 'bg_color', true);
$title_color = get_post_meta($shop_banner, 'title_color', true);
$bg = get_post_meta($shop_banner, 'bg', true);
$inline = '';
$selector = ".elab__banner__{$shop_banner}";
$banner_style = get_post_meta($shop_banner, 'banner_style', true);

if(!empty($bg_color)) {
    $inline .= "{$selector} {
        background-color : {$bg_color};
    }";
}

if(!empty($title_color)) {
    $inline .= "{$selector} .elab__banner_title {
        color : {$title_color};
    }";
}

if(!empty($bg)){
    $bg_image = wp_get_attachment_image_src($bg, 'full', true);
    $inline .= "{$selector} {
        background-image : url('{$bg_image[0]}');
        background-repeat : no-repeat;
        background-size : cover;
        background-position: center center;
    }";
}


if(empty($banner_style)){
    $banner_style = 'style_1';
}
elab_enqueue_parted_style($banner_style, 'banners/', $inline);