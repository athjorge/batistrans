<?php
$shop_banner = elab_get_banner_id();
$button_title = get_post_meta($shop_banner, 'button_title', true);
$button_link = get_post_meta($shop_banner, 'button_link', true);
if(!empty($button_title) and !empty($button_link)): ?>
    <div class="elab__banner_btn">
        <a href="<?php echo esc_attr($button_link) ?>" class="btn btn-primary">
            <?php echo sanitize_text_field($button_title); ?>
        </a>
    </div>
<?php endif;