<?php
$parts = "partials/banner/";
?>

<div class="elab__banner_buttons">
    <?php get_template_part("{$parts}btn") ?>
    <?php get_template_part("{$parts}btn_outline") ?>
</div>