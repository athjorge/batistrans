<?php
$shop_banner = elab_get_banner_id();
$title = get_post_meta($shop_banner, 'title', true);
if(!empty($title)): ?>
    <div class="elab__banner_title">
        <?php echo sanitize_text_field($title); ?>
    </div>
<?php endif;