<?php

$blog_banner = elab_get_option('blog_banner', '');

if ($blog_banner):
    $title = get_post_meta($blog_banner, 'title', true);

    if (!empty($title)):
        $parts = "partials/banner-blog/";
        get_template_part("{$parts}styles");

        $banner_classes = array(
            'elab__banner',
            'elab__banner__image_not_exist',
            'style_3',
            'elab__banner__' . $blog_banner
        );
        ?>

        <div class="<?php echo esc_attr(implode(' ', $banner_classes)); ?>">
            <div class="container">
                <div class="elab__banner_text">

                    <?php get_template_part("{$parts}title"); ?>
                    <?php get_template_part("{$parts}content"); ?>
                    <?php get_template_part("{$parts}btns"); ?>

                </div>
            </div>
        </div>

    <?php endif; ?>
<?php endif;