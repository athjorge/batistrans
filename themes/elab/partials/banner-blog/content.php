<?php
$shop_banner = elab_get_banner_id();
$content = get_post_meta($shop_banner, 'content', true);
if(!empty($content)): ?>
    <div class="elab__banner_content">
        <?php echo elab_filtered_output($content); ?>
    </div>
<?php endif;