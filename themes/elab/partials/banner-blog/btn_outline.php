<?php
$shop_banner = elab_get_banner_id();
$button_title = get_post_meta($shop_banner, 'button_title_2', true);
$button_link = get_post_meta($shop_banner, 'button_link_2', true);
if(!empty($button_title) and !empty($button_link)): ?>
    <div class="elab__banner_btn">
        <a href="<?php echo esc_url($button_link, array('mailto')) ?>" class="btn btn-outline-primary">
            <?php echo sanitize_text_field($button_title); ?>
        </a>
    </div>
<?php endif;