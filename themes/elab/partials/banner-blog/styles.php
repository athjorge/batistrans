<?php
$blog_banner = elab_get_option('blog_banner', '');
$bg_color = get_post_meta($blog_banner, 'bg_color', true);
$title_color = get_post_meta($blog_banner, 'title_color', true);
$bg = get_post_meta($blog_banner, 'bg', true);
$inline = '';
$selector = ".elab__banner__{$blog_banner}";
$banner_style = 'style_3';

if(!empty($bg_color)) {
    $inline .= "{$selector} {
        background-color : {$bg_color};
    }";
}

if(!empty($title_color)) {
    $inline .= "{$selector} .elab__banner_title {
        color : {$title_color};
    }";
}

if(!empty($bg)){
    $bg_image = wp_get_attachment_image_src($bg, 'full', true);
    $inline .= "{$selector} {
        background-image : url('{$bg_image[0]}');
        background-repeat : no-repeat;
        background-size : cover;
        background-position : top center;
    }";
}

elab_enqueue_parted_style($banner_style, 'banners/', $inline);