<?php

$header_type = (defined('STM_HB_VER')) ? 'builder' : 'theme';

get_template_part('partials/header/header', $header_type);