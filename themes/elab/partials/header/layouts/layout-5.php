<?php
elab_enqueue_parted_style('layout_5', 'header_layouts/');
$hot_title = elab_get_option('header_hot_title', '');
?>

<div class="top-bar">

    <div class="container">

        <div class="row align-items-center  justify-content-between">

            <div class="col-sm">
                <?php get_template_part('partials/header/parts/hot_title'); ?>
            </div>

            <div class="col-sm">
                <?php get_template_part('partials/header/parts/top_menu'); ?>
            </div>

        </div>

    </div>

</div>

<div class="main-header">

    <div class="container">

        <div class="row align-items-center justify-content-between">

            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-2 col-6">

                <?php get_template_part('partials/header/parts/logo'); ?>

            </div>

            <div class="col-xl-10 col-lg-10 col-md-9 col-sm-10 col-6">


                <!--Desktop-->
                <div class="main-header-right wp_is_not_mobile woo-search-wrapper">

                    <?php get_template_part('partials/header/parts/menu'); ?>

                    <?php get_template_part('partials/header/parts/woo_search'); ?>

                    <?php get_template_part('partials/header/parts/woo_buttons'); ?>

                </div>


                <!--Mobile-->
                <div class="main-header-right wp_is_mobile">

                    <?php get_template_part('partials/header/parts/woo_buttons'); ?>

                    <div class="elab__mobile">
                        <div class="elab__mobile_toggler"
                             data-toggle-selector=".elab__mobile_content, .elab__mobile_overlay, body">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>

                        <div class="elab__mobile_overlay"
                             data-toggle-selector=".elab__mobile_toggler, .elab__mobile_content, body"></div>
                        <div class="elab__mobile_content">
                            <div class="elab__mobile_content__inner">
                                <?php get_template_part('partials/header/parts/iconbox'); ?>

                                <?php get_template_part('partials/header/parts/menu'); ?>

                                <?php get_template_part('partials/header/parts/woo_search'); ?>
                            </div>
                        </div>

                    </div>

                </div>




            </div>

        </div>

    </div>

</div>

<div class="bottom-bar">

    <div class="container">

        <?php get_template_part('partials/header/parts/shop_features'); ?>

    </div>

</div>