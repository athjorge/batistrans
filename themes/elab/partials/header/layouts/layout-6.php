<?php

elab_enqueue_parted_style( 'layout_6', 'header_layouts/' );

?>

<div class="main-header">

    <div class="container-fluid">

        <div class="row justify-content-between align-items-center">

            <div class="col-4 col-xl-4 col-lg-5 col-md-5 col-sm-4 header-logo">

                <?php get_template_part( 'partials/header/parts/logo' ); ?>
                <span class="woo-category-burger wp_is_not_mobile_large">
		            <span class="burder-item1"></span>
		            <span class="burder-item2"></span>
		            <span class="burder-item3"></span>
		            <div><?php esc_html_e( 'Categories', 'elab' ); ?></div>
	            </span>
                <?php get_template_part( 'partials/header/parts/layout_parts/layout-3/woo_categories' ); ?>
            </div>

            <div class="col-sm col-xl-8 col-lg-7 col-8 menu-column">

                <div class="main-header-right">

                    <div class="mobile-menu wp_is_mobile_large">
                        <?php get_template_part( 'partials/header/parts/menu' ); ?>
                    </div>

                    <div class="wp_is_not_mobile_large">
                        <?php get_template_part( 'partials/header/parts/menu' ); ?>
                    </div>

                    <?php get_template_part( 'partials/header/parts/layout_parts/layout-6/woo_buttons' ); ?>

                </div>

            </div>

            <div class="burgers-row col-sm-12 wp_is_mobile_large">
					<span class="woo-category-burger">
			            <span class="burder-item1"></span>
			            <span class="burder-item2"></span>
			            <span class="burder-item3"></span>
			            <div><?php esc_html_e( 'Categories', 'elab' ); ?></div>
		            </span>
                <span class="burger-menu"
                      data-toggle-selector=".mobile-menu, .mobile_overlay">
						<span></span>
						<span></span>
						<span></span>
					</span>
            </div>
            <div class="mobile_overlay wp_is_mobile_large" data-toggle-selector=".mobile-menu"></div>
        </div>

    </div>

</div>
