<?php
elab_enqueue_parted_style( 'layout_2', 'header_layouts/' );

?>

<div class="main-header">

    <div class="container">

        <div class="row align-items-center justify-content-between">

            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-3 col-6">

                <?php get_template_part( 'partials/header/parts/logo' ); ?>

            </div>

            <div class="col-xl-10 col-lg-10 col-md-9 col-sm-9 col-6">

                <div class="main-header-right">

                    <div class="heading_font main-header-right-menu">

                        <?php get_template_part( 'partials/header/parts/menu' ); ?>

                        <div class="elab__mobile wp_is_mobile">
                            <div class="elab__mobile_toggler"
                                 data-toggle-selector=".elab__mobile_content, .elab__mobile_overlay, body">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>

                            <div class="elab__mobile_overlay"
                                 data-toggle-selector=".elab__mobile_toggler, .elab__mobile_content, body"></div>

                            <div class="elab__mobile_content">
                                <div class="elab__mobile_content__inner">
                                    <?php get_template_part( 'partials/header/parts/menu' ); ?>
                                    <?php get_template_part( 'partials/header/parts/brands' ); ?>
                                </div>
                            </div>
                        </div>

                    </div>

                    <?php get_template_part( 'partials/header/parts/layout_parts/layout-2/iconbox' ); ?>

                </div>

            </div>


        </div>

    </div>

</div>

<div class="bottom-bar">

    <div class="container">

        <div class="row align-items-center justify-content-between">

            <div class="col-sm">

                <div class="bottom-bar-main woo-search-wrapper">

                    <?php get_template_part( 'partials/header/parts/layout_parts/layout-2/woo_search' ); ?>

                    <?php get_template_part( 'partials/header/parts/brands' ); ?>

                    <?php get_template_part( 'partials/header/parts/layout_parts/layout-2/woo_buttons' ); ?>

                    <?php get_template_part( 'partials/header/parts/layout_parts/layout-2/cart' ); ?>

                </div>

            </div>

        </div>

    </div>

</div>