<?php
elab_enqueue_parted_style('layout_7', 'header_layouts/');
$hot_title = elab_get_option('header_hot_title', '');
$top_menu = has_nav_menu('top-bar');

$categories = elab_get_option('header_categories', '[]');
$categories = json_decode($categories, true);
$menu = has_nav_menu('menu-header');
$socials = elab_get_option('header_socials', '{}');
$socials = json_decode($socials, true);
?>

<?php if (!empty($hot_title) or !empty($top_menu)): ?>
    <div class="top-bar">

        <div class="container">

            <div class="row align-items-center  justify-content-between">

                <div class="col-sm">
                    <div class="row">
                        <?php if (!empty($socials)): ?>
                            <div class="col-auto">
                                <?php get_template_part('partials/header/parts/socials'); ?>
                            </div>
                        <?php endif; ?>
                        <div class="col">
                            <?php get_template_part('partials/header/parts/hot_title'); ?>
                        </div>
                    </div>
                </div>

                <div class="col-sm">
                    <?php get_template_part('partials/header/parts/top_menu'); ?>
                </div>

            </div>

        </div>

    </div>

<?php endif; ?>


    <div class="main-header">

        <div class="container">

            <div class="row align-items-center justify-content-between">

                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-2 col-6">

                    <?php get_template_part('partials/header/parts/logo'); ?>

                </div>

                <div class="col-sm col-6">

                    <div class="main-header-right">
                        <div class="wp_is_not_mobile_large stmt-theme-header_menu">
                            <?php get_template_part('partials/header/parts/menu'); ?>
                        </div>
                        <?php get_template_part('partials/header/parts/woo_buttons'); ?>

                    </div>

                </div>

            </div>

        </div>

    </div>

<?php if ($menu or !empty($categories)): ?>

    <div class="bottom-bar">

        <div class="container">

            <div class="row align-items-center justify-content-between">

                <?php if (!empty($categories)): ?>

                    <div class="col-sm-3 col-lg-3 col-md-4 col-sm-6 col-9 categories-column">
                        <?php get_template_part('partials/header/parts/woo_categories'); ?>
                    </div>

                <?php endif; ?>

                <div class="col-lg col-md col-sm-6 col-3 search-iconbox">


                    <div class="wp_is_not_mobile woo_search_icons woo-search-wrapper">
                        <?php get_template_part('partials/header/parts/woo_search'); ?>
                        <div class="wp_is_not_mobile_large">
                            <?php get_template_part('partials/header/parts/iconbox'); ?>
                        </div>
                    </div>

                    <div class="elab__mobile wp_is_mobile">
                        <div class="elab__mobile_toggler"
                             data-toggle-selector=".elab__mobile_content, .elab__mobile_overlay, body">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>

                        <div class="elab__mobile_overlay"
                             data-toggle-selector=".elab__mobile_toggler, .elab__mobile_content, body"></div>

                        <div class="elab__mobile_content">
                            <div class="elab__mobile_content__inner">
                                <?php get_template_part('partials/header/parts/iconbox'); ?>

                                <?php get_template_part('partials/header/parts/menu'); ?>

                                <?php get_template_part('partials/header/parts/woo_search'); ?>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

<?php endif; ?>