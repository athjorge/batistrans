<?php
elab_enqueue_parted_style('layout_4', 'header_layouts/');
$hot_title = elab_get_option('header_hot_title', '');
$top_menu = has_nav_menu('top-bar');

$categories = elab_get_option('header_categories', '[]');
$categories = json_decode($categories, true);
$menu = has_nav_menu('menu-header');

$iconbox_icon_1_title = elab_get_option('iconbox_icon_1_title', '');
$iconbox_icon_2_title = elab_get_option('iconbox_icon_2_title', '');

$iconbox = (!empty($iconbox_icon_1_title) || !empty($iconbox_icon_2_title));

?>

<?php if (!empty($hot_title) or !empty($top_menu)): ?>
    <div class="top-bar">

        <div class="container">

            <div class="row align-items-center  justify-content-between">

                <div class="col-sm">
                    <?php get_template_part('partials/header/parts/wpml'); ?>
                    <?php get_template_part('partials/header/parts/hot_title'); ?>
                </div>

                <div class="col-sm">
                    <?php get_template_part('partials/header/parts/top_menu'); ?>
                </div>

            </div>

        </div>

    </div>

<?php endif; ?>

<div class="main-header">

    <div class="container">

        <div class="row align-items-center justify-content-between">

            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-2 col-6">

                <?php get_template_part('partials/header/parts/logo'); ?>

            </div>


            <div class="col-sm col-6 wp_is_not_mobile woo-search-wrapper">

                <div class="main-header-right <?php echo esc_attr($iconbox) ? 'has-iconboxes' : 'has-no-iconboxes'; ?>">

                    <?php get_template_part('partials/header/parts/iconbox'); ?>

                    <?php get_template_part('partials/header/parts/woo_search'); ?>

                </div>

            </div>

            <div class="col-xl-2 col-lg-2 col-md-3 col-sm-2 col-6">
                <?php get_template_part('partials/header/parts/layout_parts/layout-4/woo_buttons'); ?>
            </div>

        </div>

    </div>

</div>

<?php
$bottom_bar_classes = array(
    'bottom_bar'
);

$bottom_bar_classes[] = class_exists('WooCommerce') ? 'bottom-bar-woocommerce' : 'bottom-bar-no-wocommerce';
$bottom_bar_classes[] = (empty($categories)) ? 'bottom-bar-no-cats' : '';
?>

<div class="<?php echo esc_attr(implode(' ', $bottom_bar_classes)); ?>">

    <div class="container">

        <div class="row align-items-center justify-content-between">

            <?php if (class_exists('WooCommerce') and !empty($categories)) : ?>

                <div class="col-sm-3 col-lg-3 col-md-4 col-sm-6 col-9 categories-column">

                    <?php get_template_part('partials/header/parts/layout_parts/layout-4/woo_categories'); ?>

                </div>

            <?php endif; ?>

            <div class="col-lg col-md col-sm-6 col-3">


                <div class="elab__mobile wp_is_mobile">
                    <div class="elab__mobile_toggler"
                         data-toggle-selector=".elab__mobile_content, .elab__mobile_overlay, body">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>

                    <div class="elab__mobile_overlay"
                         data-toggle-selector=".elab__mobile_toggler, .elab__mobile_content, body"></div>
                    <div class="elab__mobile_content">
                        <div class="elab__mobile_content__inner">

                            <?php if (class_exists('WooCommerce')): ?>
                                <?php get_template_part('partials/header/parts/layout_parts/layout-2/cart'); ?>
                            <?php endif; ?>

                            <?php get_template_part('partials/header/parts/iconbox'); ?>

                            <?php get_template_part('partials/header/parts/menu'); ?>

                        </div>
                    </div>

                </div>


                <div class="header_menu_cart wp_is_not_mobile">

                    <?php get_template_part('partials/header/parts/menu'); ?>

                    <?php if (class_exists('WooCommerce')): ?>
                        <?php get_template_part('partials/header/parts/layout_parts/layout-2/cart'); ?>
                    <?php endif; ?>

                </div>


            </div>

        </div>

    </div>

</div>