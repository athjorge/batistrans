<?php
elab_enqueue_parted_style('layout_8', 'header_layouts/');
$hot_title = elab_get_option('header_hot_title', '');
$top_menu = has_nav_menu('top-bar');

$categories = elab_get_option('header_categories', '[]');
$categories = json_decode($categories, true);
$menu = has_nav_menu('menu-header');
$socials = elab_get_option('header_socials', '{}');
$socials = json_decode($socials, true);
?>

<div class="main-header">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <!-- logo -->
            <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-6">
                <?php get_template_part('partials/header/parts/logo'); ?>
            </div>

            <!-- ssearch -->
            <div class="woo-search-wrapper">
                <?php get_template_part('partials/header/parts/woo_search'); ?>
            </div>
            
            <!-- app links -->
            <div class="header__app-links">
                <a href="#" class="header__app-link">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/appstore.png" alt="App Store" width="136" height="40" />
                </a>
                <a href="#" class="header__app-link">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/googleplay.png" alt="Google Play" width="136" height="40" />
                </a>
            </div>

            <!-- woo buttons -->
            <?php get_template_part('partials/header/parts/woo_buttons'); ?>
        </div>
    </div>
</div>

<div class="bottom-bar">
    <div class="container">
        <div class="row">
            <?php if (!empty($categories)): ?>
            <div class="col-md-2 col-sm-9 col-9 categories-column">
                <?php get_template_part('partials/header/parts/woo_categories'); ?>
            </div>
            <?php endif; ?>
            <div class="col-md-10 col-3 bottom-bar-right">
                <!-- iconboxes -->
                <?php get_template_part('partials/header/parts/iconbox'); ?>
                <!-- menu -->
                <?php get_template_part('partials/header/parts/menu'); ?>
                <!-- woo buttons -->
                <?php get_template_part('partials/header/parts/woo_buttons'); ?>

                <div class="elab__mobile wp_is_mobile">
                    <div class="elab__mobile_toggler"
                            data-toggle-selector=".elab__mobile_content, .elab__mobile_overlay, body">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>

                    <div class="elab__mobile_overlay"
                            data-toggle-selector=".elab__mobile_toggler, .elab__mobile_content, body"></div>

                    <div class="elab__mobile_content">
                        <div class="elab__mobile_content__inner">
                            <!-- iconboxes -->
                            <?php get_template_part('partials/header/parts/iconbox'); ?>
                            <!-- search form -->
                            <?php get_template_part('partials/header/parts/woo_search'); ?>
                            <!-- menu -->
                            <?php get_template_part('partials/header/parts/menu'); ?>
                            
                            <!-- app links -->
                            <div class="header__app-links">
                                <a href="#" class="header__app-link">
                                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/appstore.png" alt="App Store" width="136" height="40" />
                                </a>
                                <a href="#" class="header__app-link">
                                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/googleplay.png" alt="Google Play" width="136" height="40" />
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
