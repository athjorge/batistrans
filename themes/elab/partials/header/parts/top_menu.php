<?php
$menu = wp_nav_menu(array(
    'depth' => 1,
    'container' => false,
    'items_wrap' => '%3$s',
    'theme_location' => 'top-bar',
    'echo' => FALSE,
    'fallback_cb' => '__return_false'
));

if (!empty($menu)): ?>

    <ul class="top-bar-menu heading_font">
        <?php echo elab_filtered_output($menu); ?>
    </ul>

<?php endif; ?>