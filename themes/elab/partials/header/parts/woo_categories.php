<?php
$categories = elab_get_option('header_categories', '[]');
$categories = json_decode($categories, true);
$active = (is_front_page() and !wp_is_mobile()) ? 'active' : '';
$limit = intval(elab_get_option( 'header_categories_limit_of_childs', 8));

if (!empty($categories)):
    elab_enqueue_parted_style('woo_categories');
    elab_enqueue_parted_script('woo_categories');
    ?>

    <div class="header_woo_categories <?php echo esc_attr($active); ?>">

        <div class="header_woo_categories__title heading_font">
            <span><?php esc_html_e('Categories', 'elab'); ?></span>
            <div class="header_woo_categories__toggle">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>

        <div class="header_woo_categories__list">
            <?php foreach ($categories as $category):
                $icon = get_term_meta($category['value'], 'x_product_icon', true);
                if (empty($icon)) $icon = 'lnricons-chevron-right-circle';
                $category = get_term_by('id', $category['value'], 'product_cat');
                if (empty($category) or is_wp_error($category)) continue;

                $childs = get_term_children($category->term_id, 'product_cat');

                $term_url = get_term_link($category);
                ?>
                <div class="woo_category">
                    <a href="<?php echo esc_url($term_url); ?>">
                        <i class="<?php echo esc_attr($icon); ?>"></i>
                        <?php echo sanitize_text_field($category->name); ?>
                    </a>

                    <?php if (!empty($childs)) :
                        $shop_banner = get_term_meta($category->term_id, 'shop_banner', true);
                        ?>
                        <div class="header_woo_categories__childs heading_font">
                            <h4><?php echo sanitize_text_field($category->name); ?></h4>

                            <?php if(!empty($shop_banner)):

                                $title = get_post_meta($shop_banner, 'title', true);
                                $content = get_post_meta($shop_banner, 'content', true);
                                $bg_color = get_post_meta($shop_banner, 'bg_color', true);
                                $bg = get_post_meta($shop_banner, 'bg', true);

                                $style = "";
                                if(!empty($bg_color)) {
                                    $style .= "background-color: {$bg_color};";
                                }

                                if(!empty($bg)) {
                                    $bg_image = elab_get_cropped_image_url($bg, 600,250, true);
                                    if(!empty($bg_image)) {
                                        $style .= "background-image: url({$bg_image});";
                                    }
                                }

                                ?>

                                <a href="<?php echo esc_url($term_url); ?>"
                                   class="elab_menu_banner"
                                   style="<?php echo elab_filtered_output($style); ?>">
                                    <?php if(!empty($title)): ?>
                                        <div class="elab_menu_banner_title">
                                            <?php echo esc_attr($title); ?>
                                        </div>
                                    <?php endif; ?>
                                    <?php if(!empty($content)): ?>
                                        <div class="elab_menu_banner_content">
                                            <?php echo wp_kses_post($content); ?>
                                        </div>
                                    <?php endif; ?>
                                </a>
                            <?php endif; ?>

                            <div class="header_woo_categories__childs_list">

                                <?php
                                $current = 0;

                                foreach ($childs as $child_id):
                                    $current++;
                                    if($current > $limit) continue;
                                    $child = get_term_by('id', $child_id, 'product_cat');
                                    ?>
                                    <a href="<?php echo get_term_link($child); ?>">
                                        <?php echo esc_attr($child->name); ?>
                                    </a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

            <?php endforeach; ?>

        </div>

        <div class="header_woo_categories__overlay"></div>

    </div>

<?php endif;
