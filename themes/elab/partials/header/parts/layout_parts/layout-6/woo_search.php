<?php if (class_exists('WooCommerce')):
    $class = array('woo_search');
    wp_enqueue_script('jquery-ui-autocomplete');
    elab_enqueue_parted_style('style-2', 'header_parts/woo_search/');
    elab_enqueue_parted_script('woo_search', array('jquery-ui-autocomplete'));
    $s = (!empty($_GET['s'])) ? sanitize_text_field($_GET['s']) : '';
    ?>
    <div class="<?php echo esc_attr(implode(' ', $class)); ?>">
        <form action="<?php echo get_permalink(wc_get_page_id('shop')); ?>">
            <input type="search"
                   name="s"
                   value="<?php echo esc_attr($s); ?>"
                   class="woo_search__input"
                   placeholder="<?php esc_attr_e('I am looking for...', 'elab'); ?>"/>
            <button type="submit"><i class="lnricons-magnifier"></i></button>
        </form>
    </div>
<?php endif; ?>