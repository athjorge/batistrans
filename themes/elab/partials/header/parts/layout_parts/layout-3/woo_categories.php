<?php
$categories = elab_get_option('header_categories', '[]');
$categories = json_decode($categories, true);
if (!empty($categories)):
    elab_enqueue_parted_style('woo_categories/layout-3');
    elab_enqueue_parted_script('woo_categories/layout-3');
    ?>
    <div class="header_woo_categories_overlay"></div>
    <div class="header_woo_categories heading_font">
        <div class="header_woo_categories__title">
            <span><?php esc_html_e('Categories', 'elab'); ?></span>
            <div class="header_woo_categories__toggle">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>

        <div class="header_woo_categories__list">
            <?php foreach ($categories as $category):
                $icon = get_term_meta($category['value'], 'x_product_icon', true);
                if (empty($icon)) $icon = 'lnricons-chevron-right-circle';
                $category = get_term_by('id', $category['value'], 'product_cat');
                if (empty($category) or is_wp_error($category)) continue;
                $children = get_term_children($category->term_id, 'product_cat');
                ?>
                <div class="woo_categories_list_wrap">
                    <a href="<?php echo esc_url(get_term_link($category)); ?>"
                       class="woo_category">
                        <i class="<?php echo esc_attr($icon); ?>"></i>
                        <span><?php echo sanitize_text_field($category->name); ?></span>
                    </a>
                    <?php
                    if ($children): ?>
                        <div class="has_child"></div>
                        <ul>
                            <?php foreach ($children as $child): ?>
                                <?php
                                $child_category = get_term_by('id', $child, 'product_cat');
                                $icon = get_term_meta($child, 'x_product_icon', true);
                                ?>
                                <li>
                                    <a href="<?php echo esc_url(get_term_link($child_category)); ?>"
                                       class="woo_category">
                                        <i class="<?php echo esc_attr($icon); ?>"></i>
                                        <span><?php echo sanitize_text_field($child_category->name); ?></span>
                                    </a>
                                </li>

                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>

        </div>

    </div>

<?php endif;