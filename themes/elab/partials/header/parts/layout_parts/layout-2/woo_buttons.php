<?php
$header_show_woo = elab_get_option('header_show_woo', false);
if (!empty($header_show_woo) && class_exists('WooCommerce')):

    elab_enqueue_parted_style('style-2', 'header_parts/woo_buttons/'); ?>

    <div class="elab_woocommerce_actions">

        <?php get_template_part('partials/header/parts/compare'); ?>

        <?php get_template_part('partials/header/parts/wish_list'); ?>

        <?php get_template_part('partials/header/parts/login'); ?>

    </div>

<?php endif;