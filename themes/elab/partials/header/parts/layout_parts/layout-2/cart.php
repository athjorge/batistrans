<?php elab_enqueue_parted_style('style-2', 'header_parts/cart/'); ?>

<div class="stm-cart">
    <div class="cart">

        <div class="stm-cart-icon">
            <i class="lnricons-bag2"></i>

            <!--Quantitiy-->
            <?php get_template_part('partials/header/parts/cart/quantity'); ?>
        </div>

        <div class="stm-cart-price-label">
            <?php printf(esc_html__('My Bag: %s', 'elab'), '<span class="cart__price-badge mbc"></span>'); ?>
        </div>

        <!--Mini cart-->
        <?php get_template_part('partials/header/parts/cart/mini', 'cart'); ?>
    </div>

</div>