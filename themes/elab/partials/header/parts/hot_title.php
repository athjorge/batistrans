<?php
$hot_title = elab_get_option('header_hot_title', '');
if (!empty($hot_title)): ?>
    <div class="hot-title">
        <h5><?php printf(_x('%s', 'Header hot title', 'elab'), $hot_title); ?></h5>
    </div>
<?php endif;