<?php
if (defined('YITH_WOOCOMPARE_VERSION')):
    global $yith_woocompare; ?>

    <a href="<?php echo esc_url($yith_woocompare->obj->view_table_url()); ?>"
       class="yith-woocompare-open heading_font"
       data-text="<?php esc_attr_e('Compare', 'elab'); ?>">
        <i class="lnricons-shuffle"></i>
    </a>

<?php endif;