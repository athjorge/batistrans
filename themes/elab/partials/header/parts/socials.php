<?php
$socials = elab_get_option('header_socials', '{}');
$socials = json_decode($socials, true);

if (!empty($socials)):
    elab_enqueue_parted_style('header_socials');
    ?>
    <div class="header_socials">
        <?php foreach ($socials as $social => $social_url): ?>
            <a href="<?php echo esc_url($social_url) ?>">
                <i class="fab fa-<?php echo esc_attr($social); ?>"></i>
            </a>
        <?php endforeach; ?>
    </div>
<?php endif;