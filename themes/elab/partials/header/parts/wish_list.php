<?php if (defined('YITH_WCWL')):
    $url = YITH_WCWL()->get_wishlist_url();
    ?>
    <a href="<?php echo esc_url($url); ?>"
       class="elab_wishlist_button heading_font"
       data-text="<?php esc_attr_e('Wishlist', 'elab'); ?>">
        <i class="lnricons-heart"></i>
    </a>
<?php endif;