<?php if (function_exists('WC')) : ?>
    <a href="<?php echo esc_url(wc_get_cart_url()); ?>" class="cart__notification heading_font">
        <i class="lnricons-cart-add"></i>
        <?php esc_html_e("Item added To cart", 'elab'); ?>
    </a>
    <script>
        var event = new CustomEvent("elab_product_added");
        document.dispatchEvent(event);
    </script>
<?php endif; ?>