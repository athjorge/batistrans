<?php if (function_exists('WC')) : ?>
    <span class="cart__quantity-badge mbc"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
<?php endif; ?>