<?php elab_enqueue_parted_style('shop_features'); ?>

<div class="elab_shop_features heading_font">
    <?php for ($i = 1; $i < 6; $i++):
        $title = elab_get_option("shop_feature_title_{$i}");
        if (empty($title)) continue;
        $icon = elab_get_option("shop_feature_icon_{$i}");
        ?>
        <div class="elab_shop_feature elab_shop_feature<?php echo esc_attr($i); ?>">
            <?php if (!empty($icon)): ?>
                <i class="<?php echo esc_attr($icon); ?>"></i>
            <?php endif; ?>
            <span><?php printf(esc_attr_x('%s', 'Header Shop Feature', 'elab'), $title); ?></span>
        </div>
        <div class="sep"></div>
    <?php endfor; ?>
</div>