<?php

$brands = get_terms(array(
    'taxonomy' => 'stmt_brand_taxonomy',
    'hide_empty' => false,
    'number' => 10,
    'orderby' => 'name',
    'order' => 'ASC',
));

if (!is_wp_error($brands) and !empty($brands)):
    elab_enqueue_parted_style('brands', 'header_parts/');
    ?>

    <div class="elab_brands">
        <span class="elab_brands__label">
            <?php esc_html_e('Our Brands', 'elab'); ?>
            <i class="lnricons-chevron-down"></i>
        </span>
        <span class="elab_brands__sort">
            <?php esc_html_e('A-Z', 'elab'); ?>
        </span>
        <div class="elab_brands__list">
            <?php foreach ($brands as $brand): ?>
                <a class="elab_brands__single" href="<?php echo esc_url(get_term_link($brand)); ?>">
                    <?php echo sanitize_text_field($brand->name); ?>
                </a>
            <?php endforeach; ?>
        </div>
    </div>

<?php endif;