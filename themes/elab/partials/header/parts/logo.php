<?php
$logo = elab_get_option('logo', get_template_directory_uri() . '/assets/images/logo.svg');
$options = elab_stored_theme_options();
if (is_int($logo)) $logo = elab_get_image_url($logo);
if (empty($logo)) $logo = get_template_directory_uri() . '/assets/images/logo.svg';
?>

<a href="<?php echo esc_url(site_url('/')); ?>">
    <img src="<?php echo esc_url($logo); ?>" alt="<?php esc_attr_e('Logo', 'elab'); ?>"/>
</a>