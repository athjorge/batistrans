<?php
$menu = wp_nav_menu(array(
    'depth' => 3,
    'container' => false,
    'items_wrap' => '%3$s',
    'theme_location' => 'menu-header',
    'echo' => FALSE,
    'stm_megamenu' => 1,
    'fallback_cb' => '__return_false',
));

elab_enqueue_parted_style('menu');

if (!empty($menu)): ?>

    <ul class="stmt-theme-header_menu">
        <?php echo elab_filtered_output($menu); ?>
    </ul>

<?php endif; ?>