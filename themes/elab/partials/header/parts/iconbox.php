<?php
/*Iconbox 1*/
$iconbox_icon_1 = elab_get_option('iconbox_icon_1', '');
$iconbox_icon_1_title = elab_get_option('iconbox_icon_1_title', '');
$iconbox_icon_1_sub_title = elab_get_option('iconbox_icon_1_sub_title', '');
$iconbox_icon_2 = elab_get_option('iconbox_icon_2', '');
$iconbox_icon_2_title = elab_get_option('iconbox_icon_2_title', '');
$iconbox_icon_2_sub_title = elab_get_option('iconbox_icon_2_sub_title', '');

/*Check if subtitle is email*/
$iconbox_1_is_email = is_email($iconbox_icon_1_sub_title);
$iconbox_2_is_email = is_email($iconbox_icon_2_sub_title);

elab_enqueue_parted_style('iconbox');

if (!empty($iconbox_icon_1_title) || !empty($iconbox_icon_2_title)): ?>

    <div class="stm_header_iconboxes">

        <?php if (!empty($iconbox_icon_1_title)): ?>
            <div class="stm_header_iconbox stm_header_iconbox__1">
                <?php if (!empty($iconbox_icon_1)): ?>
                    <div class="stm_header_iconbox__icon">
                        <i class="<?php echo esc_attr($iconbox_icon_1); ?>"></i>
                    </div>
                <?php endif; ?>
                <div class="stm_header_iconbox__content">

                    <?php if (!empty($iconbox_icon_1_title)): ?>
                        <div class="stm_header_iconbox__title heading_font">
                            <?php printf(_x('%s', 'Header Iconbox 1 Title', 'elab'), $iconbox_icon_1_title); ?>
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($iconbox_icon_1_sub_title)): ?>
                        <div class="stm_header_iconbox__subtitle">
                            <?php if ($iconbox_1_is_email): ?>
                                <a href="mailto:<?php echo esc_attr($iconbox_icon_1_sub_title); ?>">
                                    <?php printf(_x('%s', 'Header Iconbox 1 subtitle', 'elab'), $iconbox_icon_1_sub_title); ?>
                                </a>
                            <?php else: ?>
                                <?php printf(_x('%s', 'Header Iconbox 1 subtitle', 'elab'), $iconbox_icon_1_sub_title); ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        <?php endif; ?>

        <?php if (!empty($iconbox_icon_2_title)): ?>
            <div class="stm_header_iconbox stm_header_iconbox__2">
                <?php if (!empty($iconbox_icon_2)): ?>
                    <div class="stm_header_iconbox__icon">
                        <i class="<?php echo esc_attr($iconbox_icon_2); ?>"></i>
                    </div>
                <?php endif; ?>
                <div class="stm_header_iconbox__content">

                    <?php if (!empty($iconbox_icon_2_title)): ?>
                        <div class="stm_header_iconbox__title heading_font">
                            <?php printf(_x('%s', 'Header Iconbox 2 Title', 'elab'), $iconbox_icon_2_title); ?>
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($iconbox_icon_2_sub_title)): ?>
                        <div class="stm_header_iconbox__subtitle">
                            <?php if ($iconbox_2_is_email): ?>
                                <a href="mailto:<?php echo esc_attr($iconbox_icon_2_sub_title); ?>">
                                    <?php printf(_x('%s', 'Header Iconbox 2 subtitle', 'elab'), $iconbox_icon_2_sub_title); ?>
                                </a>
                            <?php else: ?>
                                <?php printf(_x('%s', 'Header Iconbox 2 subtitle', 'elab'), $iconbox_icon_2_sub_title); ?>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        <?php endif; ?>

    </div>

<?php endif;