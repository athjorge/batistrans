<?php elab_enqueue_parted_style('cart'); ?>

<div class="stm-cart">
    <div class="cart">

        <a href="<?php echo esc_url(wc_get_cart_url()); ?>">
            <i class="lnricons-bag2"></i>
        </a>

        <!--Quantitiy-->
        <?php get_template_part('partials/header/parts/cart/quantity'); ?>

        <!--Mini cart-->
        <?php get_template_part('partials/header/parts/cart/mini', 'cart'); ?>
    </div>
</div>