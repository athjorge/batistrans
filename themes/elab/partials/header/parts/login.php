<?php
$account_url = get_permalink(get_option('woocommerce_myaccount_page_id'));
$data_text = (is_user_logged_in()) ? esc_attr__('Account', 'elab') : esc_attr__('Login', 'elab');
?>

<a href="<?php echo esc_url($account_url); ?>"
   class="heading_font"
   title="<?php esc_attr_e('My account', 'elab'); ?>"
   data-text="<?php echo esc_attr($data_text) ?>">
    <i class="lnricons-user"></i>
</a>