<?php if (class_exists('WooCommerce')):
    $product_cats = get_terms(array('taxonomy' => 'product_cat', 'parent' => 0));
    $with_categories = !empty($product_cats) or !is_wp_error($product_cats);
    $class = array('woo_search');
    $class[] = ($with_categories) ? 'woo_search_with_cats' : 'woo_search_with_no_cats';
    wp_enqueue_script('jquery-ui-autocomplete');
    elab_enqueue_parted_style('woo_search');
    elab_enqueue_parted_script('woo_search', array('jquery-ui-autocomplete'));
    $s = (!empty($_GET['s'])) ? sanitize_text_field($_GET['s']) : '';
    ?>
    <div class="<?php echo esc_attr(implode(' ', $class)); ?>">
        <form action="<?php echo get_permalink(wc_get_page_id('shop')); ?>">
            <input type="search"
                   name="s"
                   value="<?php echo esc_attr($s); ?>"
                   class="woo_search__input"
                   placeholder="<?php esc_attr_e('What are you looking for?', 'elab'); ?>"/>
            <?php if ($with_categories): ?>
                <div class="woo_search__categories">
                    <div class="woo_search__category active_category" data-category="">
                        <span><?php esc_attr_e('All categories', 'elab'); ?></span>
                    </div>
                    <?php foreach ($product_cats as $product_cat): ?>
                        <div class="woo_search__category"
                             data-category="<?php echo esc_attr($product_cat->term_id); ?>">
                            <span><?php echo sanitize_text_field($product_cat->name); ?></span>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
            <button type="submit"><i class="lnricons-magnifier"></i></button>
        </form>
    </div>
<?php endif; ?>