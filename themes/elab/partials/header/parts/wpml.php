<?php
if (class_exists('SitePress')) {
    $langs = apply_filters('wpml_active_languages', NULL, 'skip_missing=0&orderby=id&order=asc');

    elab_enqueue_parted_style('wpml', 'header_parts/');
    if (!empty($langs)): ?>
        <div class="elab_wpml_flags">
            <?php foreach ($langs as $lang_name => $lang): ?>
                <a href="<?php echo esc_url($lang['url']); ?>"
                   class="elab_wpml_flags__flag <?php echo esc_attr($lang['active']) ? 'active' : ''; ?>">
                    <img src="<?php echo esc_url($lang['country_flag_url']); ?>"/>
                </a>
            <?php endforeach; ?>
        </div>
    <?php endif;
}