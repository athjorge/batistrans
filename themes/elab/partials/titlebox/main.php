<?php
if (!is_front_page() or elab_get_option('show_title', false)) :

    elab_enqueue_parted_style('style-1', 'titlebox/');

    $show_title = true;

    $queried_object = get_queried_object();

    $page_id = (!empty($queried_object->ID)) ? $queried_object->ID : 0;

    $title = (!empty($queried_object->name)) ? $queried_object->name : '';
    $title = (!empty($queried_object->label)) ? $queried_object->label : $title;
    $title = (!empty($queried_object->post_title)) ? $queried_object->post_title : $title;
    if (empty($title) and is_front_page() and get_post_type() === 'post') {
        $page_for_posts = get_option('page_for_posts');
        $title = (!empty($page_for_posts)) ? get_the_title($page_for_posts) : esc_html__('News', 'elab');
    };

    if (elab_is_shop() or elab_is_product_category()) {
        $page_id = get_option('woocommerce_shop_page_id');
    }
    $show_title = elab_get_option('show_title', false);
    $show_bc = (get_post_meta($page_id, 'hide_page_bc', true) != 'on');
    $hide_title = get_post_meta($page_id, 'hide_page_title', true);

    if (($show_title && !$hide_title) or $show_bc) : ?>

        <div class="elab_titlebox">
            <?php if ($show_bc) get_template_part('partials/archive/post/parts/breadcrumbs'); ?>
            <?php if (!is_single()): ?>
                <?php if ($show_title && !$hide_title): ?><h3><?php echo sanitize_text_field($title); ?></h3> <?php endif; ?>
            <?php endif; ?>
        </div>

    <?php else : ?>
        <div class="elab_titlebox_holder"></div>
    <?php endif; ?>

<?php endif;