<?php

$promo_popup = elab_get_option('promo_popup', false);

if ($promo_popup):
    elab_enqueue_parted_style('style-1', 'promo-popup/');
    elab_enqueue_parted_script('promo-popup');
    $promo_popup_title = elab_get_option('promo_popup_title');
    $promo_popup_image = elab_get_option('promo_popup_image');

    $popup_id = md5("{$promo_popup}-{$promo_popup_title}-{$promo_popup_image}"); ?>

    <div class="elab_promo_popup__overlay" data-popup-id="<?php echo esc_attr($popup_id); ?>"
         style="display: none"></div>
    <div class="elab_promo_popup <?php echo esc_attr($popup_id) ?>" data-popup-id="<?php echo esc_attr($popup_id); ?>"
         style="display: none">
        <i class="elab_promo_popup__close lnricons-cross" data-popup-id="<?php echo esc_attr($popup_id); ?>"></i>

        <?php if (!empty($promo_popup_image)): ?>
            <div class="elab_promo_popup__image">
                <?php echo elab_get_cropped_image($promo_popup_image, 280, 360); ?>
            </div>
        <?php endif; ?>

        <div class="elab_promo_popup__content">
            <?php if (!empty($promo_popup_title)): ?>
                <h3><?php echo sanitize_text_field($promo_popup_title); ?></h3>
            <?php endif; ?>

            <div class="elab_promo_popup__mc">
                <?php get_template_part('partials/footer/parts/mc_form'); ?>
            </div>
        </div>

    </div>

<?php endif;