<?php
/**
 *
 * @var $params
 * @var $name
 * @var $module
 * @var $number
 * @var $per_row
 * @var $custom_css
 */

$params = stm_x_builder_get_params($module, $params);
extract($params);
$classes = array();
$module_id = stm_x_builder_module_id($module, $params);
$classes[] = $module_id;
$classes[] = "{$module}_{$style}";
$classes[] = 'owl-carousel';
$inline_styles = (empty($custom_css)) ? '' : $custom_css;


wp_enqueue_style('owl-carousel');
elab_enqueue_parted_style($module, 'x_builder/', $inline_styles);
elab_enqueue_parted_script($module, array('owl-carousel', 'imagesloaded'), '', $module_id, array(
    'per_row' => $per_row,
    'style' => $style
));
$number = (!empty($number)) ? $number : 4;

$args = array(
    'post_type' => 'stmt-testimonials',
    'posts_per_page' => $number
);

$q = new WP_Query($args);

if ($q->have_posts()): ?>

    <div class="x_testimonials <?php echo esc_attr(implode(' ', $classes)); ?>"
         data-module="<?php echo esc_attr($module_id); ?>">
        <?php while ($q->have_posts()): $q->the_post();
            $id = get_the_ID();
            $author = get_post_meta($id, 'author', true);
            ?>

            <div class="x_testimonial">

                <div class="x_testimonial__title">
                    <h5><?php the_title(); ?></h5>
                </div>

                <div class="x_testimonial__content">
                    <?php the_excerpt(); ?>
                </div>

                <?php if (!empty($author)): ?>
                    <div class="x_testimonial__author">
                        <?php if ($style == 'style_2'): ?>
                            <div class="x_testimonial__author-image">
                                <?php the_post_thumbnail() ?>
                            </div>
                            <div class="x_testimonial__author-info">
                                <div class="x_testimonial__author_name"><?php echo esc_html($author) ?></div>
                                <div class="x_testimonial__stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        <?php else: ?>
                            <?php printf(esc_html__('By %s on %s', 'elab'), "<strong>{$author}</strong><span>", get_the_date() . "</span>"); ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

            </div>

        <?php endwhile; wp_reset_postdata(); ?>
    </div>

<?php endif;