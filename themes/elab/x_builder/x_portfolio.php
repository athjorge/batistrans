<?php
/**
 *
 * @var $params
 * @var $name
 * @var $module
 * @var $number
 * @var $categories
 * @var $custom_css
 */

$params = stm_x_builder_get_params($module, $params);
extract($params);
$classes = array();
$module_id = stm_x_builder_module_id($module, $params);
$classes[] = $module_id;
$inline_styles = (empty($custom_css)) ? '' : $custom_css;

if(empty($categories)) $categories = array();

array_unshift($categories, array(
    'name' => esc_html__('All', 'elab'),
    'term_id' => 'all'
));

elab_enqueue_parted_style($module, 'x_builder/', $inline_styles);
elab_enqueue_parted_script(
    $module,
    array('vue.js', 'vue-resource.js', 'imagesloaded', 'packery'),
    '',
    $module_id,
    array(
        'categories' => $categories,
    )
);
?>

<div class="x_portfolio <?php echo esc_attr(implode(' ', $classes)); ?>"
     data-module="<?php echo esc_attr($module_id); ?>">

    <div class="x_portfolio__tabs" v-bind:class="'x_portfolio__tabs_' + categories.length">
        <div class="x_portfolio__tab" v-for="category in categories"
             v-bind:class="{'active' : active_category == category.term_id}">
            <a href="#" v-html="category.name" @click.prevent="getPortfolio(category)"></a>
        </div>
    </div>

    <div class="x_loader_wrapper"
         v-if="loading">
        <div class="x_loader">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>

    <div class="x_portfolio__grid"
         v-bind:class="{'is-loading' : loading}"
         v-if="products[active_category]">
        <a v-bind:href="product.permalink"
           class="x_portfolio__grid__single"
           v-bind:style="{'width' : product.width + '%', 'display' : 'block'}"
           v-bind:class="{'loading': loading, 'loaded' : product.loaded}"
           v-for="(product, index) in products[active_category]['products']">
            <div class="x_portfolio__grid__single_image">
                <img v-bind:src="product.image" v-bind:src="product.title"/>
            </div>
            <div class="x_portfolio__grid__single_content heading_font">
                <div class="x_portfolio__grid__single_cats" v-html="product.cats"></div>
                <div class="x_portfolio__grid__single_title">
                    <h4 v-html="product.title"></h4>
                </div>
            </div>
        </a>
    </div>

    <div class="text-center" v-if="products[active_category]">
        <div class="load_more btn btn-primary" @click="loadMore()" v-bind:class="{'loading' : loading}"
             v-if="hasMorePosts()">
            <span><?php esc_html_e('Load more', 'elab'); ?></span>
        </div>
    </div>

</div>