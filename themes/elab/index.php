<?php get_header(); ?>

    <div id="primary" class="container">
        <main id="main" class="site-main">

			<?php
			$post_type = get_post_type();
			get_template_part("partials/archive/{$post_type}/main");
			?>

        </main>
    </div>

<?php
get_footer();