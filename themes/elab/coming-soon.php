<?php
/**
 * Template Name: Coming Soon Page
 *
 */

get_header();
elab_enqueue_parted_style('coming-soon', '');
?>

    <div id="primary" class="container">
        <main id="main" class="site-main">

            <?php
            $post_type = get_post_type();
            get_template_part("partials/archive/{$post_type}/main"); ?>

            <div class="coming-soon-mailchimp">
                <div class="coming-soon-mailchimp__title text-center">
                    <?php printf(esc_html__('Notify me when %sit\'s ready%s', 'elab'), '<strong>', '</strong>'); ?>
                </div>
                <?php get_template_part("partials/footer/parts/mailchimp"); ?>
            </div>

        </main>
    </div>

<?php
get_footer();