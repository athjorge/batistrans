# Elab

> Elite E-commerce Theme from Stylemixthemes

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
gulp

# build all styles 
gulp all
