<?php
$stmt_inc_path = get_template_directory() . '/inc';

define('ELAB_PATH', get_template_directory());

require_once $stmt_inc_path . "/template/functions.php";
require_once $stmt_inc_path . "/template/layout_config.php";
require_once $stmt_inc_path . "/template/setup.php";
require_once $stmt_inc_path . "/template/enqueue.php";
require_once $stmt_inc_path . "/template/comments.php";

if (class_exists('WooCommerce')) {
    require_once $stmt_inc_path . "/template/woocommerce/woocommerce.php";
}

if (is_admin()) {
    require_once $stmt_inc_path . "/admin/css/css-php.php";
    require_once $stmt_inc_path . "/admin/tgm/registration.php";
    require_once $stmt_inc_path . "/admin/product_registration/admin.php";
    require_once $stmt_inc_path . "/admin/init/init.php";
}


function stm_get_plugin_path($plugin_slug, $wp_repository = false)
{

    $is_dev_mode = defined('STM_DEV_MODE') && STM_DEV_MODE === true;

    /*DEV mode is off and we have WP Repository*/
    if (!$is_dev_mode && $wp_repository) return null;

    /*DEV mode is off and is not a WP Repository*/
    if (!$is_dev_mode && !$wp_repository) return elab_get_package($plugin_slug, 'zip');

    /*Only dev mode now*/
    $plugins_path = get_template_directory() . '/inc/tgm/plugins';
    $plugins_path = "{$plugins_path}/{$plugin_slug}.zip";

    /*DEV mode is on but no plugin uploaded locally */
    if (defined('STM_DEV_MODE') && !file_exists($plugins_path)) {
        return !$wp_repository ? elab_get_package($plugin_slug, 'zip') : null;
    }
    
    /*So we have this plugin locally*/
    return $plugins_path;

}