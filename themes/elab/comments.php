<?php

if (post_password_required()) {
    return;
}
?>

<div id="comments" class="comments-area">

    <?php
    if (have_comments()) : ?>
        <h2 class="comments-title"><?php esc_html_e('Comments', 'elab'); ?></h2>

        <?php the_comments_navigation(); ?>

        <ul class="comment-list">
            <?php wp_list_comments(array(
                'style' => 'ul',
                'short_ping' => true,
                'callback' => 'elab_comment'
            )); ?>
        </ul>

        <?php the_comments_navigation();

        if (!comments_open()) :?>
            <p class="no-comments"><?php esc_html_e('Comments are closed.', 'elab'); ?></p>
        <?php endif;

    endif;

    comment_form(); ?>

</div>
