<?php get_header();
?>

    <div id="primary" class="container">
        <main id="main" class="site-main">

            <?php get_template_part("partials/archive/post/main"); ?>

        </main>
    </div>

<?php
get_footer();