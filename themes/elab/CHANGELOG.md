## 1.1.0
- **NEW** Medical demo.
- STM Configurations plugin updated (v 1.1.0).
- X-Builder plugin updated (v 1.1.0).
- **FIX:** Minor bug fixes.
- **FIX:** Fixed product variations price bug.
- **FIX:** Fixed filter button on brand page.

## 1.0.6

- **NEW:** Grocery Store demo.
- STM Configurations plugin updated (v 1.0.6).
- Revolution Slider plugin updated (v 6.2.6).
- **FIX:** Minor bug fixes.

## 1.0.5

- STM Configurations plugin updated (v 1.0.5).
- Revolution Slider plugin updated (v 6.2.2).
- WooCommerce templates updated (v 4.0).
- Dokan templates updated (v 3.0).
- **FEATURE:** Carousel with Grid Products element - Post Count option added.
- **FIX:** Dashboard - Edit Product > Default Global Style bug fixed.
- **FIX:** Checkout - displaying Shipping Price bug fixed.
- **FIX:** Day/Hour/Minutes/Seconds static strings made translatable.
- **FIX:** Login/Register page bugs fixed.
- **FIX:** Minor bug fixes.

## 1.0.4

- **NEW:** Dokan PRO compatible.
- **NEW:** WC Vendors PRO compatible.
- STM Configurations plugin updated (v 1.0.4).
- Revolution Slider plugin updated (v 6.1.5).
- **FIX:** Minor bug fixes.

## 1.0.3

- Compatible with WordPress 5.3.
- STM Configurations plugin updated (v 1.0.3).
- Revolution Slider plugin updated (v 6.1.4).
- WooCommerce templates updated (v 3.8).

## 1.0.2

- **NEW:** Product style 4 added.
- **FIX:** Minor bug fixes.
- STM Configurations plugin updated (v 1.0.2).

## 1.0.1

- **NEW:** Trendy demo added.
- **NEW:** Classic demo added.
- **NEW:** Dokan multi-vendor plugin integrated.
- **NEW:** WC Vendors multi-vendor plugin integrated.
- **FIX:** Minor bug fixes.
- STM Configurations plugin updated (v 1.0.1).

## 1.0
- Theme release.