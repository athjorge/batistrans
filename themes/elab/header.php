<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>
    <meta charset="<?php bloginfo('charset'); ?>">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <meta name="format-detection" content="telephone=no"/>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<?php do_action('elab_after_body_started'); ?>
<div id="page" class="site">
    <header id="masthead" class="site-header">
        <?php get_template_part('partials/header/main'); ?>
    </header>

    <div id="content" class="site-content">