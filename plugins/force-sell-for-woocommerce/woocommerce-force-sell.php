<?php
/**
 * Plugin Name: Force Sell for WooCommerce
 * Plugin URI: https://wordpress.org/plugins/force-sell-for-woocommerce/?utm_source=free_plugin&utm_medium=plugins&utm_campaign=force_sell
 * Description: WooCommerce Force Sell plugin allows you to link products to another product, so they are added to the cart together.
 * Version: 3.5.1.3
 * Author: BeRocket
 * Requires at least: 5.0
 * Author URI: https://berocket.com?utm_source=free_plugin&utm_medium=plugins&utm_campaign=force_sell
 * Text Domain: force-sell-for-woocommerce
 * Domain Path: /languages/
 * WC tested up to: 4.1
 */
define( "BeRocket_force_sell_version", '3.5.1.3' );
define( "BeRocket_force_sell_file", __FILE__ );
include_once('main.php');
