=== Force Sell for WooCommerce ===
Plugin Name: Force Sell for WooCommerce
Contributors: berocket, dholovnia
Donate link: https://berocket.com?utm_source=wordpress_org&utm_medium=donate&utm_campaign=force_sell
Tags: force sell, tied up products, sell products, woocommerce force sell, woocommerce sell together, sell products together, force sell together, link products, sell linked products, force linked products, force add product, automatically add product, automatic product, related products, related forced add to cart, product forced add to cart, berocket, berocket force sell for woocommerce
Requires at least: 5.0
Tested up to: 5.4.1
Stable tag: 3.5.1.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Force Sell for WooCommerce plugin allows you to link products to another product, so they are added to the cart together.

== Description ==

Force Sell for WooCommerce plugin allows you to link products to another product, so they are added to the cart together. This is useful for linking a service or required product.

= Features: =
&#9989; Link products to another product
&#9989; Display linked products on product page


= Features in version 3.5: =
&#9989; Link products to all products
&#9989; Link products to product categories
&#9989; Link products to list of products
&#9989; Linked products as link to product page
&#9989; Widget to display linked products
&#9989; Shortcode to display linked products


= Plugin Links: =
[Demo](https://woocommerce-products-compare.berocket.com/shop/?utm_source=wordpress_org&utm_medium=plugin_links&utm_campaign=force_sell)
[Demo Description](https://woocommerce-products-compare.berocket.com/force-sell-preview/?utm_source=wordpress_org&utm_medium=plugin_links&utm_campaign=force_sell)


= &#127852; Wanna try admin side? =
[Admin Demo](https://berocket.com/product/woocommerce-force-sell?utm_source=wordpress_org&utm_medium=admin_demo&utm_campaign=force_sell#try-admin) - Get access to this plugin's admin and try it from inside. Change things and watch how they work.

= Plugin video =
[youtube https://youtu.be/n4v1KJV3IN4]
*we don't have video with free plugin right now but we are working on it*

= Compatibility with WooCommerce plugins =
Force Sell for WooCommerce has been tested and compatibility is certain with the following WooCommerce plugins that you can add to your site:

&#128312; [**Brands for WooCommerce**](https://wordpress.org/plugins/brands-for-woocommerce/)
&#128312; [**Grid/List View for WooCommerce**](https://wordpress.org/plugins/gridlist-view-for-woocommerce/)
&#128312; [**Min and Max Quantity for WooCommerce**](https://wordpress.org/plugins/minmax-quantity-for-woocommerce/)
&#128312; [**Product Tabs Manager for WooCommerce**](https://wordpress.org/plugins/product-tabs-manager-for-woocommerce/)
&#128312; [**Products Suggestions for WooCommerce**](https://wordpress.org/plugins/cart-products-suggestions-for-woocommerce/)
&#128312; [**Terms and Conditions Popup for WooCommerce**](https://wordpress.org/plugins/terms-and-conditions-popup-for-woocommerce/)

== Installation ==
Important: First of all, you have to download and activate WooCommerce plugin, because without it Force Sell for WooCommerce cannot work.

1. Unzip the downloaded .zip file.
2. Upload the Force Sell for WooCommerce folder into the `wp-content/plugins/` directory of your WordPress site.
3. Activate `Force Sell for WooCommerce` from Plugins page

= Configuration =
Force Sell for WooCommerce will add a new sub-menu item called "Force Sell" in "WooCommerce" menu. There you will be able to access plugin settings page.


== Frequently Asked Questions ==

= Is it compatible with all WordPress themes? =
Compatibility with all themes is impossible, because they are too many, but generally if themes are developed according to WordPress and WooCommerce guidelines, BeRocket plugins are compatible with them.

= How can I get support if my WooCommerce plugin is not working? =
If you have problems with our plugins or something is not working as it should, first follow this preliminary steps:

* Test the plugin with a WordPress default theme, to be sure that the error is not caused by the theme you are currently using.
* Deactivate all plugins you are using and check if the problem is still occurring.
* Ensure that your plugin version, your theme version and your WordPress and WooCommerce version (if required) are updated and that the problem you are experiencing has not already been solved in a later plugin update.

If none of the previous listed actions helps you solve the problem, then, submit a ticket in the forum and describe your problem accurately, specify WordPress and WooCommerce versions you are using and any other information that might help us solve your problem as quickly as possible. Thanks!

---

== Screenshots ==
1. General settings
2. Option on product page

---

== Changelog ==

= 3.5.1.3 =
* Enhancement - Compatibility version: Wordpress 5.4.1 and WooCommerce 4.1

= 3.5.1.2 =
* Enhancement - Compatibility version: Wordpress 5.4 and WooCommerce 4.0

= 3.5.1.1 =
* Enhancement - Compatibility version: Wordpress 5.3 and WooCommerce 3.8
* Fix - Error for removed products

= 3.5.1 =
* Enhancement - Possibility to enable/disable some Force Sells

= 3.5.0.3 =
* Fix - Variable products
* Fix - Widget notices
* Fix - Links to BeRocket
* Fix - Compatibility with other BeRocket plugins

= 3.5.0.2 =
* Fix - Some notices displayed incorrect
* Fix - Compatibility with other plugins

= 3.5.0.1 =
* Enhancement - Changed text domain
* Enhancement - Added language files

= 3.5 =
* Enhancement - Link products to all products
* Enhancement - Link products to product categories
* Enhancement - Link products to list of products
* Enhancement - Linked products as link to product page
* Enhancement - Widget to display linked products
* Enhancement - Shortcode to display linked products
* Enhancement - Products, that will be added once to each products in list

= 1.1.4.2 =
* Enhancement - Compatibility with other BeRocket plugins
* Enhancement - Code Security
* Fix - JavaScript errors

= 1.1.4.1 =
* Fix - PHP notice on product page

= 1.1.4 =
* Enhancement - Update BeRocket plugin framework 2.1
* Fix - Fatal error without WooCommerce

= 1.1.3.2 =
* Fix - Linked products do not removing from cart

= 1.1.3.1 =
* Enhancement - New way of adding force sell products
* Enhancement - Condition mode
* Fix - Conditions work incorrect with variable products
* Fix - Font Awesome 5 on some themes

= 1.1.3 =
* Fix - Subscribe
* Fix - Feature request send

= 1.1.2 =
* Enhancement - Feature request box
* Enhancement - Feedback box
* Fix - Force sell for variable products

= 1.1.1 =
* Fix - Support link
* Fix - Restore some information, that was removed in previous version

= 1.1.0 =
* Upgrade - New better UI 
* Fix - Updater Compatibility

= 1.0.9 =
* Upgrade - WordPress 4.9 compatibility

= 1.0.8 =
* Upgrade - more useful subscribe
* Fix - updater fix

= 1.0.7 =
* Upgrade - recent woocommerce version support
* Upgrade - new Admin notices

= 1.0.6 =
* Upgrade - Option to subscribe
* Upgrade - Better advertisement

= 1.0.5 =
* Fix - Better WooCommerce 3 compatibility
* Fix - Premium plugin link on settings page

= 1.0.4 =
* Compatibility with WooCommerce 3.0.0
* Better compatibility with WPML

= 1.0.3 =
* Change styles in admin panel
* Small fixes

= 1.0.2 =
* Better support for PHP5.2

= 1.0.1.2 =
* First public version
