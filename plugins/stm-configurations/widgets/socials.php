<?php

class Stm_Socials_Widget extends WP_Widget
{

    /**
     * Register widget with WordPress.
     */
    function __construct()
    {
        parent::__construct(
            'socials', // Base ID
            __('Socials', 'stm-configurations'), // Name
            array('description' => __('Display a socials list from theme options section.', 'stm-configurations'),) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance)
    {
        $title = (!empty($instance['title'])) ? apply_filters('widget_title', $instance['title']) : esc_html__('Contacts', 'stm-configurations');

        echo $args['before_widget'];
        if (!empty($title)) {
            echo $args['before_title'] . esc_html($title) . $args['after_title'];
        }

        $socials = elab_get_option('socials', '{}');
        $socials = json_decode($socials, true);

        if (!empty($socials)):
            elab_enqueue_parted_style('socials', 'footer_parts/');
            ?>
            <div class="footer_socials">
                <?php foreach ($socials as $social => $social_url): ?>
                    <a href="<?php echo esc_url($social_url) ?>">
                        <i class="fab fa-<?php echo esc_attr($social); ?>"></i>
                    </a>
                <?php endforeach; ?>
            </div>
        <?php endif;

        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form($instance)
    {


        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('Socials', 'stm-configurations');
        }

        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'stm-configurations'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>"
                   name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>">
        </p>
        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? esc_attr($new_instance['title']) : '';

        return $instance;
    }

}

function stmt_register_socials_widget()
{
    register_widget('Stm_Socials_Widget');
}

add_action('widgets_init', 'stmt_register_socials_widget');