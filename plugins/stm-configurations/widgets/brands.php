<?php

class Stm_Brands_Widget extends WP_Widget
{

    /**
     * Register widget with WordPress.
     */
    function __construct()
    {
        parent::__construct(
            'brands', // Base ID
            __('Filter Products by Brand', 'stm-configurations'), // Name
            array('description' => __('Display a list of attributes to filter products in your store.', 'stm-configurations'),) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance)
    {
        $title = (!empty($instance['title'])) ? apply_filters('widget_title', $instance['title']) : esc_html__('Contacts', 'stm-configurations');

        echo $args['before_widget'];
        if (!empty($title)) {
            echo $args['before_title'] . esc_html($title) . $args['after_title'];
        }

        $brands = get_terms('stmt_brand_taxonomy');
        if (!empty($brands)):
            $queried_obj = get_queried_object();
            ?>
            <ul class="woocommerce-widget-layered-nav-list">
                <?php foreach ($brands as $brand):
                    $active = (!empty($_GET['brand']) and $_GET['brand'] == $brand->slug) ? 'woocommerce-widget-layered-nav-list__item--chosen chosen' : '';
                    if(!empty($queried_obj->term_id)) {
                        $active = ($queried_obj->term_id == $brand->term_id) ? 'woocommerce-widget-layered-nav-list__item--chosen chosen' : '';
                    }
                    ?>
                    <li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term <?php echo esc_attr($active); ?>">
                        <!--<a rel="nofollow" href="<?php /*echo esc_url(add_query_arg(array('brand' => $brand->slug), $current_url)); */?>">-->
                        <a rel="nofollow" href="<?php echo esc_url(get_term_link($brand)); ?>">
                            <?php echo sanitize_text_field($brand->name); ?>
                        </a>
                        <span class="count">(<?php echo sanitize_text_field($brand->count); ?>)</span>
                    </li>
                <?php endforeach; ?>
            </ul>

        <?php endif;

        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form($instance)
    {


        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('Brands', 'stm-configurations');
        }

        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'stm-configurations'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>"
                   name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>">
        </p>
        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? esc_attr($new_instance['title']) : '';

        return $instance;
    }

}

function stmt_register_brands_widget()
{
    register_widget('Stm_Brands_Widget');
}

add_action('widgets_init', 'stmt_register_brands_widget');