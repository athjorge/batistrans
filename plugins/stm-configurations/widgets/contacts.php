<?php

class Stm_Contacts_Widget extends WP_Widget
{

    /**
     * Register widget with WordPress.
     */
    function __construct()
    {
        parent::__construct(
            'contacts', // Base ID
            __('Contacts', 'stm-configurations'), // Name
            array('description' => __('Contacts widget', 'stm-configurations'),) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance)
    {
        $title = (!empty($instance['title'])) ? apply_filters('widget_title', $instance['title']) : esc_html__('Contacts', 'stm-configurations');

        if (function_exists('elab_enqueue_parted_style')) {
            elab_enqueue_parted_style('contacts_widget', 'widgets/');
        }

        echo $args['before_widget'];
        if (!empty($title)) {
            echo $args['before_title'] . esc_html($title) . $args['after_title'];
        }
        echo '<ul class="widget_contacts">';

        if (!empty($instance['address'])) { ?>
            <li class="widget_contacts_address">
                <div class="icon"><i class="lnricons-map-marker"></i></div>
                <div class="text"><?php echo($instance['address']); ?></div>
            </li>
        <?php }

        if (!empty($instance['phone'])) { ?>
            <li class="widget_contacts_phone">
                <div class="icon"><i class="lnricons-telephone"></i></div>
                <div class="text"><?php echo $instance['phone']; ?></div>
            </li>
        <?php }

        if (!empty($instance['fax'])) { ?>
            <li class="widget_contacts_fax">
                <div class="icon"><i class="lnricons-phone-bubble"></i></div>
                <div class="text"><?php echo($instance['fax']); ?></div>
            </li>
        <?php }

        if (!empty($instance['email'])) { ?>
            <li class="widget_contacts_email">
                <div class="icon"><i class="lnricons-envelope"></i></div>
                <div class="text">
                    <a href="mailto:<?php echo sanitize_email($instance['email']); ?>">
                        <?php echo sanitize_email($instance['email']); ?>
                    </a>
                </div>
            </li>
        <?php }

        if (!empty($instance['hours'])) { ?>
            <li class="widget_contacts_hours">
                <div class="icon"><i class="lnricons-clock3"></i></div>
                <div class="text"><?php echo($instance['hours']); ?></div>
            </li>
        <?php }

        echo '</ul>';


        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form($instance)
    {

        $title = '';
        $address = '';
        $phone = '';
        $fax = '';
        $email = '';
        $hours = '';

        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('Contact', 'stm-configurations');
        }

        if (isset($instance['address'])) {
            $address = $instance['address'];
        }

        if (isset($instance['phone'])) {
            $phone = $instance['phone'];
        }

        if (isset($instance['fax'])) {
            $fax = $instance['fax'];
        }

        if (isset($instance['email'])) {
            $email = $instance['email'];
        }

        if (isset($instance['hours'])) {
            $hours = $instance['hours'];
        }

        $instance['style'] = (!empty($instance['style'])) ? $instance['style'] : 'style_1';

        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'stm-configurations'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>"
                   name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('address')); ?>"><?php _e('Address:', 'stm-configurations'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('address')); ?>"
                   name="<?php echo esc_attr($this->get_field_name('address')); ?>" type="text"
                   value="<?php echo esc_attr($address); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('phone')); ?>"><?php _e('Phone:', 'stm-configurations'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('phone')); ?>"
                   name="<?php echo esc_attr($this->get_field_name('phone')); ?>" type="text"
                   value="<?php echo esc_attr($phone); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('fax')); ?>"><?php _e('Fax:', 'stm-configurations'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('fax')); ?>"
                   name="<?php echo esc_attr($this->get_field_name('fax')); ?>" type="text"
                   value="<?php echo esc_attr($fax); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('email')); ?>"><?php _e('E-mail:', 'stm-configurations'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('email')); ?>"
                   name="<?php echo esc_attr($this->get_field_name('email')); ?>" type="text"
                   value="<?php echo sanitize_email($email); ?>">
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('hours')); ?>"><?php _e('Service hours:', 'stm-configurations'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('hours')); ?>"
                   name="<?php echo esc_attr($this->get_field_name('hours')); ?>" type="text"
                   value="<?php echo esc_attr($hours); ?>">
        </p>
        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? esc_attr($new_instance['title']) : '';
        $instance['address'] = (!empty($new_instance['address'])) ? esc_attr($new_instance['address']) : '';
        $instance['phone'] = (!empty($new_instance['phone'])) ? esc_attr($new_instance['phone']) : '';
        $instance['fax'] = (!empty($new_instance['fax'])) ? esc_attr($new_instance['fax']) : '';
        $instance['email'] = (!empty($new_instance['email'])) ? sanitize_email($new_instance['email']) : '';
        $instance['hours'] = (!empty($new_instance['hours'])) ? esc_attr($new_instance['hours']) : '';

        return $instance;
    }

}

function register_contacts_widget()
{
    register_widget('Stm_Contacts_Widget');
}

add_action('widgets_init', 'register_contacts_widget');