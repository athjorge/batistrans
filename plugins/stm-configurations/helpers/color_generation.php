<?php
if (!defined('ABSPATH')) exit; //Exit if accessed directly

if(is_admin() or !empty($_GET['rcs'])) {
    add_action('init', 'stmt_generate_custom_css_colors');
}

function stmt_generate_custom_css_colors()
{

    global $wp_filesystem;

    if (empty($wp_filesystem)) {
        require_once ABSPATH . '/wp-admin/includes/file.php';
        WP_Filesystem();
    }

    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/stmt_theme_colors';

    if (!$wp_filesystem->is_dir($upload_dir)) {
        wp_mkdir_p($upload_dir);
    }

    /*X Builder*/
    if (defined('STM_X_BUILDER_DIR')) {
        /*Create Public and css folders inside*/

        if (!$wp_filesystem->is_dir("{$upload_dir}/x-builder")) {
            wp_mkdir_p("{$upload_dir}/x-builder");
        }

        if (!$wp_filesystem->is_dir("{$upload_dir}/x-builder/public")) {
            wp_mkdir_p("{$upload_dir}/x-builder/public");
        }

        if (!$wp_filesystem->is_dir("{$upload_dir}/x-builder/public/ccs")) {
            wp_mkdir_p("{$upload_dir}/x-builder/public/css");
        }

        $x_builder_upload_dir = "{$upload_dir}/x-builder";

        $target_dir = STM_X_BUILDER_DIR . '/public/css/';

        x_scan_dir($target_dir, $x_builder_upload_dir, STM_X_BUILDER_DIR);
    }

    if(defined('ELAB_PATH')) {
        /*Create Public and css folders inside*/

        if (!$wp_filesystem->is_dir("{$upload_dir}/theme")) {
            wp_mkdir_p("{$upload_dir}/theme");
        }

        if (!$wp_filesystem->is_dir("{$upload_dir}/theme/css")) {
            wp_mkdir_p("{$upload_dir}/theme/css");
        }

        $theme_upload_dir = "{$upload_dir}/theme";

        $target_dir = ELAB_PATH . '/assets/css/';

        x_scan_dir($target_dir, $theme_upload_dir, ELAB_PATH);
    }
}

function x_scan_dir($target_dir, $upload_dir, $main_dir)
{
    global $wp_filesystem;

    if (empty($wp_filesystem)) {
        require_once ABSPATH . '/wp-admin/includes/file.php';
        WP_Filesystem();
    }

    $dir_items = array_diff(scandir($target_dir), array('..', '.'));

    foreach ($dir_items as $dir_item) {
        $current_dir = $target_dir . $dir_item;
        if (is_dir($current_dir)) {

            $destination = str_replace($main_dir, $upload_dir, $current_dir);

            if (!$wp_filesystem->is_dir($destination)) {
                wp_mkdir_p($destination);
            }

            x_scan_dir($current_dir . '/', $upload_dir, $main_dir);

        } else {

            $file = stmt_replace_colors(file_get_contents($current_dir));
            $destination = str_replace($main_dir, $upload_dir, $current_dir);

            $wp_filesystem->put_contents($destination, $file, FS_CHMOD_FILE);
        }
    }
}

function stmt_replace_colors($file_content) {
    if(function_exists('elab_site_colors')) {

        $image_path = get_template_directory_uri() . "/assets/";

        $original_colors = array(
            '#292e38',
            '#ffc400',
            '#ffc401',
            '#292E38',
            '#FFC400',
            '#FFC401',
            '../../',
            '../',
        );

        /*Here We Need To get Theme Options Colors*/
        $replace_colors = elab_site_colors();
        $additional_settings = array($image_path, $image_path);

        $replace_colors = array_merge($replace_colors, $replace_colors);

        $replace_colors = array_merge($replace_colors, $additional_settings);

        return str_replace($original_colors, $replace_colors, $file_content);
    }
}

if(STMT_TO_DIST) {

    add_filter('elab_assets_css_path', 'stmt_stmt_theme_assets_css_path');

    function stmt_stmt_theme_assets_css_path()
    {

        $upload = wp_upload_dir();
        $upload_url = $upload['baseurl'] . '/stmt_theme_colors/theme/assets/css/';

        return $upload_url;
    }

    add_filter('x_builder_css_path', 'stmt_x_builder_css_path');

    function stmt_x_builder_css_path($path)
    {

        $upload = wp_upload_dir();
        $upload_url = $upload['baseurl'] . '/stmt_theme_colors/x-builder/public/css/';

        return $upload_url;
    }
}