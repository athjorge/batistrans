<?php
add_filter('x_get_elements', 'x_add_portfolio_element', 10, 2);

function x_add_portfolio_element($elements, $is_api)
{

    $terms = ($is_api) ? stm_x_get_terms('stmt_portfolio_category') : array();

    $elements[] = array(
        "module" => "x_portfolio",
        "name" => "Portfolio",
        "params" => array(
            "fields" => array(
                array(
                    "id" => "categories",
                    "type" => "multiselect",
                    "label" => "Categories",
                    "value" => "",
                    "options" => $terms
                )
            )
        )
    );

    return $elements;
}

add_action('wp_ajax_x_get_portfolio', 'x_get_portfolio');
add_action('wp_ajax_nopriv_x_get_portfolio', 'x_get_portfolio');

function x_get_portfolio()
{
    $category = (!empty($_GET['term_id'])) ? intval($_GET['term_id']) : '';
    $offset = (!empty($_GET['offset'])) ? intval($_GET['offset']) : 0;
    $per_page = 11;

    $r = array(
        'products' => array(),
        'offset' => $offset,
        'per_page' => $per_page
    );

    $args = array(
        'post_type' => 'stmt-portfolio',
        'posts_per_page' => $per_page,
        'offset' => $per_page * $offset
    );

    if (!empty($category)) {
        $args['tax_query'] = array(
            array(
                'field' => 'term_id',
                'taxonomy' => 'stmt_portfolio_category',
                'terms' => $category
            )
        );
    }

    $images_layout = array(
        /*1*/
        array('25', array(380, 468)),
        /*2*/
        array('25', array(380, 468)),
        /*3*/
        array('50', array(760, 936)),
        /*4*/
        array('50', array(760, 467)),
        /*5*/
        array('33.33', array(490, 490)),
        /*6*/
        array('33.33', array(490, 490)),
        /*7*/
        array('33.33', array(490, 490)),
        /*1*/
        array('25', array(380, 468)),
        /*2*/
        array('25', array(380, 468)),
        /*3*/
        array('50', array(760, 936)),
        /*4*/
        array('50', array(760, 467)),
    );


    $q = new WP_Query($args);

    if ($q->have_posts()) {
        $i = 0;
        $r['total'] = intval($q->found_posts);
        while ($q->have_posts()) {
            $q->the_post();
            $id = get_the_ID();

            /*Categories*/
            $cats = wp_get_post_terms($id, 'stmt_portfolio_category');
            if (!empty($cats) and !is_wp_error($cats)) {
                $cats = wp_list_pluck($cats, 'name');
            }

            $r['products'][] = array(
                'id' => $id,
                'title' => get_the_title(),
                'permalink' => get_the_permalink(),
                'cats' => implode(', ', $cats),
                'width' => $images_layout[$i][0],
                'image' => stm_x_builder_get_cropped_image_url(get_post_thumbnail_id(), $images_layout[$i][1][0], $images_layout[$i][1][1]),
            );

            $i++;
        }

        wp_reset_postdata();
        wp_reset_query();
    }

    wp_send_json($r);
    exit;
}