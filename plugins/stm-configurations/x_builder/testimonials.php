<?php
add_filter('x_get_elements', 'x_add_testimonials_element');

function x_add_testimonials_element($elements)
{

    $elements[] = array(
        "module" => "x_testimonials",
        "name"   => "Testimonials",
        "params" => array(
            "fields" => array(
                array(
                    "id"         => "per_row",
                    "type"       => "select",
                    "options" => array(
                        1 => 1,
                        2 => 2,
                        3 => 3,
                        4 => 4,
                    ),
                    "value" => 2,
                    "label"      => esc_html__("Per Row", 'stmt_theme_configurations'),
                ),
                array(
                    "id"         => "number",
                    "type"       => "number",
                    "label"      => esc_html__("Number of posts", 'stmt_theme_configurations'),
                ),
                array(
                    "id"         => "style",
                    "type"       => "select",
                    "options" => array(
                        "style_1" => "Style 1",
                        "style_2" => "Style 2"
                    ),
                    "value" => "style_1",
                    "label"      => esc_html__("Style", 'stmt_theme_configurations'),
                ),
            )
        )
    );

    return $elements;
}