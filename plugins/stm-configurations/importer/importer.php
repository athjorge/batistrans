<?php
require_once(STM_CONFIGURATIONS_DIR . '/importer/helpers/content.php');
require_once(STM_CONFIGURATIONS_DIR . '/importer/helpers/theme_options.php');
require_once(STM_CONFIGURATIONS_DIR . '/importer/helpers/slider.php');
require_once(STM_CONFIGURATIONS_DIR . '/importer/helpers/widgets.php');
require_once(STM_CONFIGURATIONS_DIR . '/importer/helpers/set_content.php');
require_once(STM_CONFIGURATIONS_DIR . '/importer/helpers/set_hb_options.php');
require_once(STM_CONFIGURATIONS_DIR . '/importer/helpers/x_builder.php');
require_once(STM_CONFIGURATIONS_DIR . '/importer/helpers/yith.php');

function stm_demo_import_content()
{
    check_ajax_referer('stm_demo_import_content', 'security');
    $layout = 'layout-1';

    if(!empty($_GET['demo_template'])){
        $layout = sanitize_title($_GET['demo_template']);
    }

	update_option('stm_layout_mode', $layout);

    /*Import content*/
    stm_theme_import_content($layout);

    /*Import theme options*/
	stm_set_layout_options($layout);

    /*Import sliders*/
    stm_theme_import_sliders($layout);

    /*Import Widgets*/
    stm_theme_import_widgets($layout);

    /*Set menu and pages*/
    stm_set_content_options($layout);

	/*Set hb_options*/
	stm_set_hb_options($layout);

    /*Set x builder */
    stm_set_x_fields($layout);

	do_action('stmt_theme_importer_done');

	wp_send_json(array(
		'url' => get_home_url('/'),
		'title' => esc_html__('View site', 'stm-configurations'),
		'theme_options_title' => esc_html__('Theme options', 'stm-configurations'),
		'theme_options' => esc_url_raw(admin_url('admin.php?page=stmt-to-settings')),
        'yith' => esc_url_raw(add_query_arg('stm_yith_fix', $layout, admin_url()))
	));

}

add_action('wp_ajax_stm_demo_import_content', 'stm_demo_import_content');