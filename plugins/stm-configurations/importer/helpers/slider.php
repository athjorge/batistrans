<?php

function stm_theme_import_sliders($layout)
{
    $slider_names = array(
        'layout-1' => array(
            'main_slider',
            'home_slider',
        ),
        'layout-2' => array(
            'main_slider',
        ),
        'layout-3' => array(
            'main_slider',
            'home_slider',
            'slider-1',
        ),
        'layout-4' => array(
            'main_slider',
            'home_slider',
        ),
        'layout-5' => array(
            'main_slider',
            'home_slider',
        ),
        'layout-6' => array(
            'main_slider',
            'home_slider',
        ),
        'layout-7' => array(
            'main_slider',
            'home_slider',
        ),
        'layout-8' => array(
            'main_slider',
            'home_slider',
        ),
        'layout-9' => array(
            'main_slider',
            'home_slider',
        ),
    );

    if (!empty($slider_names[$layout])) {

        if (class_exists('RevSlider')) {
            $path = STM_CONFIGURATIONS_DIR . '/importer/demos/' . $layout . '/sliders/';
            foreach ($slider_names[$layout] as $slider_name) {
                $slider_path = $path . $slider_name . '.zip';
                if (file_exists($slider_path)) {
                    $slider = new RevSlider();
                    $slider->importSliderFromPost(true, true, $slider_path);
                }
            }
        }

    }
}