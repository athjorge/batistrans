<?php

function stm_set_content_options($layout)
{
    $locations = get_theme_mod('nav_menu_locations');
    $menus = wp_get_nav_menus();

    if (!empty($menus)) {
        foreach ($menus as $menu) {

            if (is_object($menu) && $menu->name === 'All Pages') {
                $locations['top-bar'] = $menu->term_id;
            }

            if (is_object($menu) && $menu->name === 'Main Menu') {

                $locations['menu-header'] = $menu->term_id;
            }

        }
    }

    set_theme_mod('nav_menu_locations', $locations);

    update_option('show_on_front', 'page');

    $front_pages = array(
        'Front Page',
        'Home'
    );

    foreach ($front_pages as $front_page_name) {
        $front_page = get_page_by_title($front_page_name);
        if (isset($front_page->ID)) {
            update_option('page_on_front', $front_page->ID);
        }
    }

    $blog_page = get_page_by_title('Blog');
    if (isset($blog_page->ID)) {
        update_option('page_for_posts', $blog_page->ID);
    }

    $fxml = get_temp_dir() . $layout . '.xml';
    $fzip = get_temp_dir() . $layout . '.zip';
    if (file_exists($fxml)) @unlink($fxml);
    if (file_exists($fzip)) @unlink($fzip);
}