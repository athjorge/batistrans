<?php
if (!empty($_GET['stm_yith_fix'])) {
    add_action('init', function () {
        $layout = sanitize_text_field($_GET['stm_yith_fix']);

        global $wp_filesystem;

        if (empty($wp_filesystem)) {
            require_once ABSPATH . '/wp-admin/includes/file.php';
            WP_Filesystem();
        }

        $widgets = STM_CONFIGURATIONS_DIR . '/importer/demos/' . $layout . '/yith/yith.json';

        if (file_exists($widgets)) {
            $yith_opt = json_decode($wp_filesystem->get_contents($widgets), true);
            foreach ($yith_opt as $opt_name => $opt_value) {
                update_option($opt_name, $opt_value);
            }
        }

    });
}