<?php

function stm_set_layout_options($layout)
{
    global $wp_filesystem;

    if (empty($wp_filesystem)) {
        require_once ABSPATH . '/wp-admin/includes/file.php';
        WP_Filesystem();
    }

    $options = STM_CONFIGURATIONS_DIR . '/importer/demos/' . $layout . '/options/options.json';

    if (file_exists($options)) {
        $encode_options = $wp_filesystem->get_contents($options);
        $import_options = json_decode( $encode_options, true );


        stm_recreate_to_settings($import_options);

        update_option('stmt_to_settings', $import_options);

    }

}

function stm_recreate_to_settings(&$options) {

    $placeholder_name = 'placeholder_image_stylemixthemes_999x999';
    $placeholder = stmt_it_wp_get_attachment_by_post_name($placeholder_name);
    $placeholder_id = (!empty($placeholder)) ? $placeholder->ID : '';

    if(!empty($options['header_categories'])) {
        $taxonomy = 'product_cat';
        $header_cats = json_decode($options['header_categories'], true);
        if(!empty($header_cats)) {
            foreach($header_cats as &$term_data) {
                $term = get_term_by('name', $term_data['name'], $taxonomy);
                if(empty($term) or is_wp_error($term)) continue;
                $term_data = array(
                    'name' => $term->name,
                    'value' => $term->term_id,
                );
            }
        }

        $options['header_categories'] = json_encode($header_cats);
    }

    if(!empty($options['promo_popup_image'])) {
        if (empty($placeholder)) return false;
        $placeholder_id = $placeholder->ID;
        $options['promo_popup_image'] = $placeholder_id;
    }

    if(!empty($options['shop_banner'])) {
        $banner = get_page_by_path('Shop Banner', 'ARRAY_A', 'stmt-banners');
        if(!empty($banner) and !is_wp_error($banner)) {
            $options['shop_banner'] = $banner['id'];
        }
    }

}