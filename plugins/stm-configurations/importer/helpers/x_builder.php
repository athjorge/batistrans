<?php


function stm_set_x_fields($layout)
{

    global $wp_filesystem;

    if (empty($wp_filesystem)) {
        require_once ABSPATH . '/wp-admin/includes/file.php';
        WP_Filesystem();
    }

    stmt_products_set_additional_images();
    stmt_it_setup_woocommerce_pages();

    $widgets = STM_CONFIGURATIONS_DIR . '/importer/demos/' . $layout . '/x_builder/builder.json';

    if (file_exists($widgets)) {
        $x_options = json_decode($wp_filesystem->get_contents($widgets), true);

        $images = $x_options['images'];

        $x_content = $x_options['meta_fields'];

        foreach ($x_content as $item_key => $meta_value) {
            $key = explode('|', $item_key);
            $post_type = $key[0];
            $post_title = $key[1];
            $meta_key = $key[2];

            $post = get_page_by_title($post_title, 'ARRAY_A', $post_type);

            $meta_value = stmt_walk_x_content($meta_value, $meta_value);

            if (!empty($post)) {
                $post_id = $post['ID'];

                $update = update_post_meta($post_id, $meta_key, $meta_value);

            }

        }

    }

}

function stmt_walk_x_content(&$chunk, &$content)
{

    if (!is_array($chunk)) return $content;

    foreach ($chunk as $index => &$value) {

        if (!empty($value['term_id'])) {

            $term = get_term_by('slug', $value['name'], $value['taxonomy'], 'ARRAY_A');
            if (is_wp_error($term) or empty($term)) continue;

            $value = $term;
        }

        if (is_array($value)) {
            $content = stmt_walk_x_content($value, $content);
        }

    }

    return $content;

}

function stmt_products_set_additional_images()
{
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => '-1',
        'post_status' => 'any'
    );

    $q = new WP_Query($args);

    $hint_images = array(
        'hint_image_1',
        'hint_image_2',
        'hint_image_3',
    );

    $placeholder_name = 'placeholder_image_stylemixthemes_999x999';
    $placeholder = stmt_it_wp_get_attachment_by_post_name($placeholder_name);
    if (empty($placeholder)) return false;
    $placeholder_id = $placeholder->ID;
    $placeholder_url = wp_get_attachment_image_src($placeholder_id, 'full');

    if ($q->have_posts()) {
        while ($q->have_posts()) {
            $q->the_post();

            $id = get_the_ID();

            foreach ($hint_images as $hint_image_name) {
                $hint_image = get_post_meta($id, $hint_image_name, true);
                if (empty($hint_image)) continue;

                $hint_image = json_decode($hint_image, true);
                $hint_image['image_id'] = $placeholder_id;
                $hint_image['image_url'] = $placeholder_url[0];

                update_post_meta($id, $hint_image_name, json_encode($hint_image));
            }

            $additional_image = get_post_meta($id, 'product_carousel_grid', true);
            if (empty($additional_image)) continue;

            update_post_meta($id, 'product_carousel_grid', $placeholder_id);

        }

        wp_reset_postdata();
        wp_reset_query();
    }
}

function stmt_it_setup_woocommerce_pages() {
    $pages = array(
        'woocommerce_shop_page_id' => 'Shop',
        'woocommerce_cart_page_id' => 'Cart',
        'woocommerce_checkout_page_id' => 'Checkout',
        'yith_wcwl_wishlist_page_id' => 'Wishlist',
    );

    foreach($pages as $option => $page_title) {
        $page = get_page_by_title($page_title);
        if(empty($page) or is_wp_error($page)) continue;

        update_option($option, $page->ID);
    }
}

function stmt_it_wp_get_attachment_by_post_name($post_name)
{
    $args = array(
        'posts_per_page' => 1,
        'post_type' => 'attachment',
        'name' => trim($post_name),
    );

    $get_attachment = new WP_Query($args);

    if (!$get_attachment || !isset($get_attachment->posts, $get_attachment->posts[0])) {
        return false;
    }

    wp_reset_postdata();
    wp_reset_query();

    return $get_attachment->posts[0];
}