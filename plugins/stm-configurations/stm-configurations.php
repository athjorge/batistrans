<?php
/*
Plugin Name: STM Configurations
Plugin URI: https://stylemixthemes.com/
Description: Configurations plugin.
Author: StylemixThemes
Author URI: https://stylemixthemes.com/
Text Domain: stm-configurations
Version: 1.0.6
*/

define( 'STM_CONFIGURATIONS_DIR', dirname(__FILE__) );
define( 'STM_CONFIGURATIONS_PATH', plugin_basename(__FILE__) );
define( 'STM_CONFIGURATIONS_URL', plugins_url('/', __FILE__) );

define('STMT_TO_DIR', STM_CONFIGURATIONS_DIR . '/theme-options');
define('STMT_TO_URL', STM_CONFIGURATIONS_URL . '/theme-options');
define('STMT_TO_DIST', true);

if (!is_textdomain_loaded('stm-configurations')) {
	load_plugin_textdomain(
		'stm-configurations',
		false,
		'stm-configurations/languages'
	);
}

require_once STM_CONFIGURATIONS_DIR . '/helpers/crop-images.php';
require_once STM_CONFIGURATIONS_DIR . '/helpers/mailchimp/mailchimp.php';
require_once STM_CONFIGURATIONS_DIR . '/helpers/share.php';
require_once STM_CONFIGURATIONS_DIR . '/helpers/widgets.php';
require_once STM_CONFIGURATIONS_DIR . '/helpers/woocommerce/woocommerce.php';
require_once STM_CONFIGURATIONS_DIR . '/helpers/theme.php';
require_once STM_CONFIGURATIONS_DIR . '/widgets/contacts.php';
require_once STM_CONFIGURATIONS_DIR . '/widgets/recent_news.php';
require_once STM_CONFIGURATIONS_DIR . '/widgets/brands.php';
require_once STM_CONFIGURATIONS_DIR . '/widgets/socials.php';

require_once STMT_TO_DIR . '/post_type/posts.php';
//
require_once STM_CONFIGURATIONS_DIR . '/megamenu/main.php';

/*X-builder Modules*/
require_once STM_CONFIGURATIONS_DIR . '/x_builder/testimonials.php';
require_once STM_CONFIGURATIONS_DIR . '/x_builder/portfolio.php';

require_once STM_CONFIGURATIONS_DIR . '/helpers/color_generation.php';

if(is_admin()) {
	require_once STM_CONFIGURATIONS_DIR . '/importer/importer.php';
	require_once STM_CONFIGURATIONS_DIR . '/theme-options/theme-options.php';
}