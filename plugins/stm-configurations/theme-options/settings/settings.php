<?php

class STMT_TO_Settings
{

    private static $typography_fonts = array();
    private static $themeOptions = array();

    function __construct()
    {
        self::stmt_to_get_settings();
        self::set_typography_fonts();

        add_action('admin_menu', array($this, 'stmt_to_settings_page'), 1000);
        add_action('wp_ajax_stmt_save_settings', array($this, 'stmt_save_settings'));

        add_action('wp_ajax_stmt_get_settings', array($this, 'get_settings'));

        add_action('wp_enqueue_scripts', array($this, 'stmt_enqueue_custom_fonts'), 160);
        add_action('wp_enqueue_scripts', array($this, 'stmt_enqueue_typography_css'), 200);

        add_filter('stmt_to_settings', array($this, 'add_shop_features'));
    }

    function add_shop_features($options)
    {

        for ($i = 1; $i < 6; $i++) {
            $options['args']['stmt_to_settings']['section_1_0']['fields']["shop_feature_icon_{$i}"] = array(
                'type' => 'iconpicker',
                'label' => sprintf(esc_html__('Shop Feature %s Icon', 'stmt_theme_options'), $i),
                'columns' => 50
            );
            $options['args']['stmt_to_settings']['section_1_0']['fields']["shop_feature_title_{$i}"] = array(
                'type' => 'text',
                'label' => sprintf(esc_html__('Shop Feature %s Title', 'stmt_theme_options'), $i),
                'columns' => 50
            );
        }


        return $options;

    }

    function stmt_to_settings_page()
    {
        add_menu_page(
            'Theme Options',
            'Theme Options',
            'manage_options',
            'stmt-to-settings',
            array($this, 'stmt_to_settings_page_view'),
            'dashicons-admin-tools',
            5
        );
    }

    public static function stmt_get_post_type_array($post_type, $args = array())
    {
        $r = array(
            '' => esc_html__('Choose Page', 'stmt_theme_options'),
        );

        $default_args = array(
            'post_type' => $post_type,
            'posts_per_page' => -1,
            'post_status' => 'publish'
        );

        $q = get_posts(wp_parse_args($args, $default_args));

        if (!empty($q)) {
            foreach ($q as $post_data) {
                $r[$post_data->ID] = $post_data->post_title;
            }
        }

        wp_reset_query();

        return $r;
    }
    function stmt_to_settings()
    {
        global $wp_registered_sidebars;
        $sidebars = array('' => esc_html__('Select Sidebar', 'stmt_theme_options'));
        $sidebarPosition = array(
            'no_sidebar' => esc_html__('Without Sidebar', 'stmt_theme_options'),
            'left' => esc_html__('Left', 'stmt_theme_options'),
            'right' => esc_html__('Right', 'stmt_theme_options')
        );

        $banners = $this->stmt_get_post_type_array('stmt-banners');
        foreach ($wp_registered_sidebars as $k => $val) {
            $sidebars[$val['id']] = $val['name'];
        }

        $columns = array(
            '12' => '100%',
            '9' => '75%',
            '6' => '50%',
            '4' => '33.3%',
            '3' => '25%',
            '2' => '16.6%',
        );

        return apply_filters('stmt_to_settings', array(
            'id' => 'stmt_to_settings',
            'args' => array(
                'stmt_to_settings' => array(
                    'section_1' => array(
                        'name' => esc_html__('General', 'stmt_theme_options'),
                        'fields' => array(
                            'content_max_width' => array(
                                'type' => 'text',
                                'label' => esc_html__('Content Max Width', 'stmt_theme_options'),
                                'columns' => ''
                            ),
                            'post_sidebar_id' => array(
                                'type' => 'select',
                                'label' => esc_html__('Global Sidebar Name', 'stmt_theme_options'),
                                'options' => $sidebars,
                                'value' => ''
                            ),
                            'post_sidebar_position' => array(
                                'type' => 'select',
                                'label' => esc_html__('Global Sidebar Position', 'stmt_theme_options'),
                                'options' => $sidebarPosition,
                                'value' => 'no_sidebar',
                            ),
                            'google_map_api_key' => array(
                                'type' => 'text',
                                'label' => esc_html__('Google Map API key', 'stmt_theme_options'),
                            ),
                            'promo_popup' => array(
                                'type' => 'checkbox',
                                'label' => esc_html__('Show Promo Popup', 'stmt_theme_options'),
                            ),
                            'promo_popup_title' => array(
                                'type' => 'text',
                                'label' => esc_html__('Promo Popup Title', 'stmt_theme_options'),
                                'dependency' => array(
                                    'key' => 'promo_popup',
                                    'value' => 'not_empty'
                                )
                            ),
                            'promo_popup_image' => array(
                                'type' => 'image',
                                'label' => esc_html__('Promo Popup Image', 'stmt_theme_options'),
                                'dependency' => array(
                                    'key' => 'promo_popup',
                                    'value' => 'not_empty'
                                )
                            ),
                            'enable_preloader' => array(
                                'type' => 'checkbox',
                                'label' => esc_html__('Enable Preloader', 'stmt_theme_options'),
                            ),
                        )
                    ),
                    'section_1_0' => array(
                        'name' => esc_html__('Header', 'stmt_theme_options'),
                        'fields' => array(
                            'header_style' => array(
                                'type' => 'radio_images',
                                'label' => esc_html__('Header Style', 'stmt_theme_options'),
                                'options' => array(
                                    '1' => array(
                                        'title' => esc_html__('Header Style 1', 'stmt_theme_options'),
                                        'image' => STM_CONFIGURATIONS_URL . '/theme-options/settings/images/header_1.png',
                                    ),
                                    '2' => array(
                                        'title' => esc_html__('Header Style 2', 'stmt_theme_options'),
                                        'image' => STM_CONFIGURATIONS_URL . '/theme-options/settings/images/header_2.png',
                                    ),
                                    '3' => array(
                                        'title' => esc_html__('Header Style 3', 'stmt_theme_options'),
                                        'image' => STM_CONFIGURATIONS_URL . '/theme-options/settings/images/header_3.png',
                                    ),
                                    '4' => array(
                                        'title' => esc_html__('Header Style 4', 'stmt_theme_options'),
                                        'image' => STM_CONFIGURATIONS_URL . '/theme-options/settings/images/header_4.png',
                                    ),
                                    '5' => array(
                                        'title' => esc_html__('Header Style 5', 'stmt_theme_options'),
                                        'image' => STM_CONFIGURATIONS_URL . '/theme-options/settings/images/header_5.png',
                                    ),
                                    '6' => array(
                                        'title' => esc_html__('Header Style 6', 'stmt_theme_options'),
                                        'image' => STM_CONFIGURATIONS_URL . '/theme-options/settings/images/header_6.png',
                                    ),
                                    '7' => array(
                                        'title' => esc_html__('Header Style 7', 'stmt_theme_options'),
                                        'image' => STM_CONFIGURATIONS_URL . '/theme-options/settings/images/header_7.png',
                                    ),
                                    '8' => array(
                                        'title' => esc_html__('Header Style 8', 'stmt_theme_options'),
                                        'image' => STM_CONFIGURATIONS_URL . '/theme-options/settings/images/header_8.jpg',
                                    ),
                                )
                            ),
                            'logo' => array(
                                'type' => 'image',
                                'label' => esc_html__('Site logo', 'stmt_theme_options'),
                            ),
                            'header_hot_title' => array(
                                'type' => 'text',
                                'label' => esc_html__('Header Hot Title', 'stmt_theme_options'),
                            ),
                            'header_show_woo' => array(
                                'type' => 'checkbox',
                                'label' => esc_html__('Show Woocommerce buttons', 'stmt_theme_options'),
                            ),
                            'iconbox_icon_1' => array(
                                'type' => 'iconpicker',
                                'label' => esc_html__('Iconbox 1 Icon', 'stmt_theme_options'),
                            ),
                            'iconbox_icon_1_title' => array(
                                'type' => 'text',
                                'label' => esc_html__('Iconbox 1 title', 'stmt_theme_options'),
                                'columns' => 50
                            ),
                            'iconbox_icon_1_sub_title' => array(
                                'type' => 'text',
                                'label' => esc_html__('Iconbox 1 subtitle', 'stmt_theme_options'),
                                'columns' => 50
                            ),
                            'iconbox_icon_2' => array(
                                'type' => 'iconpicker',
                                'label' => esc_html__('Iconbox 2 Icon', 'stmt_theme_options'),
                            ),
                            'iconbox_icon_2_title' => array(
                                'type' => 'text',
                                'label' => esc_html__('Iconbox 2 title', 'stmt_theme_options'),
                                'columns' => 50
                            ),
                            'iconbox_icon_2_sub_title' => array(
                                'type' => 'text',
                                'label' => esc_html__('Iconbox 2 subtitle', 'stmt_theme_options'),
                                'columns' => 50
                            ),
                            'header_categories' => array(
                                'type' => 'multiselect',
                                'label' => esc_html__('Categories', 'stmt_theme_options'),
                                'options' => 'product_cat'
                            ),
                            'header_socials' => array(
                                'type' => 'socials',
                                'label' => esc_html__('Header Socials', 'stmt_theme_options'),
                            ),
                        )
                    ),
                    'section_1_1' => array(
                        'name' => esc_html__('Page', 'stmt_theme_options'),
                        'fields' => array(
                            'show_title' => array(
                                'type' => 'checkbox',
                                'label' => esc_html__('Show Page Title', 'stmt_theme_options'),
                            ),
                            'show_sidebar' => array(
                                'type' => 'checkbox',
                                'label' => esc_html__('Show Page Sidebar', 'stmt_theme_options'),
                            ),
                        )
                    ),
                    'section_1_2' => array(
                        'name' => esc_html__('Style', 'stmt_theme_options'),
                        'fields' => array(
                            'primary_color' => array(
                                'type' => 'color',
                                'label' => esc_html__('Primary Color', 'stmt_theme_options'),
                                'columns' => '50'
                            ),
                            'secondary_color' => array(
                                'type' => 'color',
                                'label' => esc_html__('Secondary Color', 'stmt_theme_options'),
                                'columns' => '50'
                            ),
                            'third_color' => array(
                                'type' => 'color',
                                'label' => esc_html__('Third Color', 'stmt_theme_options'),
                                'columns' => '50'
                            ),
                            'page_color' => array(
                                'type' => 'color',
                                'label' => esc_html__('Site Background Color', 'stmt_theme_options'),
                                'columns' => '50'
                            ),
                            'body' => array(
                                'type' => 'typography',
                                'label' => esc_html__('Body Font', 'stmt_theme_options'),
                                'selector' => 'body, .normal-font'
                            ),
                            'default_header_font_family' => array(
                                'type' => 'select',
                                'label' => esc_html__('Default Headings Font Family', 'stmt_theme_options'),
                                'options' => stmt_get_google_fonts()
                            ),
                            'default_header_font_color' => array(
                                'type' => 'color',
                                'label' => esc_html__('Default Headings Font Color', 'stmt_theme_options'),
                            ),
                            'h1, .h1' => array(
                                'type' => 'typography',
                                'label' => esc_html__('H1', 'stmt_theme_options'),
                            ),
                            'h2, .h2' => array(
                                'type' => 'typography',
                                'label' => esc_html__('H2', 'stmt_theme_options'),
                            ),
                            'h3, .h3' => array(
                                'type' => 'typography',
                                'label' => esc_html__('H3', 'stmt_theme_options'),
                            ),
                            'h4, .h4' => array(
                                'type' => 'typography',
                                'label' => esc_html__('H4', 'stmt_theme_options'),
                            ),
                            'h5, .h5' => array(
                                'type' => 'typography',
                                'label' => esc_html__('H5', 'stmt_theme_options'),
                            ),
                            'h6, .h6' => array(
                                'type' => 'typography',
                                'label' => esc_html__('H6', 'stmt_theme_options'),
                            ),
                        )
                    ),
                    'section_3' => array(
                        'name' => esc_html__('Footer', 'stmt_theme_options'),
                        'fields' => array(
                            'footer_main_bg' => array(
                                'type' => 'color',
                                'label' => esc_html__('Footer Background Color', 'stmt_theme_options'),
                                'columns' => '50'
                            ),
                            'footer_main_text_color' => array(
                                'type' => 'color',
                                'label' => esc_html__('Footer Text Color', 'stmt_theme_options'),
                                'columns' => '50'
                            ),
                            'footer_main_links_color' => array(
                                'type' => 'color',
                                'label' => esc_html__('Footer Links Color', 'stmt_theme_options'),
                                'columns' => '50'
                            ),
                            'footer_main_links_action_color' => array(
                                'type' => 'color',
                                'label' => esc_html__('Footer Links Action Color', 'stmt_theme_options'),
                                'columns' => '50'
                            ),
                            'enable_footer_maiclhimp' => array(
                                'type' => 'checkbox',
                                'label' => esc_html__('Enable Pre Footer Mailchimp block', 'stmt_theme_options'),
                            ),
                            'prefooter_position' => array(
                                'type' => 'select',
                                'label' => esc_html__('Pre Footer Position', 'stmt_theme_options'),
                                'options' => array(
                                    'pre' => esc_html__('Before Footer', 'stmt_theme_options'),
                                    'inside' => esc_html__('Inside Footer', 'stmt_theme_options'),
                                    'after' => esc_html__('After Footer', 'stmt_theme_options'),
                                ),
                                'dependency' => array(
                                    'key' => 'enable_footer_maiclhimp',
                                    'value' => 'not_empty'
                                )
                            ),
                            'prefooter_icon' => array(
                                'type' => 'iconpicker',
                                'label' => esc_html__('Pre Footer Mailchimp Icon', 'stmt_theme_options'),
                                'dependency' => array(
                                    'key' => 'enable_footer_maiclhimp',
                                    'value' => 'not_empty'
                                )
                            ),
                            'prefooter_title' => array(
                                'type' => 'text',
                                'label' => esc_html__('Pre Footer Mailchimp Title', 'stmt_theme_options'),
                                'dependency' => array(
                                    'key' => 'enable_footer_maiclhimp',
                                    'value' => 'not_empty'
                                ),
                                'columns' => 50
                            ),
                            'prefooter_subtitle' => array(
                                'type' => 'text',
                                'label' => esc_html__('Pre Footer Mailchimp Subtitle', 'stmt_theme_options'),
                                'dependency' => array(
                                    'key' => 'enable_footer_maiclhimp',
                                    'value' => 'not_empty'
                                ),
                                'columns' => 50
                            ),
                            'prefooter_icon_2' => array(
                                'type' => 'iconpicker',
                                'label' => esc_html__('Pre Footer Mailchimp Icon Phone', 'stmt_theme_options'),
                                'dependency' => array(
                                    'key' => 'enable_footer_maiclhimp',
                                    'value' => 'not_empty'
                                )
                            ),
                            'prefooter_title_2' => array(
                                'type' => 'text',
                                'label' => esc_html__('Pre Footer Mailchimp Title Phone', 'stmt_theme_options'),
                                'dependency' => array(
                                    'key' => 'enable_footer_maiclhimp',
                                    'value' => 'not_empty'
                                ),
                                'columns' => 50
                            ),
                            'prefooter_subtitle_2' => array(
                                'type' => 'text',
                                'label' => esc_html__('Pre Footer Mailchimp Subtitle Phone', 'stmt_theme_options'),
                                'dependency' => array(
                                    'key' => 'enable_footer_maiclhimp',
                                    'value' => 'not_empty'
                                ),
                                'columns' => 50
                            ),
                            'prefooter_icon_3' => array(
                                'type' => 'iconpicker',
                                'label' => esc_html__('Pre Footer Mailchimp Icon Email', 'stmt_theme_options'),
                                'dependency' => array(
                                    'key' => 'enable_footer_maiclhimp',
                                    'value' => 'not_empty'
                                )
                            ),
                            'prefooter_title_3' => array(
                                'type' => 'text',
                                'label' => esc_html__('Pre Footer Mailchimp Title Email', 'stmt_theme_options'),
                                'dependency' => array(
                                    'key' => 'enable_footer_maiclhimp',
                                    'value' => 'not_empty'
                                ),
                                'columns' => 50
                            ),
                            'prefooter_subtitle_3' => array(
                                'type' => 'text',
                                'label' => esc_html__('Pre Footer Mailchimp Subtitle Email', 'stmt_theme_options'),
                                'dependency' => array(
                                    'key' => 'enable_footer_maiclhimp',
                                    'value' => 'not_empty'
                                ),
                                'columns' => 50
                            ),
                            'widget_area_1_width' => array(
                                'type' => 'select',
                                'label' => esc_html__('Footer 1 Width', 'stmt_theme_options'),
                                'options' => $columns,
                                'value' => '3',
                                'columns' => '50'
                            ),
                            'widget_area_2_width' => array(
                                'type' => 'select',
                                'label' => esc_html__('Footer 2 Width', 'stmt_theme_options'),
                                'options' => $columns,
                                'value' => '3',
                                'columns' => '50'
                            ),
                            'widget_area_3_width' => array(
                                'type' => 'select',
                                'label' => esc_html__('Footer 3 Width', 'stmt_theme_options'),
                                'options' => $columns,
                                'value' => '3',
                                'columns' => '50'
                            ),
                            'widget_area_4_width' => array(
                                'type' => 'select',
                                'label' => esc_html__('Footer 4 Width', 'stmt_theme_options'),
                                'options' => $columns,
                                'value' => '3',
                                'columns' => '50'
                            ),
                            'widget_area_5_width' => array(
                                'type' => 'select',
                                'label' => esc_html__('Footer 5 Width', 'stmt_theme_options'),
                                'options' => $columns,
                                'value' => '3',
                                'columns' => '50'
                            ),
                            'widget_area_6_width' => array(
                                'type' => 'select',
                                'label' => esc_html__('Footer 6 Width', 'stmt_theme_options'),
                                'options' => $columns,
                                'value' => '3',
                                'columns' => '50'
                            ),
                            'widget_area_1_width_sm' => array(
                                'type' => 'select',
                                'label' => esc_html__('Footer 1 Width Tablet', 'stmt_theme_options'),
                                'options' => $columns,
                                'columns' => '50'
                            ),
                            'widget_area_2_width_sm' => array(
                                'type' => 'select',
                                'label' => esc_html__('Footer 2 Width Tablet', 'stmt_theme_options'),
                                'options' => $columns,
                                'columns' => '50'
                            ),
                            'widget_area_3_width_sm' => array(
                                'type' => 'select',
                                'label' => esc_html__('Footer 3 Width Tablet', 'stmt_theme_options'),
                                'options' => $columns,
                                'columns' => '50'
                            ),
                            'widget_area_4_width_sm' => array(
                                'type' => 'select',
                                'label' => esc_html__('Footer 4 Width Tablet', 'stmt_theme_options'),
                                'options' => $columns,
                                'columns' => '50'
                            ),
                            'widget_area_5_width_sm' => array(
                                'type' => 'select',
                                'label' => esc_html__('Footer 5 Width Tablet', 'stmt_theme_options'),
                                'options' => $columns,
                                'columns' => '50'
                            ),
                            'widget_area_6_width_sm' => array(
                                'type' => 'select',
                                'label' => esc_html__('Footer 6 Width Tablet', 'stmt_theme_options'),
                                'options' => $columns,
                                'columns' => '50'
                            ),
                            'copyright' => array(
                                'type' => 'text',
                                'label' => esc_html__('Copyright', 'stmt_theme_options'),
                            ),
                            'copyright_image' => array(
                                'type' => 'image',
                                'label' => esc_html__('Copyright image', 'stmt_theme_options'),
                            ),
                        )
                    ),
                    'section_4' => array(
                        'name' => esc_html__('Socials', 'stmt_theme_options'),
                        'fields' => array(
                            'socials' => array(
                                'type' => 'socials',
                                'label' => 'Social Links'
                            )
                        )
                    ),
                    'section_4_1' => array(
                        'name' => esc_html__('MailChimp', 'stmt_theme_options'),
                        'fields' => array(
                            'mc_list_id' => array(
                                'type' => 'text',
                                'label' => esc_html__('Mailchimp List ID', 'stmt_theme_options'),
                            ),
                            'mc_api_key' => array(
                                'type' => 'text',
                                'label' => esc_html__('Mailchimp API key', 'stmt_theme_options'),
                            ),
                        )
                    ),
                    'section_5' => array(
                        'name' => esc_html__('Archive Pages', 'stmt_theme_options'),
                        'fields' => array(
                            '404_content_bg_img' => array(
                                'type' => 'image',
                                'label' => esc_html__('404 Page Content Img BG', 'stmt_theme_options'),
                            ),
                            'categ_head_bg_img' => array(
                                'type' => 'image',
                                'label' => esc_html__('Category Page Header Img BG', 'stmt_theme_options'),
                            ),
                            'tag_head_bg_img' => array(
                                'type' => 'image',
                                'label' => esc_html__('Tags Page Header Img BG', 'stmt_theme_options')
                            ),
                            'search_head_bg_img' => array(
                                'type' => 'image',
                                'label' => esc_html__('Search Page Header Img BG', 'stmt_theme_options'),
                            ),
                        )
                    ),
                    'section_6' => array(
                        'name' => esc_html__('Blog', 'stmt_theme_options'),
                        'fields' => array(
                            'blog_style' => array(
                                'type' => 'select',
                                'label' => esc_html__('Blog page style', 'stmt_theme_options'),
                                'options' => array(
                                    '' => esc_html__('Select Style', 'stmt_theme_options'),
                                    '1' => esc_html__('Style 1', 'stmt_theme_options'),
                                    '2' => esc_html__('Style 2', 'stmt_theme_options'),
                                    '3' => esc_html__('Style 3', 'stmt_theme_options')
                                ),
                            ),
                            'blog_banner' => array(
                                'type' => 'select',
                                'label' => esc_html__('Blog page banner', 'stmt_theme_options'),
                                'options' => $banners,
                                'default' => '',
                                'dependency' => array(
                                    'key' => 'blog_style',
                                    'value' => '3'
                                )
                            ),
                        )
                    )
                )
            )
        ));
    }

    function stmt_to_get_settings()
    {
        self::$themeOptions = get_option('stmt_to_settings', array());
        return self::$themeOptions;
    }

    private static function set_typography_fonts()
    {
        $to = self::$themeOptions;

        if (isset($to['default_header_font_family']) && !empty($to['default_header_font_family'])) self::$typography_fonts[] = $to['default_header_font_family'];

        foreach ($to as $k => $val) {
            $setting = json_decode($val);
            if (!is_null($setting) && is_object($setting)) {
                foreach ($setting as $key => $value) {
                    $key = str_replace('"', '', $key);
                    if ($key == 'font-family') self::$typography_fonts[] = $value;
                }
            }
        }
    }

    function stmt_to_settings_page_view()
    {
        $metabox = $this->stmt_to_settings();
        $settings = $this->stmt_to_get_settings();

        foreach ($metabox['args']['stmt_to_settings'] as $section_name => $section) {
            foreach ($section['fields'] as $field_name => $field) {
                $default_value = (!empty($field['value'])) ? $field['value'] : '';
                $metabox['args']['stmt_to_settings'][$section_name]['fields'][$field_name]['value'] = (!empty($settings[$field_name])) ? $settings[$field_name] : $default_value;
            }
        }
        require_once(STMT_TO_DIR . '/settings/view/main.php');
    }

    function stmt_save_settings()
    {
        if (empty($_REQUEST['name'])) die;
        $id = sanitize_text_field($_REQUEST['name']);
        $settings = array();
        $request_body = file_get_contents('php://input');
        if (!empty($request_body)) {
            $request_body = json_decode($request_body, true);
            foreach ($request_body as $section_name => $section) {
                foreach ($section['fields'] as $field_name => $field) {
                    $settings[$field_name] = $field['value'];
                }
            }
        }

        $update = update_option($id, $settings);
        self::stmt_to_get_settings();

        do_action('stmt_set_custom_color');

        wp_send_json($update);
    }

    function get_settings()
    {
        $option = sanitize_text_field($_GET['option_name']);
        wp_send_json(get_option($option, array()));
    }

}

new STMT_TO_Settings;