<?php
function stmt_to_term_meta_field_select($field_key, $value, $field = array())
{
    ?>

    <select name="<?php echo esc_attr($field_key) ?>"
            id="<?php echo esc_attr($field_key) ?>"
            class="term-meta-text-field">
        <?php foreach ($field['options'] as $option_value => $option): ?>
            <option value="<?php echo esc_attr($option_value) ?>" <?php echo selected($value, $option_value); ?>>
                <?php echo sanitize_text_field($option); ?>
            </option>
        <?php endforeach; ?>
    </select>
<?php }

function stmt_to_get_posts($post_type = 'post')
{

    if (!function_exists('is_user_logged_in')) return array();

    $r = array(
        '' => esc_html__('Select', 'stmt_theme_domain')
    );

    if (!is_admin()) return $r;

    $args = array(
        'post_type' => $post_type,
        'posts_per_page' => -1,
    );
    $q = new WP_Query($args);


    if ($q->have_posts()) {
        while ($q->have_posts()) {
            $q->the_post();

            $r[get_the_ID()] = get_the_title();

        }

        wp_reset_postdata();
        wp_reset_query();
    }


    return $r;

}