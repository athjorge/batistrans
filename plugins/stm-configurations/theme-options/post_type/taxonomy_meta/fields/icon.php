<?php
function stmt_to_term_meta_field_icon($field_key, $value)
{
    $icons = array();
    $linear_icons = get_template_directory() . '/assets/icons/linearicons/selection.json';
    if(file_exists($linear_icons)) {
        $icons['Linear'] = array();
        $linear_icons = json_decode(file_get_contents($linear_icons), true);
        foreach($linear_icons['icons'] as $icon) {
            $icons['Linear'][] = "lnricons-{$icon['properties']['name']}";
        }

    }

    ?>
	<div class="stmt_to_icon_field">
		<input type="text"
			   name="<?php echo esc_attr($field_key) ?>"
			   id="<?php echo esc_attr($field_key) ?>"
			   value="<?php echo esc_attr($value); ?>" />
	</div>
	<script type="text/javascript">
        (function($){
            $(document).ready(function($) {
                $('#<?php echo esc_attr($field_key) ?>').fontIconPicker({
                    source : {
                        'LinearIcons' : <?php echo json_encode($icons['Linear']); ?>
                    }
                });
            });
        })(jQuery)
	</script>
<?php }