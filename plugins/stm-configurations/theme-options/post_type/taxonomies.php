<?php

if (!defined('ABSPATH')) exit; //Exit if accessed directly

class STMT_TO_Taxonomies
{
    function __construct()
    {
        add_action('init', array($this, 'taxonomies_init'), -1);
    }

    function taxonomies()
    {
        return apply_filters('stmt_to_taxonomies', array(
            'stmt_brand_taxonomy' => array(
                'post_type' => 'product',
                'args' => array(
                    'hierarchical' => true,
                    'labels' => array(
                        'name' => _x('Brand category', 'taxonomy general name', 'stmt_theme_options'),
                        'singular_name' => _x('Brand category', 'taxonomy singular name', 'stmt_theme_options'),
                        'search_items' => __('Search Brands category', 'stmt_theme_options'),
                        'all_items' => __('All Brands category', 'stmt_theme_options'),
                        'parent_item' => __('Parent Brand category', 'stmt_theme_options'),
                        'parent_item_colon' => __('Parent Brand category:', 'stmt_theme_options'),
                        'edit_item' => __('Edit Brand category', 'stmt_theme_options'),
                        'update_item' => __('Update Brand category', 'stmt_theme_options'),
                        'add_new_item' => __('Add New Brand category', 'stmt_theme_options'),
                        'new_item_name' => __('New Brand category Name', 'stmt_theme_options'),
                        'menu_name' => __('Brand category', 'stmt_theme_options'),
                    ),
                    'show_ui' => true,
                    'show_admin_column' => true,
                    'query_var' => true,
                    'rewrite' => array(
                        'slug' => 'brands'
                    )
                )
            ),
            'stmt_portfolio_category' => array(
                'post_type' => 'stmt-portfolio',
                'args' => array(
                    'hierarchical' => true,
                    'labels' => array(
                        'name' => _x('Portfolio category', 'taxonomy general name', 'stmt_theme_options'),
                        'singular_name' => _x('Portfolio category', 'taxonomy singular name', 'stmt_theme_options'),
                        'search_items' => __('Search Portfolio category', 'stmt_theme_options'),
                        'all_items' => __('All Portfolio category', 'stmt_theme_options'),
                        'parent_item' => __('Parent Portfolio category', 'stmt_theme_options'),
                        'parent_item_colon' => __('Parent Portfolio category:', 'stmt_theme_options'),
                        'edit_item' => __('Edit Portfolio category', 'stmt_theme_options'),
                        'update_item' => __('Update Portfolio category', 'stmt_theme_options'),
                        'add_new_item' => __('Add New Portfolio category', 'stmt_theme_options'),
                        'new_item_name' => __('New Portfolio category Name', 'stmt_theme_options'),
                        'menu_name' => __('Portfolio category', 'stmt_theme_options'),
                    ),
                    'show_ui' => true,
                    'show_admin_column' => true,
                    'query_var' => true,
                    'public' => false
                )
            )
        ));
    }

    function taxonomies_init()
    {
        $taxonomies = $this->taxonomies();
        foreach ($taxonomies as $taxonomy => $taxonomy_args) {
            register_taxonomy($taxonomy, $taxonomy_args['post_type'], $taxonomy_args['args']);
        }
    }
}

new STMT_TO_Taxonomies();