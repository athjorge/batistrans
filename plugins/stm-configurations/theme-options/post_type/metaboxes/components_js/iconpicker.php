<script type="text/javascript">
    <?php
    ob_start();
    include STMT_TO_DIR . '/post_type/metaboxes/components/iconpicker.php';
    $template = preg_replace("/\r|\n/", "", addslashes(ob_get_clean()));

    $icons = array();
    $linear_icons = get_template_directory() . '/assets/icons/linearicons/selection.json';
    if (file_exists($linear_icons)) {
        $icons['Linear'] = array();
        $linear_icons = json_decode(file_get_contents($linear_icons), true);
        foreach ($linear_icons['icons'] as $icon) {
            $icons['Linear'][] = "lnricons-{$icon['properties']['name']}";
        }
    }
    ?>


    Vue.component('stmt-iconpicker', {
        props: ['stored_icon'],
        data: function () {
            return {
                newIcon: '',
                sets : <?php echo json_encode($icons); ?>,
                openSets : false,
                searchQuery : ''
            }
        },
        mounted: function () {
            this.newIcon = this.stored_icon;
        },
        template: '<?php echo $template; ?>',
        methods: {
            filterItems: function(presets) {
                var app = this;
                return presets.filter(function(preset) {
                    let regex = new RegExp('(' + app.searchQuery + ')', 'i');
                    return preset.match(regex);
                })
            },
            addIcon(icon) {
                this.newIcon = icon;
                this.openSets = false;
            }
        },
        watch: {
            newIcon: function () {
                this.$emit('get-icon', this.newIcon);
            }
        }
    });

    Vue.component('v-style', {
        render: function (createElement) {
            return createElement('style', this.$slots.default)
        }
    });

</script>